import { defaultEnvironment } from './environment.default';

export const environment = {
  ...defaultEnvironment,
  apiUrl: window['env']['apiUrl'] || 'http://10.20.30.210:8086/mobileweb',
  production: false,
};
