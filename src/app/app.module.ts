import { BrowserModule, Title } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import './core/heplers/string-prototype';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';

// plugin
// 404
import { PagenotfoundModule } from './features/common/pagenotfound/pagenotfound.module';

// Service
// 共用開窗
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { ModalService } from 'src/app/core/modal/modal.service';
// 共用資料
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
// API過濾器
import { ApiHelperService } from 'src/app/core/services/utils/api-helper.service';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
// 後端清單
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { PolicyService } from './core/services/api/policy/policy.service';
import { LocalService } from './core/services/api/local/local.service';
import { LangService } from './core/services/lang/lang.service';
import { InsuranceService } from './core/services/api/Insurance/insurance.service';
/** 試算交換資料 */
import { OnlinecalcService } from './core/services/onlinecalc/onlinecalc.service';
/** 路由攔截器 */
import { RouterFilterService } from './core/services/utils/router-filter.service';
// 社群
import { SocialService } from 'src/app/core/services/social.service';
// 靜態服務區 .. 無狀態專用
import { CommonService } from './core/services/common.service';
// 樣式調整方法
import { StyleHelperService } from './core/services/style-helper.service';

// google 驗證
import {
  SocialLoginModule,
  SocialAuthServiceConfig,
} from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';

// global Variants
import { environment } from 'src/environments/environment';
import { ScullyLibModule } from '@scullyio/ng-lib';
// GA Start
// export const EnvironmentToken = new InjectionToken('ENVIRONMENT');
declare let gtag: Function;

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    // CoreModule,
    SharedModule,
    HttpClientModule,
    SocialLoginModule,
    PagenotfoundModule,
    ScullyLibModule,
  ],
  providers: [
    AlertServiceService,
    DatapoolService,
    ApiHelperService,
    ValidatorsService,
    MemberService,
    PublicService,
    DatePipe,
    PolicyService,
    LangService,
    LocalService,
    ModalService,
    InsuranceService,
    OnlinecalcService,
    StyleHelperService,
    CommonService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.google.AuthID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.fb.AuthID, {
              auth_type: 'reauthenticate',
              scope: 'email,public_profile',
              fields: 'name,email,picture,first_name,last_name',
              version: 'v4.0',
            }),
          },
        ],
      } as SocialAuthServiceConfig,
    },
    SocialService,
    {
      /** 資料工廠 */
      provide: APP_INITIALIZER,
      useFactory: (provider: PublicService) => () => provider.beforstart(),
      deps: [PublicService],
      multi: true,
    },
    /** 攔截器 */
    RouterFilterService,
    /** GA */
    // [{ provide: EnvironmentToken, useValue: environment }],
    Title,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
