import { TagType } from './tags';

export interface NewsCard {
  type: TagType;
  category: string;
  img: string;
  date: string;
  title: string;
}
