import { MwHeaderRes } from './mw-header-res';

export interface BaseRes<T> {
  MWHEADER: MwHeaderRes;
  TRANRS: T;
}
