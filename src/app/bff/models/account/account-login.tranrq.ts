export interface AccountLoginTranrq {
  ACCOUNT_ID: string;
  ACCOUNT_PWD: string;
  ACCOUNT_TYPE: string;
  CLIENT_DEVICE: string;
  CLIENT_IP: string;
  KEY_VERSION?: string;
  SESSION_ID: string;
}
