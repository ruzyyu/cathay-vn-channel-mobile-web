import { MwHeaderReq } from './mw-header-req';

export interface BaseReq<T> {
  MWHEADER: MwHeaderReq;
  TRANRQ?: T;
}