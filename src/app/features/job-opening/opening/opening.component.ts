import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { jobDetail } from 'src/app/data/job';
import { Lang } from 'src/types';

@Component({
  selector: 'app-opening',
  templateUrl: './opening.component.html',
  styleUrls: ['./opening.component.scss'],
})
export class OpeningComponent implements OnInit {
  filterTypeId: number;

  /** Opening Type List */
  openingType: any[];

  /** Opening Card */
  openingCard = jobDetail;

  // 變數宣告
  FLang: Lang;
  FLangType: string;

  constructor(private cds: DatapoolService, private router: Router) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit() {
    this.filterTypeId = 0;
    this.openingType = [
      { name: this.FLang.G021.L0002, type: '' },
      { name: this.FLang.G021.L0003, type: 'Indemnify' },
      { name: this.FLang.G021.L0004, type: 'IT' },
      { name: this.FLang.G021.L0005, type: 'ReBao' },
    ];
  }

  /** Change Type */
  filterTypeIdChange(param) {
    this.filterTypeId = param;
  }

  /** Return Opening Card */
  get filterCardList() {
    if (this.filterTypeId === 0) {
      return this.openingCard;
    }
    return this.openingCard.filter(
      (item) => item.type === this.openingType[this.filterTypeId].type
    );
  }

  clickJob(queryParams: string) {
    this.router.navigate(['about/opening/opening-detail'], {
      queryParams: { jobId: queryParams },
    });
  }
}
