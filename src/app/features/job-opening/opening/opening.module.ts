import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OpeningRoutingModule } from './opening-routing.module';
import { OpeningComponent } from './opening.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [OpeningComponent],
  imports: [
    CommonModule,
    OpeningRoutingModule,
    SharedModule
  ]
})
export class OpeningModule { }
