import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OpeningDetailRoutingModule } from './opening-detail-routing.module';
import { OpeningDetailComponent } from './opening-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [OpeningDetailComponent],
  imports: [
    CommonModule,
    OpeningDetailRoutingModule,
    SharedModule
  ]
})
export class OpeningDetailModule { }
