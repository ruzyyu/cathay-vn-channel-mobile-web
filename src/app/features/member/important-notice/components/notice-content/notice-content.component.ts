import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-notice-content',
  templateUrl: './notice-content.component.html',
  styleUrls: ['./notice-content.component.scss'],
})
export class NoticeContentComponent implements OnInit {
  // 重要通知資料
  @Input() noticeList = [];

  // 重要通知 Id
  @Output() noticeId = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  /** Emit Notice Id
   * id: 重要通知 Id
   */
  emitId(id) {
    this.noticeId.emit(id);
  }
}
