import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImportantNoticeRoutingModule } from './important-notice-routing.module';
import { ImportantNoticeComponent } from './important-notice.component';
import { NoticeContentComponent } from './components/notice-content/notice-content.component';
import { BackgroundComponent } from './components/background/background.component';

@NgModule({
  declarations: [
    ImportantNoticeComponent,
    NoticeContentComponent,
    BackgroundComponent,
  ],
  imports: [CommonModule, ImportantNoticeRoutingModule, SharedModule],
})
export class ImportantNoticeModule {}
