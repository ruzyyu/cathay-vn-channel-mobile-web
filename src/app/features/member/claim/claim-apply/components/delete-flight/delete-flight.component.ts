import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-delete-flight',
  templateUrl: './delete-flight.component.html',
  styleUrls: ['./delete-flight.component.scss'],
})
export class DeleteFlightComponent implements OnInit, OnDestroy {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  @Output() confirm = new EventEmitter();

  FLang: any = {};

  ngOnInit() {}

  ngOnDestroy() {
    $('#deleteFlight').modal('hide');
  }

  onConfirm() {
    $('#deleteFlight').modal('hide');

    this.confirm.emit();
  }

  onCancel() {
    $('#deleteFlight').modal('hide');
  }
}
