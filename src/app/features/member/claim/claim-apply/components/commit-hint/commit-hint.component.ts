import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-commit-hint',
  templateUrl: './commit-hint.component.html',
  styleUrls: ['./commit-hint.component.scss'],
})
export class CommitHintComponent implements OnInit {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  @Output() confirm = new EventEmitter();

  FLang: any = {};

  ngOnInit() {}

  onConfirm() {
    this.confirm.emit();

    this.hideModal();
  }

  onCancel() {
    this.hideModal();
  }

  hideModal() {
    $('#commitHint').modal('hide');
  }
}
