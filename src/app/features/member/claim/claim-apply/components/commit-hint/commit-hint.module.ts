import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommitHintComponent } from './commit-hint.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CommitHintComponent]
})
export class CommitHintModule { }
