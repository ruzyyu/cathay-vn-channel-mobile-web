import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClaimResultComponent } from './claim-result.component';
import { ClaimResultRoutingModule } from './claim-result-routing.module';

@NgModule({
  declarations: [ClaimResultComponent],
  imports: [CommonModule, ClaimResultRoutingModule, SharedModule],
})
export class ClaimResultModule {}
