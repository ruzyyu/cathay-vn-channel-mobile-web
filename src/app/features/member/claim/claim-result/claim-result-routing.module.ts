import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClaimResultComponent } from './claim-result.component';

const routes: Routes = [{ path: '', component: ClaimResultComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClaimResultRoutingModule {}
