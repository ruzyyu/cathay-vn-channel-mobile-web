import { CommonService } from 'src/app/core/services/common.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cds: DatapoolService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }
  FLang: any;
  FLangType: any;
  FUrl: string;
  insuranceData: any[] = [];

  ngOnInit(): void {
    this.route.queryParams.subscribe((queryParams) => {
      this.FUrl = queryParams.intro;
    });
    this.getData();
    // console.log(this.FUrl);
  }
  getData() {
    if (!this.FUrl) {
      this.router.navigate(['/']);
    }
    /** 意外險 */
    if (this.FUrl.indexOf('contract03.json') !== -1) {
      this.insuranceData = [
        {
          title: this.FLang.F09.L0065,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0009,
              info: this.FLang.F09.L0013,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0010,
              info: this.FLang.F09.L0014,
              file: this.FLang.F09.L0015,
              fileUrl: '#',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0011,
              info: this.FLang.F09.L0016,
              file: '',
              fileUrl: '',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0012,
              info: this.FLang.F09.L0017,
              file: '',
              fileUrl: '',
            },
          ],
        },
      ];
    }
    /** 車險 */
    if (this.FUrl.indexOf('contract01.json') !== -1) {
      this.insuranceData = [
        {
          title: this.FLang.F09.L0065,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0019,
              info: this.FLang.F09.L0021,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0020,
              info: this.FLang.F09.L0022,
              file: '',
              fileUrl: '',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0011,
              info: this.FLang.F09.L0023,
              file: this.FLang.F09.L0024,
              fileParen: true,
              fileUrl: '#',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0012,
              info: this.FLang.F09.L0025,
              file: '',
              fileUrl: '',
            },
          ],
        },
      ];
    }
    /** 旅遊險 */
    if (this.FUrl.indexOf('contract02.json') !== -1) {
      this.insuranceData = [
        {
          title: this.FLang.F09.L0054,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0056,
              info: this.FLang.F09.L0057,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0058,
              info: this.FLang.F09.L0059,
              file: '',
              fileUrl: '',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0060,
              info: this.FLang.F09.L0061,
              file: '',
              fileUrl: '',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0062,
              info: this.FLang.F09.L0063,
              file: this.FLang.F09.L0064,
              fileUrl: '#',
            },
          ],
        },
        {
          title: this.FLang.F09.L0065,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0009,
              info: this.FLang.F09.L0026,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0010,
              info: this.FLang.F09.L0014,
              file: this.FLang.F09.L0015,
              fileUrl: '#',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0011,
              info: this.FLang.F09.L0027,
              file: '',
              fileUrl: '',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0012,
              info: this.FLang.F09.L0028,
              file: '',
              fileUrl: '',
            },
          ],
        },
      ];
    }
    /** 住火險 (房屋險)*/
    if (this.FUrl.indexOf('contract04.json') !== -1) {
      this.insuranceData = [
        {
          title: this.FLang.F09.L0065,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0019,
              info: this.FLang.F09.L0029,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0010,
              info: this.FLang.F09.L0030,
              infoTwo: this.FLang.F09.L0030_,
              file: this.FLang.F09.L0031,
              fileUrl: '#',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0011,
              info: this.FLang.F09.L0032,
              file: '',
              fileUrl: '',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0012,
              info: this.FLang.F09.L0033,
              file: '',
              fileUrl: '',
            },
          ],
        },
      ];
    }
    /** 工程險 */
    if (this.FUrl.indexOf('contract05.json') !== -1) {
      this.insuranceData = [
        {
          title: this.FLang.F09.L0065,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0019,
              info: this.FLang.F09.L0043,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0034,
              info: this.FLang.F09.L0035,
              file: '',
              fileUrl: '',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0011,
              info: this.FLang.F09.L0036,
              infoTwo: this.FLang.F09.L0036_,
              file: this.FLang.F09.L0037,
              fileUrl: '#',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0012,
              info: this.FLang.F09.L0033,
              file: '',
              fileUrl: '',
            },
          ],
        },
      ];
    }
    /** 貨運險 */
    if (this.FUrl.indexOf('contract08.json') !== -1) {
      this.insuranceData = [
        {
          title: this.FLang.F09.L0065,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0019,
              info: this.FLang.F09.L0039,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0038,
              info: this.FLang.F09.L0040,
              file: '',
              fileUrl: '',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0011,
              info: this.FLang.F09.L0041,
              file: this.FLang.F09.L0037,
              fileUrl: '#',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0012,
              info: this.FLang.F09.L0042,
              file: '',
              fileUrl: '',
            },
          ],
        },
      ];
    }
    /** 責任險 */
    if (this.FUrl.indexOf('contract06.json') !== -1) {
      this.insuranceData = [
        {
          title: this.FLang.F09.L0065,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0019,
              info: this.FLang.F09.L0045,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0044,
              info: this.FLang.F09.L0046,
              infoTwo: this.FLang.F09.L0046_,
              file: this.FLang.F09.L0037,
              fileUrl: '',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0011,
              info: this.FLang.F09.L0048,
              infoTwo: this.FLang.F09.L0048_,
              file: this.FLang.F09.L0037,
              fileUrl: '#',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0012,
              info: this.FLang.F09.L0049,
              file: '',
              fileUrl: '',
            },
          ],
        },
      ];
    }
    /** 財產險 */
    if (this.FUrl.indexOf('contract07.json') !== -1) {
      this.insuranceData = [
        {
          title: this.FLang.F09.L0065,
          textArray: [
            {
              number: '1',
              src: 'assets/img/commodity/service/img-claim-start.png',
              srcset: 'assets/img/commodity/service/img-claim-start@2x.png 2x',
              title: this.FLang.F09.L0019,
              info: this.FLang.F09.L0050,
              file: '',
              fileUrl: '',
            },
            {
              number: '2',
              src: 'assets/img/commodity/service/img-claim-info.png',
              srcset: 'assets/img/commodity/service/img-claim-info@2x.png 2x',
              title: this.FLang.F09.L0010,
              info: this.FLang.F09.L0051,
              infoTwo: this.FLang.F09.L0051_,
              file: this.FLang.F09.L0037,
              fileUrl: '#',
            },
            {
              number: '3',
              src: 'assets/img/commodity/service/img-claim-paper.png',
              srcset: 'assets/img/commodity/service/img-claim-paper@2x.png 2x',
              title: this.FLang.F09.L0011,
              info: this.FLang.F09.L0052,
              infoTwo: this.FLang.F09.L0052_,
              file: this.FLang.F09.L0037,
              fileUrl: '#',
            },
            {
              number: '4',
              src: 'assets/img/commodity/service/img-claim-done.png',
              srcset: 'assets/img/commodity/service/img-claim-done@2x.png 2x',
              title: this.FLang.F09.L0012,
              info: this.FLang.F09.L0053,
              file: '',
              fileUrl: '',
            },
          ],
        },
      ];
    }
  }

  // 滑動
  scrollTo() {
    CommonService.scrollto('description', 60);
  }
}
