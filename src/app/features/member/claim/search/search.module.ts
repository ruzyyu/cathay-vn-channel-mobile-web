import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { IndemnityInfoComponent } from './indemnity-info/indemnity-info.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { UploadFileComponent } from './upload-file/upload-file.component';

@NgModule({
  declarations: [SearchComponent, IndemnityInfoComponent, ContactUsComponent, UploadFileComponent],
  imports: [CommonModule, SearchRoutingModule, SharedModule],
})
export class SearchModule {}
