import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
} from '@angular/core';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

export interface IUploadFile {
  supplement_no?: string;
  image?: any;
  file?: any;
  error?: boolean;
}

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
})
export class UploadFileComponent implements OnInit, OnChanges {
  constructor(private memberApi: MemberService, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }
  FLang: any;
  @Input() data = { doc_list: [] };

  @Output() loaded = new EventEmitter();

  FFileList: any[];

  isAllUpload = false;

  ngOnInit(): void {}

  ngOnChanges(): void {
    if (this.data && this.data.doc_list) {
      this.FFileList = this.data.doc_list.map((item) => ({
        ...item,
      }));
    }
  }

  async setFile(event, item) {
    const imgFile = event.target.files[0];
    // 直接取代圖片
    const fileSize = imgFile.size;
    const accept = event.target.accept;

    const reader = new FileReader();
    if (!accept.includes(imgFile.name.replace(/(.*)(\.\S+)$/, '$2'))) {
      item.image = null;
      item.error = true;
      item.errorMsg = this.FLang.F05.F0038;
      return;
    } else if (fileSize <= 7340032) {
      const payload = { image: imgFile };
      const rep = await this.memberApi.upload(payload);
      if (rep.status === 200) {
        item.file = {
          imgName: rep.data.filename,
          imgType: imgFile.type,
        };
      }
      reader.readAsDataURL(imgFile);
      reader.onload = () => {
        item.image = reader.result;
        item.imageName = imgFile.name.replace(/(.*)(\.\S+)$/, '$1$2');
        item.error = false;

        this.checkAllUpload();
      };
    } else {
      item.image = null;
      item.error = true;
      item.errorMsg = this.FLang.F05.F0038;
    }
  }

  checkAllUpload() {
    this.isAllUpload = this.FFileList.every((item) => !!item.file);
  }

  clearImg(index) {
    this.FFileList[index].file = null;
    this.FFileList[index].image = null;
    this.FFileList[index].error = false;
  }

  doSubmit() {
    this.loaded.emit(this.FFileList);
  }
}
