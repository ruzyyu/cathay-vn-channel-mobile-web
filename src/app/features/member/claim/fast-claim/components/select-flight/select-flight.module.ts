import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectFlightComponent } from './select-flight.component';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [SelectFlightComponent],
})
export class SelectFlightModule {}
