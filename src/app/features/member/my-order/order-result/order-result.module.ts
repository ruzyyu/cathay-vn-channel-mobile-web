import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderResultComponent } from './order-result.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrderResultRoutingModule } from './order-result-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, OrderResultRoutingModule],
  declarations: [OrderResultComponent],
})
export class OrderResultModule {}
