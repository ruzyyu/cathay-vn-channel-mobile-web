import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-tab-card',
  templateUrl: './tab-card.component.html',
  styleUrls: ['./tab-card.component.scss'],
})
export class TabCardComponent implements OnChanges {
  constructor() {}

  /** 網頁元素 id */
  @Input() id: string;
  /** 群組名稱 */
  @Input() groupName: string;

  /** 收合面板元素 id  */
  collapseId: string;

  /** 收合面板目標 selector */
  collapseTarget: string;

  ngOnChanges() {
    this.collapseId = `collapse-${this.id}`;
    this.collapseTarget = `#${this.collapseId}`;
  }
}
