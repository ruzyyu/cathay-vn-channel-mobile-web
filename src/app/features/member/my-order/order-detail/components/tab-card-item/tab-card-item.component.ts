import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tab-card-item',
  templateUrl: './tab-card-item.component.html',
  styleUrls: ['./tab-card-item.component.scss'],
})
export class TabCardItemComponent {
  constructor() {}

  /** 項目名稱 */
  @Input() label: string;
}
