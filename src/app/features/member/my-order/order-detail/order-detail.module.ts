import { PaymentIconPipe } from './pipes/payment-icon.pipe';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderDetailRoutingModule } from './order-detail-routing.module';
import { OrderDetailComponent } from './order-detail.component';
import { TabCardComponent } from './components/tab-card/tab-card.component';
import { TabCardItemComponent } from './components/tab-card-item/tab-card-item.component';
import { PaymentFailComponent } from '../../../../shared/components/payment-fail/payment-fail.component';

@NgModule({
  declarations: [
    OrderDetailComponent,
    TabCardComponent,
    TabCardItemComponent,
    PaymentIconPipe,
  ],
  imports: [CommonModule, OrderDetailRoutingModule, SharedModule],
})
export class OrderDetailModule {}
