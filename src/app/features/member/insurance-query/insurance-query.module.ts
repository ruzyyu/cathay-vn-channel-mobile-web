import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InsuranceQueryRoutingModule } from './insurance-query-routing.module';
import { InsuranceQueryComponent } from './insurance-query.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [InsuranceQueryComponent],
  imports: [CommonModule, InsuranceQueryRoutingModule, SharedModule],
})
export class InsuranceQueryModule {}
