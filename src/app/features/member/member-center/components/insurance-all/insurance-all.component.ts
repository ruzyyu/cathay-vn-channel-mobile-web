import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from './../../../../../core/services/datapool/datapool.service';
import { Router } from '@angular/router';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { environment } from 'src/environments/environment';
import { Lang } from 'src/types';

interface IPageinfo {
  car_policy_count: number;
  moto_policy_count: number;
  travel_count: number;
  tmp_count: number;
  renew_count: number;
}

interface IData {
  title: string;
  number: number;
  url: string;
  img: string;
  srcset: string;
  mobileImg: string;
  mobileSrcset: string;
  tag: string;
}

@Component({
  selector: 'app-insurance-all',
  templateUrl: './insurance-all.component.html',
  styleUrls: ['./insurance-all.component.scss'],
})
export class InsuranceAllComponent implements OnInit, OnChanges {
  @Input() Finfo: IPageinfo;

  @Input() FPop: boolean;

  @Output() FShow = new EventEmitter();

  FCardList: Array<IData>;

  FLang: Lang;

  /** 儲存線下保單狀態 */
  FPolicy: string;

  constructor(
    private router: Router,
    private cds: DatapoolService,
    private api: MemberService
  ) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {
    this.FPolicy = this.cds.userInfo.is_validate_policy;

    this.FCardList = [
      {
        title: this.FLang.F001.L0011,
        number: this.Finfo.car_policy_count,
        url: '/member/my-policy/car',
        img: 'assets/img/ic-member-car.png',
        srcset: 'assets/img/ic-member-car@2x.png 2x',
        mobileImg: 'assets/img/ic-member-car-m.png',
        mobileSrcset: 'assets/img/ic-member-car-m@2x.png 2x',
        tag: 'car',
      },
      {
        title: this.FLang.F001.L0012,
        number: this.Finfo.moto_policy_count,
        url: '/member/my-policy/moto',
        img: 'assets/img/ic-member-motor.png',
        srcset: 'assets/img/ic-member-motor@2x.png 2x',
        mobileImg: 'assets/img/ic-member-motor-m.png',
        mobileSrcset: 'assets/img/ic-member-motor-m@2x.png 2x',
        tag: 'motor',
      },
      {
        title: this.FLang.F001.L0010,
        number: this.Finfo.travel_count,
        url: '/member/my-policy/travel',
        img: 'assets/img/ic-member-travel.png',
        srcset: 'assets/img/ic-member-travel@2x.png 2x',
        mobileImg: 'assets/img/ic-member-travel-m.png',
        mobileSrcset: 'assets/img/ic-member-travel-m@2x.png 2x',
        tag: 'travel',
      },
    ];
  }

  get FCount() {
    let mCount = 0;
    this.FCardList.forEach((item) => {
      mCount = mCount + item.number;
    });
    return mCount;
  }

  ngOnChanges(change) {
    this.FCardList = [
      {
        title: this.FLang.F001.L0011,
        number: this.Finfo.car_policy_count,
        url: '/member/my-policy',
        img: 'assets/img/ic-member-car.png',
        srcset: 'assets/img/ic-member-car@2x.png 2x',
        mobileImg: 'assets/img/ic-member-car-m.png',
        mobileSrcset: 'assets/img/ic-member-car-m@2x.png 2x',
        tag: 'car',
      },
      {
        title: this.FLang.F001.L0012,
        number: this.Finfo.moto_policy_count,
        url: '/member/my-policy',
        img: 'assets/img/ic-member-motor.png',
        srcset: 'assets/img/ic-member-motor@2x.png 2x',
        mobileImg: 'assets/img/ic-member-motor-m.png',
        mobileSrcset: 'assets/img/ic-member-motor-m@2x.png 2x',
        tag: 'motor',
      },
      {
        title: this.FLang.F001.L0010,
        number: this.Finfo.travel_count,
        url: '/member/my-policy/se',
        img: 'assets/img/ic-member-travel.png',
        srcset: 'assets/img/ic-member-travel@2x.png 2x',
        mobileImg: 'assets/img/ic-member-travel-m.png',
        mobileSrcset: 'assets/img/ic-member-travel-m@2x.png 2x',
        tag: 'travel',
      },
    ];
    if (change.FPop && change.FPop.currentValue) {
      this.openCLife();
    }
  }

  /** 開啟彈窗 */
  goCLife() {
    this.FShow.emit();
  }

  /** 開啟看我的人壽保險 */
  async openCLife() {
    if (!this.FPop) {
      return;
    }
    // 呼叫會員加密
    // let goLife = window.open();
    const payload = { seq_no: String(this.cds.userInfo.seq_no) };
    const rep = await this.api.providesso(payload);

    if (rep.status === 200) {
      // (goLife.location.href = `${environment.cathaylifebaseurl}?param=${rep.data.encrypt_seq_no}&LG_CODE=${this.cds.gLangType} `),
      //   '_blank';
      const open = document.createElement('a');
      open.setAttribute(
        'href',
        `${this.cds.cathaylifebaseurl}?param=${rep.data.encrypt_seq_no}&LG_CODE=${this.cds.gLangType}`
      );
      open.setAttribute('target', '_blank');
      document.body.appendChild(open);
      open.click();
    }
  }

  /** 去線下保單 */
  goVerify() {
    this.router.navigate(['/member/verify']);
  }

  styleCss(index, classes: string) {
    return index !== '' ? `number ${classes}` : '';
  }

  goMyPolicy(xtag) {
    this.router.navigate(['member/my-policy'], {
      queryParams: {
        type: xtag,
      },
    });
  }
}
