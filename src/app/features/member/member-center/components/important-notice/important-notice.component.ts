import { Component, OnInit } from '@angular/core';
import { DatapoolService } from './../../../../../core/services/datapool/datapool.service';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { IResponse } from 'src/app/core/interface';
import moment from 'moment';
@Component({
  selector: 'app-important-notice',
  templateUrl: './important-notice.component.html',
  styleUrls: ['./important-notice.component.scss'],
})
export class ImportantNoticeComponent implements OnInit {
  // 重要通知資料
  cardList = [];

  // 重要通知詳細資料
  noticeDetail = {
    tag: '',
    create_date: '',
    content: '',
  };
  FLang: any;
  date: any;

  constructor(
    private cds: DatapoolService,
    private memberService: MemberService
  ) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {
    this.getNoticeList();
  }

  /** 取得重要通知資料 */
  async getNoticeList() {
    if (this.cds.userInfo.seq_no) {
      const param = {
        member_seq_no: this.cds.userInfo.seq_no,
      };
      const rep: IResponse = await this.memberService.NoticeList(param);
      if (rep.status === 200) {
        this.cardList = rep.data.slice(0, 2).map((item) => ({
          ...item,
          create_date: moment(item.create_date).format('HH:mm YYYY/MM/DD'),
          contentView: item.content.replace(/<a/gi, '<div').replace(/<\/a/gi, '</div')
        }));
      } else {
        console.log('errors');
      }
    }
  }

  /** 取得重要通知詳細內容
   * id: 重要通知 Id
   */
  getNoticeDetail(id) {
    this.noticeDetail = this.cardList[id];
    this.cds.gNoticeDetail = this.noticeDetail;
    $('#Modal-19').modal('show');
  }
}
