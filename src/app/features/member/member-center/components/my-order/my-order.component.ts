import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Router } from '@angular/router';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
} from '@angular/core';
import { UserInfo } from 'src/app/core/interface/index';

interface IPageinfo {
  car_policy_count: number;
  moto_policy_count: number;
  travel_count: number;
  tmp_count: number;
  renew_count: number;
}

interface IDataList {
  title: string;
  url: string;
  number: number;
}

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.scss'],
})
export class MyOrderComponent implements OnInit, OnChanges {
  // 變數宣告
  @Output() chooseAction = new EventEmitter<any>();
  @Input() Finfo: IPageinfo;
  @Input() FDueSoonCardLength;
  @Input() FEndRsmntCardLength;
  @Input() xbeforstart;
  userInfo: UserInfo;
  FLinkList: Array<IDataList>;
  FLang: any;
  constructor(
    private alert: AlertServiceService,
    private go: Router,
    private cds: DatapoolService
  ) {
    this.FLang = this.cds.gLang;
  }

  ngOnChanges() {
    if (this.xbeforstart === 'keep') {
      this.doAction(1);
    }
    if (this.xbeforstart === 'policyChange') {
      this.doAction(2);
    }
    this.FLinkList = [
      {
        title: this.FLang.F001.L0001,
        url: '/member/my-order',
        number: this.Finfo.tmp_count,
      },
      {
        title: this.FLang.F001.L0002,
        url: '/member/my-policy',
        number: this.Finfo.renew_count,
      },
      {
        title: this.FLang.F001.L0003,
        url: '/member/my-policy/insurance-changing',
        number: -1,
      },
      { title: this.FLang.F001.L0004, url: '', number: -1 },
      { title: this.FLang.F001.L0005, url: '', number: -1 },
    ];
  }

  ngOnInit(): void {
    this.userInfo = this.cds.userInfo;
    this.FLinkList = [
      {
        title: this.FLang.F001.L0001,
        url: '/member/my-order',
        number: this.Finfo.tmp_count,
      },
      {
        title: this.FLang.F001.L0002,
        url: '/member/my-policy',
        number: this.Finfo.renew_count,
      },
      {
        title: this.FLang.F001.L0003,
        url: '/member/my-policy/insurance-changing',
        number: 0,
      },
      { title: this.FLang.F001.L0004, url: '', number: 0 },
      { title: this.FLang.F001.L0005, url: '', number: 0 },
    ];
  }

  styleCss(index) {
    return index !== '' ? 'number' : '';
  }

  goMember() {
    this.go.navigateByUrl('member');
  }

  doAction(index: number) {
    switch (index) {
      // 我的訂單
      case 0:
        this.go.navigate(['member/my-order']);
        break;
      //  我要續保
      case 1:
        // 查詢API
        if (this.FDueSoonCardLength > 0) {
          // 符合條件
          this.chooseAction.emit(index);
        } else {
          // 無符合條件
          this.alert.open({
            type: 'file',
            title: this.FLang.F001.L0018,
            content: this.FLang.F001.L0025,
            callback: this.goMember(),
          });
        }
        break;
      //  保單變更
      case 2:
        // 呼叫API
        if (this.FEndRsmntCardLength > 0) {
          // 符合條件
          this.chooseAction.emit(index);
        } else {
          // 無符合條件
          console.log('not fit');
          this.alert.open({
            type: 'file ',
            title: this.FLang.F001.L0018,
            content: this.FLang.F001.L0019,
          });
        }
        break;
      // 我要理賠
      case 3:
        // 1. 有沒有登入
        if (this.userInfo.seq_no) {
          // 2. 有沒有快速理賠
          /** 有 */
          this.go.navigate(['member/claim']);
          /** 沒有 */
          this.go.navigate(['member/claim']);
        }
        break;

      // 理賠查詢
      case 4:
        this.go.navigate(['member/claim/search']);
        break;
    }
  }
}
