import { CapsuleStylePipe } from './../../../../../../shared/pipes/capsule-style.pipe';
import { CapsulePipe } from './../../../../../../shared/pipes/capsule.pipe';
import { DatapoolService } from './../../../../../../core/services/datapool/datapool.service';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Lang } from 'src/types';

@Component({
  selector: 'app-policy-detail-travel',
  templateUrl: './policy-detail-travel.component.html',
  styleUrls: ['./policy-detail-travel.component.scss'],
})
export class PolicyDetailTravelComponent implements OnInit, OnChanges {
  /** 保單資料 */
  @Input() policyData: any = {};
  /** 被保人資料 */
  @Input() insrData: any = [];
  /** 資料 */
  @Input() policyholderData: any = {};
  /** 是否為要保人 */
  @Input() isApc = false;
  /** 是否為團單 */
  @Input() isGroup = false;
  /** 是否為被保人 */
  @Input() isInsr = false;

  FLang: Lang;

  FLangType: string;

  FPropLangType: string;

  /** 被保人投保內容 */
  insrCoverages: any[] = [{ coverages: [], additionalTerms: [] }];

  /** 是否只顯示使用者的被保人資訊 */
  isInsrSelf: boolean;

  /** 被保人超過10人(包含10人) */
  isMoreThanTen: boolean;

  /** 保單狀態樣式 */
  expiry: string;

  /** 保單狀態 */
  expiryText: string;

  constructor(
    private cds: DatapoolService,
    private capsulePipe: CapsulePipe,
    private capsuleStylePipe: CapsuleStylePipe
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;

    this.FPropLangType =
      {
        'vi-VN': 'Vn',
        'Zh-TW': 'Zh',
        'en-US': 'En',
      }[this.cds.gLangType] || 'Zh';
  }

  ngOnInit() {}

  ngOnChanges() {
    this.getExpiry();
    this.getInsrSelf();
    this.getCoverages();
  }

  /** 檢核生失效 */
  getExpiry() {
    if (this.policyData) {
      this.expiry = this.capsuleStylePipe.transform(
        this.FLangType,
        this.policyData.pol_dt,
        this.policyData.pol_due_dt
      );

      this.expiryText = this.capsulePipe.transform(
        this.FLangType,
        this.policyData.pol_dt,
        this.policyData.pol_due_dt
      );
    }
  }

  /** 取得10人以上團單同時投保人是被保人、使用者非投保人或使用者是被保人，只顯示自己的被保人保單 */
  getInsrSelf() {
    if (this.insrData && this.policyholderData) {
      this.isMoreThanTen = this.insrData.length >= 10;
      if (this.isMoreThanTen || (!this.isApc && this.isInsr)) {
        this.isInsrSelf = true;

        this.insrData = this.insrData.filter(
          (item) =>
            item.insr_name.toUpperCase() ===
            this.cds.userInfo.customer_name.toUpperCase()
        );
      } else {
        this.isInsrSelf = false;
      }
    }
  }

  /** 取得被保人投保內容 */
  getCoverages() {
    if (this.insrData) {
      this.insrCoverages = this.insrData.map((insrItem) => {
        const coverageGroup: any = {};
        let additionalTerms = [];

        // 分類並提取附加條款
        insrItem.coverages.forEach((item) => {
          if (Number(item.amt) > 0) {
            if (item.prod_id === 'F99') {
              additionalTerms = additionalTerms.concat(item.coverageItems);
            } else {
              if (!coverageGroup[item.coverageTypeId]) {
                coverageGroup[item.coverageTypeId] = {
                  coverageTypeName: item.coverageTypeName,
                  coverages: [],
                };
              }
              coverageGroup[item.coverageTypeId].coverages.push(item);
            }
          }
        });

        // 將物件轉成陣列
        const coverages = Object.keys(coverageGroup).map(
          (key) => coverageGroup[key]
        );
        return { coverages, additionalTerms };
      });

      console.log('1', this.insrCoverages);
    }
  }
}
