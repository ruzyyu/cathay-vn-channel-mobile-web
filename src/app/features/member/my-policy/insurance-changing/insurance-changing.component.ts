import { CommonService } from './../../../../core/services/common.service';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, HostListener } from '@angular/core';
import { IResponse } from '../../../../core/interface/index';
import {
  trigger,
  state,
  transition,
  animate,
  style,
} from '@angular/animations';
import { PolicyService } from 'src/app/core/services/api/policy/policy.service';
import {
  Validators,
  FormBuilder,
  FormGroup,
  AbstractControl,
} from '@angular/forms';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { UserInfo } from 'src/app/core/interface';
import * as moment from 'moment';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { DatePipe } from '@angular/common';

/** API 保戶服務中心保單變更牌卡 */
interface EndRsmntCard {
  prod_pol_code: string;
  pol_dt: Date;
  pol_due_dt: Date;
  pol_no: string;
  cntr_no: string;
  pol_days?: number;
  tral_proj: string;
  amt: string;
}
@Component({
  selector: 'app-insurance-changing',
  templateUrl: './insurance-changing.component.html',
  styleUrls: ['./insurance-changing.component.scss'],
  animations: [
    trigger('openClose', [
      state(
        'open',
        style({
          opacity: 1,
        })
      ),
      state(
        'closed',
        style({
          opacity: 0,
        })
      ),
      transition('* => closed', [animate('0.3s')]),
      transition('* => open', [animate('0.5s')]),
    ]),
  ],
})
export class InsuranceChangingComponent implements OnInit {
  constructor(
    private go: Router,
    private route: ActivatedRoute,
    private alter: AlertServiceService,
    private fb: FormBuilder,
    private cds: DatapoolService,
    private apiPolicy: PolicyService,
    private validatorsService: ValidatorsService,
    private datePipe: DatePipe
  ) {
    this.FLangType = this.cds.gLangType;
    this.FLang = this.cds.gLang;
    if (typeof this.FLang.F03.F0009 == 'string') {
      this.FLang.F03.F0009 = this.FLang.F03.F0009.split('$');
    }
    if (!this.cds.xinsuranceChangingData) {
      this.go.navigate(['member']);
    }
  }
  FUserInfo: UserInfo;
  FLangType: string;

  FStep = '1';
  FinsuranceChangingData: EndRsmntCard;
  FLang: any;
  Ftoday: any;
  /** 此方案限定的年齡上限 */
  FAge: number;

  // Ftoday = new Date();
  // FmaxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
  FmaxDate = moment().add(1, 'year');
  FthisYear = moment().add(1, 'year').format('YYYY');
  /** 時間序列 */
  FTimelist: Array<any> = [];
  /** 表單群組 */
  group: FormGroup;
  /** 表單控制項 */
  controls;

  FDate: string;
  FReadonly: boolean;
  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }
  ngOnInit(): void {
    this.FUserInfo = this.cds.userInfo;

    moment().get('hours') >= 17
      ? (this.Ftoday = moment().add(1, 'days'))
      : (this.Ftoday = moment());

    // this.FinsuranceChangingData = this.cds.xinsuranceChangingData;
    this.FinsuranceChangingData = this.cds.xinsuranceChangingData || {
      prod_pol_code: 'AC01001',
      pol_dt: new Date(moment().format('YYYY-MM-DD')),
      pol_due_dt: new Date(moment().add(5, 'days').format('YYYY-MM-DD')),
      pol_no: 'CI20CB0028916',
      cntr_no: '200101809004',
    };

    this.cds.xinsuranceChangingData = null;
    this.FinsuranceChangingData.pol_days = moment(
      this.FinsuranceChangingData.pol_due_dt
    ).diff(this.FinsuranceChangingData.pol_dt, 'days');

    this.route.queryParams.subscribe((params) => {
      if (params.step) {
        this.FStep = params.step;
      }
    });
    this.formValidate();

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }
  /** 表單控制項 */
  formValidate() {
    const validators = {
      pol_dt_new: [null],
      pol_due_dt_new: [null],
      pol_time: [null],
    };
    this.group = this.fb.group(validators, { updateOn: 'blur' });
    this.controls = this.group.controls;
  }
  async goStep(position) {
    switch (position) {
      case 'center':
        this.go.navigate(['member']);
        break;
      case '2':
        // let age80: any = new Date(this.cds.userInfo.birthday + ' 00:00:00');
        let age80: any = moment(this.cds.userInfo.birthday, 'YYYY-MM-DD');

        // 國外旅遊  AC01001 (T1)
        // 銀(年齡6周~80歲)W7
        // 金(年齡6周~80歲)W1
        // 鑽石(年齡18-70)W2
        // 方案
        // AC01005  - AC01013
        // 銀(年齡6周~80歲) (T3)
        // 金(年齡6周~80歲)(T4)
        // 鑽石(年齡6周~80歲) (T4)
        // 方案只會向後變更，無須檢核 周歲
        this.FAge =
          (this.FinsuranceChangingData.prod_pol_code === 'AC01001' &&
            this.FinsuranceChangingData.tral_proj === 'W2') ||
          (this.FinsuranceChangingData.prod_pol_code === 'AC01005' &&
            this.FinsuranceChangingData.tral_proj === 'V2')
            ? 70
            : 80;

        /** 因保單生日判別需 小於且不等於 歲數 因此-1天判定  */
        age80 = age80.add(this.FAge, 'years').subtract(1, 'days');

        // compareSpecificTime3
        this.group.get('pol_dt_new').setValidators([
          Validators.required,
          this.validatorsService.compareSpecificTime(
            this.group.get('pol_dt_new').value,
            // new Date(),
            moment(),
            '>=',
            'hours',
            6
          ),
          this.validatorsService.compareSpecificTime(
            this.group.get('pol_dt_new').value,
            this.FinsuranceChangingData.pol_dt,
            '!=',
            '',
            0
          ),
          this.validatorsService.compareSpecificTime(
            this.group.get('pol_dt_new').value,
            age80,
            '<=',
            '',
            0
          ),
        ]);

        this.group.get('pol_dt_new').updateValueAndValidity();
        this.group.get('pol_time').setValidators([Validators.required]);
        this.group.get('pol_time').updateValueAndValidity();

        if (this.group.valid) {
          this.FStep = '2';
          const CheckForPolicy: IResponse = await this.apiPolicy.CheckForPolicy(
            {
              language: this.FLangType,
              token: this.cds.gToken,
              client_target: this.FUserInfo.certificate_number_9
                ? this.FUserInfo.certificate_number_9
                : this.FUserInfo.certificate_number_12,
              aply_no: '',
              type_pol: 'B',
              cntr_no: this.FinsuranceChangingData.cntr_no,
              upl_quty_code: 'A3',
              pol_dt_new: moment(this.controls.pol_dt_new.value).format(
                'yyyy-MM-DD HH:mm:ss'
              ),
              pol_due_dt_new: moment(this.controls.pol_due_dt_new.value).format(
                'yyyy-MM-DD HH:mm:ss'
              ),
            }
          );
          if (CheckForPolicy.status !== 200 && CheckForPolicy.status !== 999) {
            switch (true) {
              default:
                this.alter.open({
                  type: 'warning',
                  title: this.FLang.F03.F0028,
                  content: this.FLang.F03.F0029,
                });
            }
            this.FStep = '1';
          }
        } else {
          this.group.markAllAsTouched();
          const pol_dt_new_errors = this.group.get('pol_dt_new').errors;
          if (pol_dt_new_errors) {
            if (pol_dt_new_errors.compareSpecificTime4) {
              this.alter.open({
                type: 'warning',
                title: this.FLang.F03.F0026,
                content: this.FLang.F03.F0027,
              });
            }
          }
        }
        break;
      case '3':
        const SaveDataForPolicy: IResponse = await this.apiPolicy.SaveDataForPolicy(
          {
            member_seq_no: this.FUserInfo.seq_no,
            aply_no: '',
            type_pol: 'B',
            apc_certificate_type: '1', // 沒有來源可以取得
            apc_certificate_number: this.FUserInfo.certificate_number_9
              ? this.FUserInfo.certificate_number_9
              : this.FUserInfo.certificate_number_12,
            apc_customer_name: this.FUserInfo.customer_name.trim(),
            apc_email: this.FUserInfo.email_bk,
            apc_phone_no: this.FUserInfo.mobile,
            cntr_no: this.FinsuranceChangingData.cntr_no,
            upl_quty_code: 'A3',
            prod_pol_code: this.FinsuranceChangingData.prod_pol_code,
            pol_dt_new: moment(this.controls.pol_dt_new.value).format(
              'yyyy-MM-DD HH:mm:ss'
            ),
            pol_due_dt_new: moment(this.controls.pol_due_dt_new.value).format(
              'yyyy-MM-DD HH:mm:ss'
            ),
          }
        );
        if (SaveDataForPolicy.status != 200) {
          this.alter.open({
            type: 'warning ',
            title: this.FLang.F03.F0028,
            content: this.FLang.F03.F0029,
          });
        } else {
          this.FStep = '3';
        }
        break;
    }
  }
  /** 更新保險期間 hours */
  updatePolDtTimes(i) {
    this.group.get('pol_dt_new').value &&
      this.group
        .get('pol_dt_new')
        .patchValue(
          moment(this.group.get('pol_dt_new').value).hours(i).toDate()
        );
    this.group.get('pol_due_dt_new').value &&
      this.group
        .get('pol_due_dt_new')
        .patchValue(
          moment(this.group.get('pol_due_dt_new').value).hours(i).toDate()
        );
    this.group.get('pol_time').patchValue(i);
  }
  counter(i: number) {
    return new Array(i);
  }
  getDate(e: any) {
    if (e) {
      let date;
      if (e && e.indexOf('/') !== -1) {
        date = this.datePipe.transform(e, 'yyyy-MM-dd');
      } else {
        date = e;
      }

      this.group.get('pol_dt_new').patchValue(date);
      this.group
        .get('pol_due_dt_new')
        .patchValue(
          moment(date)
            .add(this.FinsuranceChangingData.pol_days, 'days')
            .toDate()
        );

      if (this.group.get('pol_time').value) {
        this.updatePolDtTimes(this.group.get('pol_time').value);
        this.group
          .get('pol_time')
          .setValidators([
            Validators.required,
            this.validatorsService.compareSpecificTime(
              this.group.get('pol_dt_new').value,
              new Date(),
              '>',
              'hours',
              6
            ),
          ]);
        this.group.get('pol_time').updateValueAndValidity();
      }
      this.group.get('pol_dt_new').markAsTouched();

      /** 重製timepick */
      const today = moment().format('YYYY-MM-DD');
      const tomorrow = moment().add(1, 'days').format('YYYY-MM-DD');
      const startDay = moment(this.controls.pol_dt_new.value).format(
        'YYYY-MM-DD'
      );
      const mGetMinutes = moment().get('minutes');
      const mGetSeconds = moment().get('seconds');
      let nowHH;
      // 如果購買時剛好是整點，則時間加 6
      if (mGetMinutes === 0 && mGetSeconds === 0){
        nowHH = moment().get('hours') + 6;
      }else{
        nowHH = moment().get('hours') + 7;
      }
      let mindex: any;
      // 當天
      if (today === startDay) {
        mindex = nowHH;
        this.gernerateTimepick(mindex);
      }
      // 隔天
      else if (tomorrow === startDay) {
        mindex = 0;
        if (24 - nowHH <= 0) {
          mindex = nowHH - 24;
        }
        this.gernerateTimepick(mindex);
      } else {
        this.gernerateTimepick(0);
      }
    }
  }

  gernerateTimepick(xindex) {
    this.FTimelist = [];
    for (let i = xindex; i < 24; i++) {
      this.FTimelist.push(i);
    }
  }

  /** 取得錯誤訊息*/
  getErrors(controls: AbstractControl) {
    //  錯誤訊息先寫死在這，等有統整多國語系時再更改
    if (controls) {
      const target = Object.keys(controls.errors)[0];
      console.log('target');
      console.log(target);
      switch (target) {
        case 'required':
          return this.FLang.F03.F0030;
        case 'compareSpecificTime4': // 保險起訖日期時間需大於+6小時後
          return this.FLang.F03.F0031;
        case 'compareSpecificTime6': // 變更後的日期時間不可與原日期時間相同
          return this.FLang.F03.F0032;
        case 'compareSpecificTime3': // 變更後的開始日期時間不可大於會員生日80歲
          const mStr = this.FLang.F03.F0033.split('$');
          return `${mStr[0]}${this.FAge}${mStr[1]}`;
        default:
          return '';
      }
    }
    return '';
  }
  // get totalPeriod() {
  //   // let rawString = this.FLang.F03.F0009;
  //   // const alter = [
  //   //   this.FinsuranceChangingData.pol_days || '',
  //   //   this.datePipe.transform(
  //   //     this.controls.pol_due_dt_new.value,
  //   //     'dd/MM/yyyy'
  //   //   ) || '',
  //   //   this.controls.pol_time.value || this.controls.pol_time.value == '0'
  //   //     ? this.controls.pol_time.value + ':00'
  //   //     : '',
  //   // ];
  //   // alter.forEach((value, i) => {
  //   //   rawString = rawString.replace(`$${i + 1}`, value);
  //   // });
  //   return rawString;
  // }
  paddingLeft(i) {
    return CommonService.paddingLeft(i, 2);
  }
}
