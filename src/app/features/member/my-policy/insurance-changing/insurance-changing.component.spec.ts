import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceChangingComponent } from './insurance-changing.component';

describe('InsuranceChangingComponent', () => {
  let component: InsuranceChangingComponent;
  let fixture: ComponentFixture<InsuranceChangingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsuranceChangingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceChangingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
