import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyPolicyComponent } from './my-policy.component';

const routes: Routes = [{ path: '', component: MyPolicyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPolicyRoutingModule { }
