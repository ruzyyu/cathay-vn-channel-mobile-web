import { StorageKeys } from 'src/app/core/enums/storage-key.enum';
import { StorageService } from 'src/app/core/services/storage.service';
import { IdentityComponent } from './../../../shared/model/identity/identity.component';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { SocialService } from './../../../core/services/social.service';
import {
  UserInfo,
  ICounty,
  IProvince,
  IConfirm,
  IResponse,
} from './../../../core/interface/index';
import {
  Component,
  OnInit,
  ViewContainerRef,
  ViewChild,
  ComponentFactoryResolver,
} from '@angular/core';
import { Router } from '@angular/router';
import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { DatePipe } from '@angular/common';

import {
  Validators,
  FormBuilder,
  FormGroup,
} from '@angular/forms';

import { ValidatorsService } from 'src/app/core/services/utils/validators.service';

@Component({
  selector: 'app-maintain',
  templateUrl: './maintain.component.html',
  styleUrls: ['./maintain.component.scss'],
})
export class MaintainComponent implements OnInit {
  @ViewChild('identity', { read: ViewContainerRef }) identity: ViewContainerRef;
  constructor(
    private alert: AlertServiceService,
    private fb: FormBuilder,
    private apiMember: MemberService,
    private apiPublic: PublicService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private router: Router,
    private datePipe: DatePipe,
    private auth: SocialService,
    private componentFactory: ComponentFactoryResolver
  ) {
    this.FLang = this.cds.gLang;
  }

  /** 宣告區Start */
  FConfirm: IConfirm;
  userInfo: UserInfo;
  validators: {};
  group: FormGroup;
  controls;
  FCountyList: Array<ICounty>;
  FProvinceList: Array<IProvince>;
  FLang: any;
  comIdentity: any;
  FIdBind: string = undefined;
  /** 都用九碼 */
  FAddId = false;
  FBindData: string = undefined;
  FIdCheck: string = undefined;
  FIdError: string = undefined;
  FIsLinkId: string = undefined;

  FLangType = this.cds.gLangType;
  /** 省縣 */
  async getprovinceList() {
    // 縣市初始化
    if (!this.cds.gPubCommon.provinceList) {
      await this.apiPublic.gCountyList();
    }

    if (this.cds.gPubCommon && this.cds.gPubCommon.provinceList) {
      this.FProvinceList = this.cds.gPubCommon.provinceList
        ? this.cds.gPubCommon.provinceList
        : [];
    } else {
      this.FProvinceList = [];
    }

    if (this.FProvinceList.length > 0) {
      let mindex = Number(this.userInfo.province) - 1 || 0;
      if (mindex < 0) {
        mindex = 0;
      }
      this.FCountyList = this.FProvinceList[mindex].countys;
    }

    // 重新賦予資料
  }
  notifyCounty(event) {
    this.controls.county.setValue('');
    this.controls.county.markAsUntouched();
    const index = event.target.selectedIndex - 1;
    this.FCountyList = this.FProvinceList[index].countys;
  }

  get isSetpwd(): boolean {
    return this.group.controls.is_set_pw.value === 'Y';
  }
  get isfbLink(): boolean {
    return !!this.group.controls.fb_account.value;
  }
  get isgoogleLink(): boolean {
    return !!this.group.controls.google_account.value;
  }

  /** 宣告區End */

  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.fb.group(validator);
    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          mobile: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES006,
          email: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES007,
          email_bk:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES008,
          addr: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES009,
        }[control_key];
      }
      /** 新檢核end */
      return (
        {
          addr: this.FLang.F07.content.F0032,
          vnPhone: this.FLang.F07.content.F0033,
          email: this.FLang.F07.content.F0033,
          tenNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES002,
        }[type] || this.FLang.F07.content.F0033
      );
    }
    return this.FLang.F07.content.F0033;
  }

  showIdentity() {
    /** 動態元件區 */
    const component = this.componentFactory.resolveComponentFactory(
      IdentityComponent
    );
    this.identity.clear();
    this.comIdentity = this.identity.createComponent(component);
    // this.comIdentity.instance.data = {};

    $('#modal_Identity').modal('show');

    // 被保人資料填寫中
    this.comIdentity.instance.xAction.subscribe((item) => {
      this.cds.userInfo.is_link_id === 'N'
        ? (this.cds.userInfo.is_link_id = 'A')
        : (this.cds.userInfo.is_link_id = 'R');
      $('#modal_Identity').modal('hide');
      this.doIdBind();
    });
  }

  showChangePassword() {
    this.router.navigate(['login/change-password']);
  }
  /** 新增身分證 F07.2 */
  showIdentity_info() {
    this.alert.open({
      type: 'info',
      title: this.FLang.F07.content.F0034,
      content: this.FLang.F07.content.F0035,
    });
  }

  /** showConfirm 範例
   * @param Confirm interface
   * @param output ActionYes 成功動作 ActionNo 取消動作
   */
  showConfirm() {
    this.FConfirm = {
      title: '測試',
      content: '看看樣子',
      type: '666',
    };

    $('#lyConfirm').modal('show');
  }

  ActionYesNo(type) {
    console.log(type);
  }

  showSocialMedia_info() {
    this.alert.open({
      type: 'info',
      title: this.FLang.F07.content.F0049,
      content: this.FLang.F07.content.F0050,
    });
  }

  // 沒有修改API
  async doSubmit() {
    /**
     * 這邊要改成...先將control的資料更新給userinfo
     */

    const payload = {
      ...this.userInfo,
      email_bk: this.controls.email_bk.value,
      mobile: this.controls.mobile.value,
      county: this.controls.county.value,
      province: this.controls.province.value,
      addr: this.controls.addr.value,
    };

    const rep: any = await this.apiMember.MemberReg(payload);

    if (rep.status === 200) {
      // 修改成功後立刻重新查詢自己，不要回寫
      this.apiMember.reflashUserInfo(payload);

      this.alert.open({
        type: 'success',
        content: this.FLang.popup.P0002,
        callback: this.goMember,
      });
    }
  }

  /** 綁定成功才可執行 */
  goMember() {
    this.router.navigate(['member/']);
  }

  async fblogin() {
    const rep: any = await this.auth.signInWithFB();
    if (rep && rep.id) {
      const payload = {
        seq_no: this.userInfo.seq_no,
        fb_account: rep.id,
        fb_title: rep.name,
      };
      this.saveMember(payload, rep, 'fb');
    }
  }

  async googlelogin() {
    const rep: any = await this.auth.signInWithGoogle();
    if (rep && rep.id) {
      const payload = {
        seq_no: this.userInfo.seq_no,
        google_account: rep.id,
        google_title: rep.name,
      };
      this.saveMember(payload, rep, 'google');
    }
  }

  async saveMember(payload: any, xrep: any, xchannel: 'google' | 'fb' = 'fb') {
    console.log(payload);

    payload.is_link_sns = 'Y';
    const rep: IResponse = await this.apiMember.MemberBind(payload);
    if (rep.status === 200) {
      // 修改成功後立刻重新查詢自己，不要回寫

      // this.userInfo = this.cds.userInfo;
      if (xchannel === 'google') {
        this.controls.google_account.setValue(xrep.id);
        this.controls.google_title.setValue(xrep.name);
        this.cds.userInfo.google_account = xrep.id;
        this.cds.userInfo.google_title = xrep.name;
      } else {
        this.controls.fb_account.setValue(xrep.id);
        this.controls.fb_title.setValue(xrep.name);
        this.cds.userInfo.fb_account = xrep.id;
        this.cds.userInfo.fb_title = xrep.name;
      }
      this.apiMember.reflashSession();

      this.alert.open({
        type: 'success',
        content: this.FLang.B055.content.B0011,
      });
    }
  }

  async ngOnInit() {
    StorageService.removeItem(StorageKeys.c_user);
    this.userInfo = this.cds.userInfo;
    // 更換日期格式
    const mBirthday = this.datePipe.transform(
      this.userInfo.birthday,
      'dd/MM/yyyy'
    );

    /** 表單驗證項目 */

    this.validators = {
      customer_name: [this.userInfo.customer_name, []],
      email: [this.userInfo.email, [Validators.required, this.vi.email]],
      sex: [this.userInfo.sex, []],
      mobile: [this.userInfo.mobile, [Validators.required, this.vi.tenNumber]],
      birthday: [mBirthday, []],
      certificate_number_9: [this.userInfo.certificate_number_9, []],
      certificate_number_12: [this.userInfo.certificate_number_12, []],
      email_bk: [this.userInfo.email_bk, [Validators.required, this.vi.email]],
      addr: [
        this.userInfo.addr,
        [Validators.required, Validators.maxLength(240)],
      ],
      province: [this.userInfo.province, [Validators.required]],
      county: [this.userInfo.county, [Validators.required]],
      is_link_id: [this.userInfo.is_link_id, []],
      is_set_pw: [this.userInfo.is_set_pw, []],
      fb_account: [this.userInfo.fb_account, []],
      fb_title: [this.userInfo.fb_title, []],
      google_account: [this.userInfo.google_account, []],
      google_title: [this.userInfo.google_title, []],
    };

    this.formValidate();

    // 下拉清單
    this.getprovinceList();
    /** 身分證驗證判斷 */
    this.doIdBind();
  }
  /** 身分證驗證綁定 */
  doIdBind() {
    this.FAddId = false;
    this.FIsLinkId = this.cds.userInfo.is_link_id;
    if (this.cds.userInfo.is_link_id === 'Y') {
      // 顯示12碼input 印出9碼綁定內容
      this.FIdBind = '12';
      this.FBindData = this.cds.userInfo.certificate_number_9;
    }
    if (this.cds.userInfo.is_link_id === 'N') {
      this.FAddId = true;
      // 顯示 12碼 input 或者 9碼 input
      if (this.cds.userInfo.certificate_number_12) {
        this.FIdBind = '12';
      }
      if (this.cds.userInfo.certificate_number_9) {
        this.FIdBind = '9';
      }
    }
    if (
      this.cds.userInfo.is_link_id === 'A' ||
      this.cds.userInfo.is_link_id === 'R'
    ) {
      this.FAddId = false;
      this.FIdError = undefined;
      if (this.cds.userInfo.certificate_number_12) {
        this.FIdBind = '12';
        this.FIdCheck = '9';
      }
      if (this.cds.userInfo.certificate_number_9) {
        this.FIdBind = '9';
        this.FIdCheck = '12';
      }
    }
    if (this.cds.userInfo.is_link_id === 'F') {
      if (this.cds.userInfo.certificate_number_12) {
        this.FIdBind = '12';
        this.FIdError = '9';
      }
      if (this.cds.userInfo.certificate_number_9) {
        this.FIdBind = '9';
        this.FIdError = '12';
      }
    }
  }
  /** 測試區 */

  del() {
    StorageService.removeItem(StorageKeys.c_user);
  }
}
