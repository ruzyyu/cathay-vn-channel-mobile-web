import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintainRoutingModule } from './maintain-routing.module';
import { MaintainComponent } from './maintain.component';

// plugin
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [MaintainComponent],
  imports: [CommonModule, MaintainRoutingModule, SharedModule],
})
export class MaintainModule {}
