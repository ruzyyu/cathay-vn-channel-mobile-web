import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../core/services/common.service';
import { PolicyService } from '../../../core/services/api/policy/policy.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-policy-download',
  templateUrl: './my-policy-download.component.html',
  styleUrls: ['./my-policy-download.component.scss'],
})
export class MyPolicyDownloadComponent implements OnInit {
  constructor(
    private api: PolicyService,
    private go: Router
  ) {
    if (!this.getLocal) {
      this.go.navigate(['member']);
    }
  }

  ngOnInit(): void {
    this.getPolicyPdf();
  }

  getLocal() {
    const language = localStorage.getItem('SavePolicy_language');
    const client_target = localStorage.getItem('SavePolicy_client_target');
    const token = localStorage.getItem('SavePolicy_token');
    const cntr_no = localStorage.getItem('SavePolicy_cntr_no');
    const type_pol = localStorage.getItem('SavePolicy_type_pol');
    const payload = {
      language,
      client_target,
      token,
      cntr_no,
      type_pol,
    };
    return payload;
  }

  removeLocal() {
    localStorage.removeItem('SavePolicy_language');
    localStorage.removeItem('SavePolicy_client_target');
    localStorage.removeItem('SavePolicy_token');
    localStorage.removeItem('SavePolicy_cntr_no');
    localStorage.removeItem('SavePolicy_type_pol');
  }

  async getPolicyPdf() {
    this.showMask();
    const res = await this.api.getPolicyPdf(this.getLocal());
    if (res.status === 200) {
      this.removeLocal();
      const policy_pdf = res.data.policy_pdf;
      location.replace (CommonService.base64ToBlobUrl(
        policy_pdf,
        'application/pdf'
      ));
      this.hideMask();
    }else{
      window.close();
    }
  }

  showMask() {
    $('body').append(
      `<div class='preloader' id='download_load'><div class=\"lds-default\"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>`
    );
  }

  hideMask() {
    $('#download_load').remove();
  }


}
