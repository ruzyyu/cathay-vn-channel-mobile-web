import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyPolicyDownloadRoutingModule } from './my-policy-download-routing.module';
import { MyPolicyDownloadComponent } from './my-policy-download.component';


@NgModule({
  declarations: [MyPolicyDownloadComponent],
  imports: [
    CommonModule,
    MyPolicyDownloadRoutingModule
  ]
})
export class MyPolicyDownloadModule { }
