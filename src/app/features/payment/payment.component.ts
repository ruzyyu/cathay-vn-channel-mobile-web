import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MemberService } from './../../core/services/api/member/member.service';
import { DatapoolService } from './../../core/services/datapool/datapool.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import * as shajs from 'js-sha256';
import * as moment from 'moment';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private cds: DatapoolService,
    private api: MemberService,
    private http: HttpClient
  ) {}
  // 嵌入付款
  FUrlTest = '';
  ngOnInit(): void {
    console.log(window.opener);
    if (window.opener) {
      window.opener.location = window.location;
      window.close();
    }
    this.route.queryParams.subscribe((queryParams) => {
      /**
       * @deprecated
       * 接收支付訊息 測試中
       */
      console.log('接收支付訊息 測試中');
      console.log(queryParams);
      // http://sandbox1.vtcebank.vn/pay.vtc.vn/cong-thanh-toan/checkout.html?amount=10000
      // &bill_to_email=kuraki5336@gmail.com&bill_to_phone=0911756824&currency=VND
      // &url_return=http://127.0.0.1:4200/payment&payment_type=5&receiver_account=0963465816
      // &reference_number=Aa@123456&sercurityKey=Grsdf34lD@FSkj2l34kjldfoi@#%$^qwepzpoaisd#^uoproe353
      // &transaction_type=sale&website_id=4155&signature=2FCDD8E3F733B0053D60DCAEA4429D000CE33E6DA7B9318FDBEB209FF8E1660C
    });
  }

  testPay() {
    const sha256 = shajs.sha256;
    const securityKey = '@QAZ2wsx3edc4rfv';

    const destinationUrl =
      'http://alpha1.vtcpay.vn/portalgateway/checkout.html';
    const payload = {
      amount: '10000',
      currency: 'VND',
      receiver_account: '0963465816',
      reference_number: 'Aa@123456',
      transaction_type: 'sale',
      url_return: 'http://127.0.0.1:4200/payment',
      website_id: '182421',
      signature: undefined,
    };

    const arrPayload = Object.keys(payload).map((key) => payload[key]);
    const plaintext = `${arrPayload.join('|')}${securityKey}`;
    console.log(plaintext);

    payload.signature = sha256(plaintext).toUpperCase();
    payload.url_return = encodeURIComponent(payload.url_return);

    let mstr = '';
    Object.keys(payload).forEach((item, index) => {
      mstr = mstr + (index === 0 ? '?' : '&') + `${item}=${payload[item]}`;
    });
    this.FUrlTest = `${destinationUrl}${mstr}`;
    console.log(this.FUrlTest);

    window.open(this.FUrlTest, 'VNPay', 'height=500,width=500');
  }

  async testAES() {
    // website_id=627&amount=10000&receiver_account=0986699480&reference_number=20191015145145&currency=VND
    // &signature=C0106B11227909062ED10AED9C66258327859DDCB26AB3B69991F91E739D016A&transaction_type=sale
    // website_id=627&amount=10000&receiver_account=0986699480&reference_number=20191015145145&currency=VND
    // &signature=C0106B11227909062ED10AED9C66258327859DDCB26AB3B69991F91E739D016A&transaction_type=sale

    // website_id=627&amount=10000&receiver_account=0986699480&reference_number=20191015145145&currency=VND&signature=C0106B11227909062ED10AED9C66258327859DDCB26AB3B69991F91E739D016A&transaction_type=sale

    // C0106B11227909062ED10AED9C66258327859DDCB26AB3B69991F91E739D016A
    // B3F4B52892F01934ED557EEA6C3D9A558897C7BA24A43C996BCD9A45D78AF467
    // const a = '10000|VND|0986699480|20191015145145|sale|627|@QAZ2wsx3edc4rfv';
    const a = '627|10000|0986699480|20191015145145|VND|sale|!QAZ2wsx3edc4rfv';
    const sha256 = shajs.sha256;
    console.log(sha256(a).toUpperCase());
  }

  async getsso() {
    const param = new HttpParams().set(
      'param',
      'eyJDT1JFU1NZU1VTRVJJRCI6IjAwMDAwMDA0MDEiLCJDUkVBVEVUUyI6MTYxMTEzOTE4Mjc4OX0='
    );
    this.http
      .get(`${this.cds.cathaylifesso}ZSWeb/api/getssomember`, {
        params: param,
      })
      .subscribe();
  }

  test228() {
    const mmdd = moment('2020-02-29').format('MMDD');
    let FData = moment('2020-02-29').add(1, 'years');
    if (mmdd === '0229') {
      FData = FData.add(1, 'day');
    }
    console.log(FData.format('YYYY-MM-DD'));
  }

  windowsopen() {
    setTimeout(() => {
      const win = window.open('https://google.com');
    }, 100);
  }

  datadiff() {
    const startTime = moment();
    setTimeout(() => {
      const endTime = moment();
      // console.log(seconds);
    }, 3000);
  }
}
