import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BindAccountRoutingModule } from './bind-account-routing.module';
import { BindAccountComponent } from './bind-account.component';

import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [BindAccountComponent],
  imports: [CommonModule, BindAccountRoutingModule, SharedModule],
})
export class BindAccountModule {}
