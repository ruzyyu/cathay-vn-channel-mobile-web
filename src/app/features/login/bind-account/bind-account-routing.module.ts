import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BindAccountComponent } from './bind-account.component';

const routes: Routes = [{ path: '', component: BindAccountComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BindAccountRoutingModule { }
