import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MemberRegisteridRoutingModule } from './register-id-routing.module';
import { MemberRegisteridComponent } from './register-id.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    MemberRegisteridComponent,
  ],
  imports: [
    CommonModule,
    MemberRegisteridRoutingModule,
    FormsModule,
    SharedModule,
  ],
})
export class MemberRegisteridModule {}
