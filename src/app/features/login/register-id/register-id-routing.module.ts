import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MemberRegisteridComponent } from './register-id.component';

const routes: Routes = [{ path: '', component: MemberRegisteridComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRegisteridRoutingModule { }
