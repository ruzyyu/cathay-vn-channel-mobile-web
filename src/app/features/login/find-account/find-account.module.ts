import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FindAccountRoutingModule } from './find-account-routing.module';
import { FindAccountComponent } from './find-account.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [FindAccountComponent],
  imports: [CommonModule, FindAccountRoutingModule, SharedModule],
})
export class FindAccountModule {}
