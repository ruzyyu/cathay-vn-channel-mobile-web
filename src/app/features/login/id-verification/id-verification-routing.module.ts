import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IdVerificationComponent } from './id-verification.component';

const routes: Routes = [{ path: '', component: IdVerificationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IdVerificationRoutingModule { }
