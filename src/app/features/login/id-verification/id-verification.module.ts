import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IdVerificationRoutingModule } from './id-verification-routing.module';
import { IdVerificationComponent } from './id-verification.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [IdVerificationComponent],
  imports: [
    CommonModule,
    IdVerificationRoutingModule,
    FormsModule,
    SharedModule,
  ],
})
export class IdVerificationModule {}
