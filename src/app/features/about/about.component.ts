import { DatapoolService } from './../../core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { IAboutMap } from 'src/app/core/interface';
import { fromEvent } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';
import { MetaService } from 'src/app/core/services/meta.service';
import { jobDetail } from 'src/app/data/job';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  constructor(
    private cds: DatapoolService,
    titleService: Title,
    private router: Router,
    private metaData: MetaService,
    private metaService: Meta
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;

    /** 處理footer 錨點事件 */
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.router.url === '/about#map') {
          const map = document.querySelector('#map');
          this.scroll(map);
        }
      }
    });

    /** Opening Card List */
    this.openingCard = jobDetail;
  }
  /** 網頁標題 */
  title = '';

  filterTypeId: string;
  language: string;

  /** Map Mark List */
  mapList: IAboutMap[];

  marqueeShow: boolean;

  // 變數宣告
  FLang: any;
  FLangType: string;
  openingCard: any[];

  // 用來存放初始高度
  firstHeight: any = [];

  /** 取得標題
   * title: 標題
   */
  getTitle(title) {
    this.title = title;
  }
  ngOnInit() {
    /** 判斷若滑鼠滾動則將預設定位點的高度移除 */
    fromEvent(window, 'scroll').subscribe((event) => {
      if (event) {
        if (document.body.clientWidth >= 992) {
          this.firstHeight.push(document.documentElement.scrollTop);
          if (document.documentElement.scrollTop < this.firstHeight[0]) {
            $('.map-height').css('display', 'none');
          } else {
            $('.map-height').css('display', 'block');
          }
        } else {
          $('.map-height').css('display', 'none');
        }
      }
    });

    this.mapList = [
      {
        // 公司總部
        type: 'b',
        title: this.FLang.G01.L0008,
        p1: `46-48-50, Phạm Hồng Thái, P. Bến Thành, Q.1`,
        p2: `${this.FLang.G01.L0015}: 028 6288 8385`,
        p3: `${this.FLang.G01.L0016}: 028 6256 6886`,
        p4: `${this.FLang.G01.L0017}: 0933 555 920`,
        iframeSrc:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.518657354346!2d106.69244461494175!3d10.771531162229142!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f3e90813389%3A0xe5b54e296b1ce6bf!2sT%C3%B2a%20Nh%C3%A0%20The%20World%20Center!5e0!3m2!1szh-TW!2stw!4v1611296058369!5m2!1szh-TW!2stw',
        position: {
          lat: 10.77152,
          lng: 106.69463,
        },
      },
      {
        // 河內分部
        type: 'c',
        title: this.FLang.G01.L0009,
        p1: `Lầu 16, Ngọc Khánh Plaza, Số 01 Phạm Huy Thông, Q.Ba Đình, TP.Hà Nội`,
        p2: `${this.FLang.G01.L0015}: 024 6325 2333`,
        p3: `${this.FLang.G01.L0016}: 024 6278 2922`,
        p4: `${this.FLang.G01.L0017}: 0938 999 549`,
        // 待檢查
        iframeSrc:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.159924587785!2d105.80881601502941!3d21.02628609323837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab692f96b351%3A0x75ec046a648e47ee!2zTOG6p3UgMTYsIDEgUGjhuqFtIEh1eSBUaMO0bmcsIE5n4buNYyBLaMOhbmgsIEJhIMSQw6xuaCwgSMOgIE7hu5lpLCDotorljZc!5e0!3m2!1szh-TW!2stw!4v1611296232531!5m2!1szh-TW!2stw',
        position: {
          lat: 21.02666,
          lng: 105.81134,
        },
      },
      {
        // 同奈
        type: 'd',
        title: this.FLang.G01.L0010,
        p1: `Lầu 3B, Tòa nhà Aurora, 253 Phạm Văn Thuận, P.Tân Mai, H.Biên Hòa, tỉnh Đồng Nai`,
        p2: `${this.FLang.G01.L0015}: 025 1368 3993`,
        p3: ``,
        p4: ``,
        // 待檢查
        iframeSrc:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3917.077107243953!2d106.84258781494269!3d10.957548058820347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174ddc4c66e4255%3A0xd404cd42d8913ed8!2sAURORA%20HOTEL%20PLAZA!5e0!3m2!1szh-TW!2stw!4v1611296293373!5m2!1szh-TW!2stw',
        position: {
          lat: 16.05971,
          lng: 108.21125,
        },
      },
      {
        // 芹苴
        type: 'e',
        title: this.FLang.G01.L0011,
        p1: `Lầu 4, Tòa nhà Hòa Bình,  Số 14-16B Đại Lộ Hòa Bình, P.An Cư, Q.Ninh Kiều, TP.Cần Thơ`,
        p2: `${this.FLang.G01.L0015}: 029 2626 8855`,
        p3: ``,
        p4: ``,
        // 待檢查
        iframeSrc:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3928.7923125645775!2d105.78255271493856!3d10.03399012520083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a062a04cc93261%3A0x5162b34d879c5da7!2zSMOyYSBCw6xuaCBCdWlsZGluZw!5e0!3m2!1szh-TW!2stw!4v1611296392925!5m2!1szh-TW!2stw',
        position: {
          lat: 10.03399,
          lng: 105.78479,
        },
      },
      {
        // 峴港
        type: 'f',
        title: this.FLang.G01.L0012,
        p1: `Lầu 12, Thành Lợi Building, Số 01-03 đường Lê Đình, P.Vĩnh Trung, Q.Thanh Khê, TP.Đà Nẵng`,
        p2: `${this.FLang.G01.L0015}: 023 6655 9199`,
        p3: ``,
        p4: ``,
        // 待檢查
        iframeSrc:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2230.9290962595305!2d108.21054624889493!3d16.059379972367132!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6abd983cc7674ed8!2zVGjDoG5oIEzhu6NpIEJ1aWxkaW5nIMSQw6AgTuG6tW5n!5e0!3m2!1szh-TW!2stw!4v1611542549617!5m2!1szh-TW!2stw',
        position: {
          lat: 10.95772,
          lng: 106.8451,
        },
      },
      {
        // 海防
        type: 'g',
        title: this.FLang.G01.L0013,
        p1: `Lầu 4, số 62-64 Tôn Đức Thắng, Q.Lê Chân, Hải Phòng`,
        p2: `${this.FLang.G01.L0015}: 022 5628 3737`,
        p3: ``,
        p4: ``,
        iframeSrc:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3728.5639859712082!2d106.6670636150273!3d20.849306099256015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314a7a648f4c8579%3A0xf468067eaae2eabc!2zTOG6p3UgNCwgc-G7kSA2MiwgNjQgVMO0biDEkOG7qWMgVGjhuq9uZywgVHLhuqduIE5ndXnDqm4gSMOjbiwgTMOqIENow6JuLCBI4bqjaSBQaMOybmcsIOi2iuWNlw!5e0!3m2!1szh-TW!2stw!4v1611296499468!5m2!1szh-TW!2stw',
        position: {
          lat: 20.84946,
          lng: 106.66926,
        },
      },
    ];
  }

  /** 將畫面導向指定元素 */
  scroll(el) {
    setTimeout(() => {
      el.scrollIntoView();
    });
  }

  /** 調整跑馬燈高度 */
  adjustNewMessage() {
    // 判斷new_msg調整元素 transform
    const newMasgTargets = '.com-content, .menu-mob';
    const hasNewsMsg = $('.news-message').length;
    const isNewsMsgHidden = $('.news-message').is(':hidden');
    if (hasNewsMsg && !isNewsMsgHidden) {
      this.marqueeShow = true;
    } else {
      this.marqueeShow = false;
    }
    setTimeout(() => {
      $(this).scrollTop(0);
    }, 100);

    this.close_newMsg();
  }

  close_newMsg() {
    $('.com-content, .menu-mob').removeClass('alert-show-trans');
  }
}
