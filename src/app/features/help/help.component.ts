import { CaptchaComponent } from './../../shared/components/captcha/captcha.component';
import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  IFaq,
  IFaqTag,
  IHelpDropdownItem,
  UserInfo,
} from 'src/app/core/interface';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';
import { Lang } from 'src/types';
import { SendEmailService } from 'src/app/core/services/api/help/send-email.service';
import { CommonService } from 'src/app/core/services/common.service';
import { read } from 'node:fs';
@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
})
export class HelpComponent implements OnInit, AfterViewInit {
  constructor(
    private numberPipe: NumberPipe,
    private fb: FormBuilder,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private route: ActivatedRoute,
    private sendMailAPi: SendEmailService,
    private router: Router
  ) {
    this.FLang = this.cds.gLang;
    this.FLangB054 = this.cds.gLang.B054;

    this.insuredList = [
      {
        type: 1,
        option: this.FLang.H01.H0025_.s001_.p001,
      },
      {
        type: 2,
        option: this.FLang.H01.H0025_.s001_.p002,
      },
      {
        type: 3,
        option: this.FLang.H01.H0025_.s001_.p003,
      },
      {
        type: 4,
        option: this.FLang.H01.H0025_.s001_.p004,
      },
      {
        type: 5,
        option: this.FLang.H01.H0025_.s001_.p005,
      },
      {
        type: 6,
        option: this.FLang.H01.H0025_.s001_.p006,
      },
    ];

    this.optionList = [
      { type: 1, title: this.FLang.H01.H0025_.s001 },
      { type: 2, title: this.FLang.H01.H0025_.s002 },
      { type: 3, title: this.FLang.H01.H0025_.s003 },
      { type: 4, title: this.FLang.H01.H0025_.s004 },
      { type: 5, title: this.FLang.H01.H0025_.s005 },
      { type: 6, title: this.FLang.H01.H0025_.s006 },
      { type: 7, title: this.FLang.H01.H0025_.s007 },
    ];

    this.setDropdownList();

    this.initFaqList();

    /** 商品頁面 我要理賠各險介紹頁 更多問題 跳轉取值 */
    this.route.queryParams.subscribe((queryParams) => {
      this.tag = queryParams.tag;
      this.option = queryParams.option;
      // 專屬我要理賠各險介紹頁
      this.optionDetail = queryParams.optionDetail;
      if (queryParams && queryParams.type) {
        // 左側個人商品 leftTitle = 1 企業商品 leftTitle = 2
        switch (queryParams.type) {
          case 'question-personal':
            this.leftTitle = 1;
            break;
          case 'question-business':
            this.leftTitle = 2;
            break;
          case 'download-personal':
            this.leftTitle = 1;
            break;
          case 'download-business':
            this.leftTitle = 2;
            break;
          case 'info':
            break;
        }
      }
      if(queryParams && queryParams.type === 'info' && queryParams.from === 'apply'){
        this.isFromJob = true;
      }else{
        this.isFromJob = false;
      }

      if(queryParams && queryParams.type === 'question'){
        this.controls.option.reset();
        this.controls.optiontype.reset();
        this.applyJob = undefined;
      }
    });

    if (this.tag && this.option) {
      /** 商品頁面 更多問題 左邊menu顯示 */
      // 取大類
      const tag = this.dropdownList.filter(
        (types) => types.tag === this.tag
      )[0];
      // 取小類
      const option = tag?.content.filter(
        (options) => options.name === this.option
      )[0];

      // 大類
      this.leftTitle = tag.id;
      // 小類
      this.leftTag = option.type;
      // 小類項目
      if (this.optionDetail) {
        this.activeTag = 2;
      }
      // 下拉選單顯示
      this.collapseClose(tag.id);
      this.collapseShow(tag.id);
    }
  }

  // 常見問題 menu 標題
  get menuTitle() {
    const { dropdownList, leftTag, leftTitle } = this;
    return (
      dropdownList
        .find((item) => item.id === leftTitle)
        ?.content.find((item) => item.type === leftTag).title || ''
    );
  }

  /** 手機版下拉式選單顯示標籤 */
  get dropdownLabel() {
    const { dropdownList, leftTag, leftTitle } = this;
    return (
      dropdownList
        .find((item) => item.id === leftTitle)
        ?.content.find((item) => item.type === leftTag)?.title || ''
    );
  }

  /*************
   * 常見問題相關
   *************/

  /** 目前商品種類的 FAQ 列表 */
  get currentTitleFaqList() {
    let tag = 1;
    let type = 1;

    if (this.leftTag && this.leftTitle) {
      tag = this.leftTitle;
      type = this.leftTag;
    }

    return this.faqList.filter((faq) => faq.tag === tag && faq.type === type);
  }

  /** Faq 列表 - 搜尋後結果 */
  get faqListFilterByKeyword() {
    return this.currentTitleFaqList.filter((faq) => {
      const keyword = this.searchKeyword;
      return keyword
        ? faq.title.includes(keyword) || faq.content.includes(keyword)
        : faq;
    });
  }

  get faqListFilterBySubType() {
    const list = this.faqListFilterByKeyword;

    return this.activeTag
      ? list.filter(({ subType }) => subType === this.activeTag)
      : list;
  }

  /** 常見問題 問題filter */
  get filterFaqList() {
    return this.faqListFilterBySubType.slice(0, this.question);
  }

  get isMoreButtonDisabled() {
    return this.faqListFilterBySubType <= this.filterFaqList;
  }
  /** 圖形驗證碼產出結果 */
  captchaResult = '';
  question = 5;
  showImg = 'bg1';
  activeTag = null;
  leftTag = 1;
  /** 商品類別 */
  leftTitle = 1;
  clickNumber = 0;

  currentBookmarkTab = 1;

  validators: {};
  userInfo: UserInfo;
  group: FormGroup;
  controls;
  FLang: Lang;
  FLangB054: Lang['B054'];
  faqList: IFaq[];
  dropdownList: IHelpDropdownItem[];
  insuredList: any;
  optionList: any;

  /** 下載的選擇記憶 */
  // FDownloadIndex: number;

  tagId = 'all';

  applyJob: string;

  /** 網頁標題 */
  title = '';

  /** 商品頁面 跳轉 */
  // 大類
  tag: string;
  // 小類
  option: string;
  // 小類細項目
  optionDetail: number;
  // 下拉選單
  collapseNumber: number;

  /** 常見問題搜尋內容文字 */
  searchText = '';

  searchKeyword: string;

  menuTagList: IFaqTag[];

  // 大類類型 for 後端使用
  emailType: string;

  // 存放'點擊其他文件'後 狀態碼  需做resetStatus
  clickOther: boolean;

  // 判斷是否來自應徵
  isFromJob: boolean;

  /**  滑動元件的參數 */
  slideConfig = {
    infinite: false,
    variableWidth: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 767.98,
        settings: {
          infinite: false,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 575.98,
        settings: {
          slidesToShow: 1,
          infinite: false,
          centerMode: true,
        },
      },
    ],
  };

  @ViewChild('captcha', { read: CaptchaComponent })
  captcha: CaptchaComponent;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setSlickWidth(event.target.innerWidth);
  }

  ngOnInit(): void {
    this.userInfo = this.cds.userInfo;
    this.tagId = this.route.snapshot.queryParamMap.get('type');
    this.applyJob = this.route.snapshot.queryParamMap.get('from');

    // 我要應徵 → 寫信諮詢
    if (this.applyJob) {
      this.userInfo.optiontype = 5;
      this.userInfo.option = localStorage.getItem('jobContent');
      this.emailType = 'job';
    }

    /** 表單驗證項目 */
    this.validators = {
      name: [
        this.userInfo.customer_name,
        [
          Validators.required,
          this.vi.languagelength(30),
          this.vi.languageFormat,
        ],
      ],
      mobile: [this.userInfo.mobile, [Validators.required, this.vi.vnPhone]],
      email: [this.userInfo.email_bk, [Validators.required, this.vi.email]],
      // 意見類型 大類
      optiontype: [this.cds.userInfo.optiontype, [Validators.required]],
      // 意見類型 小類
      optiontypeDetail: [this.cds.userInfo.optiontypeDetail, []],
      // 詳細意見
      option: [
        this.cds.userInfo.option,
        [Validators.required, this.vi.languagelength(2000)],
      ],
      // 圖形驗證碼
      captchaCode: [
        this.cds.userInfo.captchaCode,
        [Validators.required, this.vi.captchaCheck(this.captchaResult)],
      ],
    };

    this.formValidate();

    $('.checkbox-group').click(function (e) {
      if ($(e.target).hasClass('big-checkbox')) {
        $(e.target).parent().next().slideDown();
        const items = $(e.target).parents('.choose-item').siblings();
        items.each(function () {
          $(this).find('.choo-sub').slideUp();
        });
      }
    });

    this.menuTagList = [
      { title: this.FLang.H01.H0008, type: null },
      { title: this.FLang.H01.H0009, type: 1 },
      { title: this.FLang.H01.H0010, type: 2 },
      { title: this.FLang.H01.H0011, type: 3 },
    ];
  }

  ngAfterViewInit(): void {
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
    this.setSlickWidth(window.innerWidth);
  }

  /**
   * 設定 Slick 最大寬度
   * @param windowWidth 螢幕尺寸
   */
  setSlickWidth(windowWidth) {
    if (windowWidth <= 768 && windowWidth > 576) {
      $('.slick_width_control li').css(
        'min-width',
        `${windowWidth / $('.tab-row-content').width() / 4}px`
      );
    }
    if (windowWidth <= 576) {
      $('.slick_width_control li').css(
        'min-width',
        `${(380 * windowWidth) / 576}px`
      );
    }
  }

  // --------------------------------------------------------------

  /** 驗證碼更新至 service */
  updateCaptchaCode(captchaCode) {
    this.captchaResult = captchaCode;
    this.controls.captchaCode.setValidators([
      Validators.required,
      this.vi.captchaCheck(this.captchaResult),
    ]);

    this.controls.captchaCode.updateValueAndValidity();

    // 驗證碼重整後更新使用者 input 框
    this.controls.captchaCode.reset();
  }

  /** 取得標題
   * title: 標題
   */
  getTitle(title) {
    this.title = title;
  }

  /** 建立驗證表單 */
  formValidate() {
    // 建立驗證表單
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.fb.group(validator);

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol, length = 30) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          name: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          mobile: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES006,
          email: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES007,
          captchaCode:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES021,
          option: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES022,
        }[control_key];
      }
      /** 新檢核end */

      return (
        {
          vnPhone: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES002,
          email: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES003,
          captchaCheck: this.FLang.ER.ER019,
          languageFormat:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          languagelength: `${this.FLang.ER.ER045} ${length} ${this.FLang.ER.ER032}`,
        }[type] || this.FLang.ER.ER023
      );
    }
    return this.FLang.ER.ER023;
  }

  setDropdownList() {
    const list: IHelpDropdownItem[] = [
      {
        id: 1,
        name: this.FLang.H01.H0006,
        tag: 'personal',
        content: [
          {
            type: 1,
            name: 'car',
            title: this.FLang.H01.H0006_.s001,
          },
          {
            type: 2,
            name: 'motor',
            title: this.FLang.H01.H0006_.s002,
          },
          {
            type: 3,
            name: 'travel',
            title: this.FLang.H01.H0006_.s003,
          },
          {
            type: 4,
            name: 'injury',
            title: this.FLang.H01.H0006_.s004,
          },
          {
            type: 5,
            name: 'house',
            title: this.FLang.H01.H0006_.s005,
          },
        ],
      },
      {
        id: 2,
        tag: 'business',
        name: this.FLang.H01.H0007,
        content: [
          // 工程險
          {
            type: 1,
            name: 'engineering',
            title: this.FLang.H01.H0007_.s001,
          },
          // 責任
          {
            type: 2,
            name: 'liability',
            title: this.FLang.H01.H0007_.s002,
          },
          // 財產
          {
            type: 3,
            name: 'property',
            title: this.FLang.H01.H0007_.s003,
          },
          // 運輸
          {
            type: 4,
            name: 'cargo',
            title: this.FLang.H01.H0007_.s004,
          },
        ],
      },
    ];

    // 如果為文件下載頁籤，新增其他文件項目
    if (this.currentBookmarkTab === 2) {
      list.push({
        id: 3,
        tag: 'other',
        name: this.FLang.H01.H0042,
        root: true,
        content: [
          // 其他文件
          {
            type: 1,
            name: 'other',
            title: this.FLang.H01.H0042_.s001,
          },
        ],
      });
    }

    this.dropdownList = list;
  }

  /** 切換 Bookmark */
  selectBookmark(value: number) {
    this.showImg = `bg${value}`;
    this.currentBookmarkTab = value;
    this.setDropdownList();
    this.setSlickWidth(window.innerWidth);

    setTimeout(() => {
      // 寫信諮詢頁面 或 文件下載的其他文件狀態下做切換需 resetStatus
      if (value === 3 || this.clickOther) {
        this.resetStatus(true);
        this.clickOther = false;
      } else {
        this.resetStatus();
      }
    }, 150);
  }

  /** 設定 FAQ 列表 */
  initFaqList() {
    const faq = this.cds.gLang.H02;

    this.faqList = Object.keys(faq).map((key) => {
      const { a: content, q: title } = faq[key];
      const [tag, type, subType, sort] = key
        .split('_')
        .map((key) => Number(key));

      return {
        tag,
        type,
        subType,
        sort,
        title,
        content,
      };
    });
  }

  /** 清除字串 & 狀態 */
  doClearInput(item: string) {
    this.group.controls[`${item}`].reset();
  }

  // --------------------------------------------------------------

  /** 左側選單 */
  dropdownTag(title, tag): any {
    this.leftTitle = title;
    this.leftTag = tag;

    // 問題選單重置
    this.resetStatus();
  }

  /** 下拉選單顯示 */
  collapseShow(index: any) {
    if (this.leftTitle) {
      return this.leftTitle === index ? true : false;
    } else {
      // 預設
      return index === 1 ? true : false;
    }
  }

  /** 下拉選單關閉 */
  collapseClose(index: number) {
    return this.leftTitle !== index;
  }

  idTag(index: number) {
    switch (index) {
      case 0:
        return 'headingOne';
      case 1:
        return 'headingTwo';
      case 2:
        return 'headingThree';
    }
  }

  dataTarget(index) {
    switch (index) {
      case 0:
        return '#collapseOne';
      case 1:
        return '#collapseTwo';
      case 2:
        return '#collapseThree';
    }
  }

  ariaControls(index) {
    switch (index) {
      case 0:
        return 'collapseOne';
      case 1:
        return 'collapseTwo';
      case 2:
        return 'collapseThree';
    }
  }

  idTagTwo(index) {
    return index === 0 ? 'headingThr' : 'headingFou';
  }

  dataTargetTwo(index) {
    return index === 0 ? '#collapseThr' : '#collapseFou';
  }

  ariaControlsTwo(index) {
    return index === 0 ? 'collapseThr' : 'collapseFou';
  }

  /** select-tag */
  switchTag(index: number | null) {
    this.question = 5;
    this.activeTag = index;
  }

  switchTitle(index) {
    return index === null
      ? this.FLang.H01.H0008
      : this.menuTagList[index].title;
  }

  /** help card */
  getGenerate(index, addHash = false): string {
    const id = 'collapseQue' + (index + 1);
    return addHash ? '#' + id : id;
  }

  getID(index): any {
    const id = 'Question' + (index + 1);
    return id;
  }

  ariaLabelledby(index) {
    return 'Question' + this.numberPipe.transform(index + 1);
  }

  optionItems(index, option = 'radio') {
    return option + (index + 1);
  }

  formatRadioId(i, j) {
    return `${i}_${j}`;
  }

  /** dropdownList mobile */
  dropdownOption(title: number, option: number) {
    this.leftTitle = title;
    this.leftTag = option;
    // 選取其他文件後切換頁面需做 resetStatus
    this.clickOther = title === 3 ? true : false;
    this.resetStatus();
  }

  /** 常見問題 看更多 */
  showMore() {
    this.question = this.question + 5;
  }

  /** 寫信諮詢 */
  // 確定送出按鈕
  buttonDisabled() {
    return !this.group.valid;
  }

  // 檢核表單內容
  async checkFormat() {
    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (!this.controls[key].value) {
        this.controls[key].markAsTouched();
        this.controls[key].markAsDirty();
      }
    });

    if (this.group.valid) {
      this.controls.optiontype.markAsTouched();
      this.controls.option.markAsTouched();

      const payload = {
        action: this.isFromJob ? 'joinus' : 'sendmail',
        name: this.controls.name.value,
        mobile: this.controls.mobile.value,
        email: this.controls.email.value,
        // 意見類型
        optiontype: this.isFromJob ? '' : this.controls.optiontype.value,
        // 意見內容
        option: this.controls.option.value,
        // 意見類型 for 後端判斷使用
        emailType: this.emailType,
        job: this.isFromJob ?  localStorage.getItem('job') : '',
      };

      // 送信 api
      const sendMailResult = await this.sendMailAPi.sendMail(payload);
      if (sendMailResult.status === 200) {
        $('#Modal-42-success').modal('show');
        this.clearAll();
      } else {
        $('#Modal-42-error').modal('show');
      }
    }
  }

  // 清除所有內容
  clearAll() {
    // 意見類型 小類 縮排
    $('.choo-sub').slideUp();
    // 清空驗證碼
    $('.form-control').val([]);
    this.controls.name.reset();
    this.controls.mobile.reset();
    this.controls.email.reset();
    this.controls.option.reset();
    this.controls.optiontype.reset();
    this.controls.optiontypeDetail.reset();
    this.controls.captchaCode.reset();
    this.captcha.createCaptcha();
    this.applyJob = undefined;
  }

  // 大類更換成小類名稱
  optiontype(optionType: number) {
    // 小類 reset
    this.controls.optiontypeDetail.reset();

    if (optionType === 0 || optionType === 1) {
      this.controls.optiontypeDetail.setValidators([Validators.required]);
      // 預設小類第一項
      this.group.controls.optiontypeDetail.setValue(0);
    } else {
      // 意見類型小類驗證 重置
      this.controls.optiontypeDetail.setValidators([]);
    }

    // 意見類型小類驗證 更新
    this.controls.optiontypeDetail.updateValueAndValidity();

    optionType = optionType + 1;
    this.controls.optiontype.value = this.optionList.filter(
      (option) => optionType === option.type
    )[0].title;

    // emailType for 後端判斷
    switch (this.controls.optiontype.value) {
      case this.FLang.H01.H0025_.s005:
        this.emailType = 'legal';
        break;
      case this.FLang.H01.H0025_.s006:
        this.emailType = 'job';
        break;
      default:
        this.emailType = 'service';
    }
  }

  // 小類存值到大類
  optiontypeDetail(optionDetail: number) {
    optionDetail = optionDetail + 1;
    this.controls.optiontype.value =
      this.controls.optiontype.value +
      '-' +
      this.insuredList.filter((option) => optionDetail === option.type)[0]
        .option;
  }

  // 小類預設
  optiontypeDetailDefault(optiontype: number, optionDetail: number) {
    return optiontype === 0 || optiontype === 1
      ? (optionDetail = 1)
      : optionDetail;
  }

  searchFaq(keyword) {
    this.searchKeyword = keyword;
  }

  /** 重置頁面狀態 */
  resetStatus(resetAll = false) {
    this.question = 5;
    this.switchTag(null);
    this.searchText = '';
    this.searchFaq('');

    if (resetAll) {
      this.leftTitle = 1;
      this.leftTag = 1;
    }
  }

  dropdownMenuClick(event, item) {
    // 選取其他文件後切換頁面需做 resetStatus
    this.clickOther = item.id === 3 ? true : false;
    if (item.root) {
      event.preventDefault();
      this.dropdownTag(item.id, 1);
      return;
    }
  }

  /** 取得 Faq 類別資料數量 */
  getFaqCountByType(type: number | null) {
    const list = this.faqListFilterByKeyword;
    return type
      ? list.filter((faq) => faq.subType === type).length
      : list.length;
  }

  contentClickHandler(event) {
    CommonService.contentClickHandler(event);
  }

  /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event) {
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
}
