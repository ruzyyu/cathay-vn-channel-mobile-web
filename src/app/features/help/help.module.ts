import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { HelpRoutingModule } from './help-routing.module';

import { HelpComponent } from './help.component';
import { SearchComponent } from './components/search/search.component';
import { TopbackgroundComponent } from './components/topbackground/topbackground.component';
import { BookmarkComponent } from './components/bookmark/bookmark.component';
import { DownloadCardComponent } from './components/download-card/download-card.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [
    HelpComponent,
    SearchComponent,
    TopbackgroundComponent,
    BookmarkComponent,
    DownloadCardComponent,
  ],
  imports: [CommonModule, HelpRoutingModule, SharedModule],
})
export class HelpModule {}
