import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { IHelpDownloadItem } from 'src/app/core/interface';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Lang } from 'src/types';

@Component({
  selector: 'app-download-card',
  templateUrl: './download-card.component.html',
  styleUrls: ['./download-card.component.scss'],
})
export class DownloadCardComponent implements OnInit {
  FLang: Lang['H01'];
  dataLinkList: IHelpDownloadItem[];
  FMoreCount = 1;

  @Input() itemId: number;
  @Input() itemType: number;
  @Input() keyword: string;
  @Input() switchTag: number;

  @Output() showMore = new EventEmitter();

  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.H01;
    this.dataLinkList = [
      {
        itemId: 2,
        itemType: 4,
        type: '貨物運輸險(水險)',
        title: this.FLang.H0017,
        content: [
          {
            option: this.FLang.H0017_.s001,
            document:
              'assets/pdf/Bao hiem hang hoa van chuyen duong hang khong.pdf',
          },
          {
            option: this.FLang.H0017_.s002,
            document: 'assets/pdf/Dieu khoan hang hoa xang dau cho roi.pdf',
          },
          {
            option: this.FLang.H0017_.s003,
            document: 'assets/pdf/Bao hiem hang hoa - hang than.pdf',
          },
          {
            option: this.FLang.H0017_.s004,
            document: 'assets/pdf/Bao hiem hang hoa - hang than noi dia.pdf',
          },
          {
            option: this.FLang.H0017_.s005,
            document:
              'assets/pdf/Bao hiem hang hoa thuc an dong lanh theo dieu kien A.pdf',
          },
          {
            option: this.FLang.H0017_.s006,
            document:
              'assets/pdf/Bao hiem hang hoa thuc an dong lanh theo dieu kien C.pdf',
          },
          {
            option: this.FLang.H0017_.s007,
            document:
              'assets/pdf/Bao hiem hang hoa thit dong lanh theo dieu kien A.pdf',
          },
          {
            option: this.FLang.H0017_.s008,
            document:
              'assets/pdf/Bao hiem hang hoa thit dong lanh 24 tieng theo dieu kien A.pdf',
          },
          {
            option: this.FLang.H0017_.s009,
            document:
              'assets/pdf/Bao hiem hang hoa thit dong lanh 24 tieng theo dieu kien C.pdf',
          },
          {
            option: this.FLang.H0017_.s010,
            document:
              'assets/pdf/Dieu khoan bao hiem hang hoa (A) 01-01-09.pdf',
          },
          {
            option: this.FLang.H0017_.s011,
            document:
              'assets/pdf/Dieu khoan bao hiem hang hoa (B) 01-01-09.pdf',
          },
          {
            option: this.FLang.H0017_.s012,
            document:
              'assets/pdf/Dieu khoan bao hiem hang hoa (C) 01-01-09.pdf',
          },
          {
            option: this.FLang.H0017_.s013,
            document: 'assets/pdf/Bao hiem hang hoa - Dieu khoan bao dong.pdf',
          },
          {
            option: this.FLang.H0017_.s014,
            document:
              'assets/pdf/Bao hiem hang hoa - Dieu khoan chien tranh.pdf',
          },
        ],
      },
      {
        itemId: 2,
        itemType: 3,
        type: '財產險',
        title: this.FLang.H0018,
        content: [
          {
            option: this.FLang.H0018_.s001,
            document: 'assets/pdf/Bao hiem chay no bat buoc.pdf',
          },
          {
            option: this.FLang.H0018_.s002,
            document: 'assets/pdf/Bao hiem chay va cac rui ro dac biet.pdf',
          },
          {
            option: this.FLang.H0018_.s003,
            document:
              'assets/pdf/Bao hiem moi rui ro tai san bao gom chay no bat buoc.pdf',
          },
          {
            option: this.FLang.H0018_.s004,
            document: 'assets/pdf/Bao hiem moi rui ro tai san.pdf',
          },
          {
            option: this.FLang.H0018_.s005,
            document:
              'assets/pdf/Bao hiem moi rui ro tai san ket hop Bao hiem Gian doan kinh doanh.pdf',
          },
          {
            option: this.FLang.H0018_.s006,
            document:
              'assets/pdf/Bao hiem cua hang - Trach nhiem cong cong (Bao hiem cua hang).pdf',
          },
        ],
      },
      {
        itemId: 2,
        itemType: 1,
        type: '工程險',
        title: this.FLang.H0019,
        content: [
          {
            option: this.FLang.H0019_.s001,
            document: 'assets/pdf/Bao hiem moi rui ro xay dung.pdf',
          },
          {
            option: this.FLang.H0019_.s002,
            document: 'assets/pdf/Bao hiem moi rui ro cong trinh lap dat.pdf',
          },
          {
            option: this.FLang.H0019_.s003,
            document: 'assets/pdf/Bao hiem may moc thiet bi cua chu thau.pdf',
          },
          {
            option: this.FLang.H0019_.s004,
            document: 'assets/pdf/Bao hiem do vo may moc.pdf',
          },
          {
            option: this.FLang.H0019_.s005,
            document:
              'assets/pdf/Bao hiem cong trinh trong thoi gian xay dung (Bat buoc).pdf',
          },
          {
            option: this.FLang.H0019_.s006,
            document:
              'assets/pdf/Bao hiem doi voi nguoi lao dong thi cong tren cong truong.pdf',
          },
          {
            option: this.FLang.H0019_.s007,
            document: 'assets/pdf/Bao hiem cong trinh lap dat bat buoc.pdf',
          },
        ],
      },
      {
        itemId: 2,
        itemType: 2,
        type: '責任險',
        title: this.FLang.H0020,
        content: [
          {
            option: this.FLang.H0020_.s001,
            document: 'assets/pdf/Bao hiem trach nhiem cong cong.pdf',
          },
          {
            option: this.FLang.H0020_.s002,
            document:
              'assets/pdf/Bao hiem trach nhiem san pham (Occurrence Form).pdf',
          },
          {
            option: this.FLang.H0020_.s003,
            document: 'assets/pdf/Bao hiem long trung thanh.pdf',
          },
          {
            option: this.FLang.H0020_.s004,
            document: 'assets/pdf/Bao hiem tien (Moi rui ro).pdf',
          },
          {
            option: this.FLang.H0020_.s005,
            document:
              'assets/pdf/Bao hiem trach nhiem cong cong va Bao hiem trach nhiem san pham dien rong.pdf',
          },
          {
            option: this.FLang.H0020_.s006,
            document:
              'assets/pdf/Bao hiem trach nhiem nghe nghiep tu van, thiet ke va giam sat.pdf',
          },
          {
            option: this.FLang.H0020_.s007,
            document:
              'assets/pdf/Bao hiem trach nhiem nguoi su dung lao dong.pdf',
          },
          {
            option: this.FLang.H0020_.s008,
            document: 'assets/pdf/Bao hiem boi thuong cho nguoi lao dong.pdf',
          },
          {
            option: this.FLang.H0020_.s009,
            document: 'assets/pdf/Bao hiem trach nhiem cang.pdf',
          },
        ],
      },
      {
        itemId: 1,
        itemType: 4,
        type: '傷害險',
        title: this.FLang.H0021,
        content: [
          {
            option: this.FLang.H0021_.s001,
            document: 'assets/pdf/Bao hiem tai nan con nguoi toan cau.pdf',
          },
          {
            option: this.FLang.H0021_.s002,
            document:
              'assets/pdf/Bao hiem tai nan con nguoi pham vi Viet Nam.pdf',
          },
          {
            option: this.FLang.H0021_.s003,
            document: 'assets/pdf/Bao hiem con nguoi ket hop 2019.pdf',
          },
          {
            option: this.FLang.H0021_.s004,
            document: 'assets/pdf/Bao hiem phuc loi nhan vien.pdf',
          },
          {
            option: this.FLang.H0021_.s005,
            document: 'assets/pdf/Bao hiem tai nan ho su dung dien.pdf',
          },
          {
            option: this.FLang.H0021_.s006,
            document: 'assets/pdf/Bao hiem Tai nan ca nhan.pdf',
          },
        ],
      },
      {
        itemId: 1,
        itemType: 3,
        type: '旅綜險',
        title: this.FLang.H0035,
        content: [
          {
            option: this.FLang.H0035_.s001,
            document:
              'assets/pdf/Bao hiem du lich tong hop (Toan the gioi).pdf',
          },
          {
            option: this.FLang.H0035_.s002,
            document: 'assets/pdf/Bao hiem du lich tong hop (Viet Nam).pdf',
          },
          {
            option: this.FLang.H0035_.s003,
            document: 'assets/pdf/Bao hiem du lich trong nuoc.pdf',
          },
        ],
      },
      {
        itemId: 1,
        itemType: 1,
        type: '汽車險',
        title: this.FLang.H0036,
        content: [
          {
            option: this.FLang.H0036_.s001,
            document: 'assets/pdf/Bao hiem bat buoc o to.pdf',
          },
          {
            option: this.FLang.H0036_.s002,
            document: 'assets/pdf/Bao hiem tu nguyen xe o to.pdf',
          },
          {
            option: this.FLang.H0036_.s003,
            document:
              'assets/pdf/Bao hiem Tai nan lai xe, phu xe, nguoi ngoi tren xe.pdf',
          },
          {
            option: this.FLang.H0036_.s004,
            document: 'assets/pdf/Bao hiem tu nguyen vat chat xe o to 2018.pdf',
          },
        ],
      },
      {
        itemId: 1,
        itemType: 2,
        type: '機車險',
        title: this.FLang.H0037,
        content: [
          {
            option: this.FLang.H0037_.s001,
            document: 'assets/pdf/Bao hiem bat buoc xe may.pdf',
          },
          {
            option: this.FLang.H0037_.s002,
            document: 'assets/pdf/Bao hiem tu nguyen xe may.pdf',
          },
          {
            option: this.FLang.H0037_.s003,
            document:
              'assets/pdf/Bao hiem Tai nan lai xe, phu xe, nguoi ngoi tren xe.pdf',
          },
          {
            option: this.FLang.H0037_.s004,
            document: 'assets/pdf/Bao hiem vat chat xe may.pdf',
          },
        ],
      },
      {
        itemId: 1,
        itemType: 5,
        type: '住宅火險',
        title: this.FLang.H0038,
        content: [
          {
            option: this.FLang.H0038_.s001,
            document: 'assets/pdf/Bao hiem ket hop nha tu nhan 2020.pdf',
          },
        ],
      },
      {
        itemId: 3,
        itemType: 1,
        type: '其他文件',
        title: this.FLang.H0043,
        content: [
          {
            option: this.FLang.H0043_.s001,
            document: 'assets/pdf/Quy che to giac.pdf',
          },
          {
            option: this.FLang.H0043_.s002,
            document: 'assets/pdf/Phieu to giac.pdf',
          },
        ],
      },
    ];
  }

  ngOnInit(): void {}

  get filterData() {
    return this.dataLinkList.find(
      (item) => item.itemId === this.itemId && item.itemType === this.itemType
    );
  }

  /** 文件下載 關鍵字搜尋後列表 */
  get filterListByKeyword() {
    return (
      this.filterData?.content.filter(({ option }) =>
        option.includes(this.keyword)
      ) || []
    );
  }

  /** 文件下載 列表 */
  get downloadList() {
    return this.filterListByKeyword.slice(0, this.switchTag);
  }

  clickDownloadCard(doc) {
    window.open(doc);
  }
}
