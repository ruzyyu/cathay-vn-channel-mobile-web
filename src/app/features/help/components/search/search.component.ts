import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Lang } from 'src/types';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  /** 搜尋文字 */
  @Input() searchText: string;
  @Output() searchTextChange = new EventEmitter<string>();

  @Output() filterList = new EventEmitter<string>();

  FLang: Lang['H01'];

  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.H01;
  }

  ngOnInit(): void {}

  search() {
    this.filterList.emit(this.searchText);
    document.getElementById('helpInputSearch')?.blur();
  }
}
