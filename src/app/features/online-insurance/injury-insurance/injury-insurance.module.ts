import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InjuryInsuranceRoutingModule } from './injury-insurance-routing.module';
import { InjuryInsuranceComponent } from './injury-insurance.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [InjuryInsuranceComponent],
  imports: [CommonModule, InjuryInsuranceRoutingModule, SharedModule],
})
export class InjuryInsuranceModule {}
