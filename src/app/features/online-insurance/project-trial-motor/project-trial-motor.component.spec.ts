import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectTrialMotorComponent } from './project-trial-motor.component';

describe('ProjectTrialMotorComponent', () => {
  let component: ProjectTrialMotorComponent;
  let fixture: ComponentFixture<ProjectTrialMotorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectTrialMotorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectTrialMotorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
