import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectTrialMotorComponent } from './project-trial-motor.component';

const routes: Routes = [{ path: '', component: ProjectTrialMotorComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectTrialMotorRoutingModule { }
