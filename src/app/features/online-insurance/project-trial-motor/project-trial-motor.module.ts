import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectTrialMotorRoutingModule } from './project-trial-motor-routing.module';
import { ProjectTrialMotorComponent } from './project-trial-motor.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ProjectTrialMotorComponent],
  imports: [CommonModule, ProjectTrialMotorRoutingModule, SharedModule],
})
export class ProjectTrialMotorModule {}
