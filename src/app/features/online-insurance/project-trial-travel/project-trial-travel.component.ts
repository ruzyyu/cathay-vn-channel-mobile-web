import { UserinfoComponent } from './../../../shared/components/userinfo/userinfo.component';
import { OnlinecalcService } from './../../../core/services/onlinecalc/onlinecalc.service';
import { PaymentService } from './../../../core/services/payment/payment.service';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { InsuredCardComponent } from './components/insured-card/insured-card.component';
import { LoginComponent } from './../../../shared/components/login/login.component';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { DatePipe } from '@angular/common';
import {
  IResponse,
  UserInfo,
  INation,
  IConfirm,
} from './../../../core/interface/index';
import { DatapoolService } from './../../../core/services/datapool/datapool.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { CommonService } from 'src/app/core/services/common.service';
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewContainerRef,
  ViewChild,
  ComponentFactoryResolver,
  Injector,
  OnDestroy,
  HostListener,
} from '@angular/core';
import { InsuranceService } from 'src/app/core/services/api/Insurance/insurance.service';
import * as moment from 'moment';
import { FastpairComponent } from 'src/app/shared/components/fastpair/fastpair.component';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { W7Component } from 'src/app/shared/components/travel/w7/w7.component';
import { V1Component } from 'src/app/shared/components/travel/v1/v1.component';
import { T44v1Component } from 'src/app/shared/components/travel/t44v1/t44v1.component';
import { W1Component } from 'src/app/shared/components/travel/w1/w1.component';
import { W2Component } from 'src/app/shared/components/travel/w2/w2.component';
import { T44Component } from 'src/app/shared/components/travel/t44/t44.component';
import { Lang } from 'src/types';

// 旅遊地點清單
export interface ITravelList {
  areaName: string;
  areaId: string;
  index: number;
}

// 被保人清單
export interface IInsured {
  name: string;
  id: string;
  birthday: string;
  sex: string;
  sameInsure: boolean;
  recno: number;
  error?: string;
}

// 理賠機場資料
export interface IAirCard {
  airplaneNo: string;
  airPortS: string;
  airPortE: string;
  airplaneDate: string;
}

// 旅遊試算結果
export interface IPrice {
  amt: number;
  amttax: number;
  prod_pol_code: string;
  t4amt: number;
  people_amt: number;
}

@Component({
  selector: 'app-project-trial-travel',
  templateUrl: './project-trial-travel.component.html',
  styleUrls: ['./project-trial-travel.component.scss'],
})
export class ProjectTrialTravelComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  constructor(
    private cds: DatapoolService,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private apiInsurance: InsuranceService,
    private route: ActivatedRoute,
    private go: Router,
    private apiMember: MemberService,
    private componentFactory: ComponentFactoryResolver,
    private api: PublicService,
    private pay: PaymentService,
    private calcsv: OnlinecalcService,
    private vi: ValidatorsService
  ) {
    /** 首次登入不允許再 個資頁 */
    this.ro = this.go.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    ) as Observable<NavigationEnd>;
    this.ro.subscribe((evt) => {
      if (
        evt.id === 1 &&
        evt.url === '/online-insurance/project-trial-travel/personInfo'
      ) {
        this.go.navigate([
          '/online-insurance/project-trial-travel/',
          'calcAmt',
        ]);
      }

      // 如果不是在本頁的活動就清除日期
      if (
        !(
          this.FPreviousUrl ===
            '/online-insurance/project-trial-travel/personInfo' ||
          this.FPreviousUrl === '/online-insurance/project-trial-travel/calcAmt'
        )
      ) {
        this.FTravelDate = undefined;
        delete this.cds.gDatapicker.travel;
      }
      if (evt.url === '/online-insurance/project-trial-travel/personInfo') {
        this.FPayTransaction = false;
      }
      this.FPreviousUrl = evt.url;
    });

    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
    this.FAirportTitleSplit = this.FLang.C02.step1.L0016.replace(
      /(.*) (\(.*\))/,
      '$1|$2'
    ).split('|');

    // console.log('this.FAirportTitleSplit');
    // console.log(this.FAirportTitleSplit);

    /** 付款方式 list */
    this.payList = this.calcsv.paylist;

    /** 預設換頁日期不清除 */
    if (this.cds.gDatapicker.travel) {
      this.FTravelDate = this.cds.gDatapicker.travel;
    }
  }

  get checkisVN() {
    const mbol =
      this.travelpositionList &&
      Array.isArray(this.travelpositionList) &&
      (this.travelpositionList.length === 10 ||
        (this.travelpositionList.length > 0 &&
          this.travelpositionList[0].areaId === 'Vietnam'));
    mbol ? this.controls.Areainput.disable() : this.controls.Areainput.enable();
    return mbol;
  }
  @ViewChild('daterangepicker') daterangepicker;
  @ViewChild('content', { read: ViewContainerRef }) content: ViewContainerRef;
  @ViewChild('cInsured', { read: ViewContainerRef }) cInsured: ViewContainerRef;
  @ViewChild('cfastpair', { read: ViewContainerRef })
  cfastpair: ViewContainerRef;
  @ViewChild('ly_pluInfo', { read: ViewContainerRef })
  ly_pluInfo: ViewContainerRef;
  @ViewChild('ly_userinfo', { read: UserinfoComponent })
  ly_userinfo: UserinfoComponent;

  cLoing: ViewContainerRef;
  FLang: Lang;
  FLangType: string;
  injector: Injector;
  /** 國家下拉輸入緩存 */
  FNationInput = '';
  /** 監測router */
  ro: Observable<any>;

  /** 註冊全局訊息 */
  unload: any = [];
  FF: any;
  controls: { [key: string]: AbstractControl };
  validators: {};
  group: FormGroup;

  /** repPayload */
  repPayload: any;
  // 狀態器
  FCalcTestState = false;
  FStep: 'calcAmt' | 'personInfo' | 'payment' = 'calcAmt';
  FPlan = 2;
  FPlanId = ['V1', 'T45', 'T46', 'W7', 'W1', 'W2'];
  // 專案目前內部專案是 v1 t44 t44v1
  FType: string; // 用於判斷是否是新增會員
  FSyncUserinfo = true; // 是否同步會員
  FSelectPayType: any;
  // 合計保費總金額
  FAmount = 0;
  FAmtTax = 0;
  // 境內境外 true 境內
  FAbroad: boolean;

  // 旅遊地點緩存
  travelpositionList: Array<ITravelList>;
  travelNationList: Array<INation>;
  FArr_in: Array<INation>;
  FArr_hot: Array<INation>;
  FArr_noarr: Array<INation>;
  FArr_noraml: Array<INation>;
  FTimelist: Array<any> = [];
  FUserInfo: UserInfo;
  /** 試算專用變數 */
  // 試算結果
  FTravelCalcList: Array<IPrice> = [];
  // 當前命中代號
  FProd_pol_code: string;
  // 被保險額
  FProd_pol_AMT: number;
  // t4被保險額
  Ft4AMT: number;
  /** 試算專用變數 */
  // 時間緩存器
  FTimeIndex: any = -1;
  DiffDate: any;
  // 人數緩存
  FUserList: Array<any> = [];
  FUserIndex: any = -1;
  // 被保人清單
  FInsuredList: Array<IInsured> = [];
  // 航班資料 0 起 1 訖 2 轉1 3 轉2
  FAirPortList: Array<IAirCard> = [];
  // 導頁來的全局資料
  // FOrder_no: string;
  // FAply_no: string;
  FDateRange: string;
  // 動態元件
  comInsured: any;
  comfastpair: any;
  // 付款選項宣告
  payList: any;
  paymentNumber = -1;
  ckPayment = false;
  // 手機板驗證高度用的
  initInnerHieght: number;
  // login補登狀態
  FIsFB: string;
  FIsGoogle: string;
  /** 額外錯誤文字email */
  FErrorMail: string;
  /** 判斷付款[完成否] */
  FPayResult = false;
  /** 判斷付款-2[付款失敗 狀態On] */
  FPayTransaction = false;
  /** 訂單狀態 */
  FOrder_no = 0;
  /** 保單編號 */
  FAply_no = '';
  /** 註冊渠道 */
  FChannel: string;
  /** 註冊帳號及姓名 */
  FSocialName: string;
  FSocialAccount: string;
  /** 作用於小於6H的日期限制... */
  FSdate: any;
  /** 付款二次確認窗 */
  FConfirm: IConfirm;
  /** 切割選擇機場標題 */
  FAirportTitleSplit: string[];

  // 元件狀態器
  chooseComponent = '';
  mapping = new Map<string, any>([
    ['v1', V1Component],
    ['t44', T44Component],
    ['t44v1', T44v1Component],
    ['W7', W7Component],
    ['W1', W1Component],
    ['W2', W2Component],
  ]);

  /** 預設換頁日期不清除 */
  FTravelDate: string;

  /** 存放上一頁網址 */
  FPreviousUrl;

  /** 選擇付款跟同意的提示start */
  FAgreeError: string;
  FPayNoError: string;

  /** confirm 專用 */
  FsubscribePayAction: Subscription;

  @HostListener('window:resize')
  onResize() {
    this.alignPaymentText();
  }

  ngOnDestroy(): void {
    this.cds.gTrafficTravelInsurance = null;
    this.unload.forEach((x) => x.unsubscribe());
    if (this.FsubscribePayAction) {
      this.FsubscribePayAction.unsubscribe();
    }
  }

  async ngOnInit() {
    this.travelpositionList = [];

    /**
     * @deprecated
     * 判別是國內還是國外旅遊
     * @param xtype=in (國內) out (國外)
     * @param param=國壽會員編(需解密)
     */
    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams && queryParams.xtype && queryParams.xtype === 'in') {
        const mstr =
          this.FLangType === 'en-US'
            ? 'Vietnam'
            : this.FLangType === 'vi-VN'
            ? 'Việt Nam'
            : '越南';

        if (this.travelpositionList.length === 0) {
          this.travelpositionList.push({
            areaName: mstr,
            areaId: 'Vietnam',
            index: 0,
          });
        }
      }
    });
    /** 國內外判別 */
    if (this.travelpositionList.length > 0) {
      this.FAbroad = this.travelpositionList[0].areaId === 'Vietnam';
    }

    /** 換頁處理 */
    this.route.params.subscribe((params) => {
      this.FStep = params.step;
      if (this.FStep) {
        $('#paymentFailModal').modal('hide');
      }
    });

    /** userinfo註冊換頁.... */
    this.calcsv.Inituser();

    // 國內旅遊 預設越南，且選項不可選

    this.FUserInfo = this.cds.userInfo;
    // 初始化時間
    this.InitTimePick();
    this.InitUserList();

    // 表單驗證初始化
    this.validators = this.calcsv.initData_travel(this.FUserInfo);

    this.formValidate();

    // 取得國家清單
    await this.api.gnationList();
    this.InitNationList();

    // 取得飛機資料(無需等待.)
    // this.api.gAirPortList();

    // /** 上一頁資料 */
    if (
      this.cds.gTCalc_to_save &&
      this.cds.gTCalc_to_save.type === 'travel' &&
      !this.route.snapshot.queryParams.action
    ) {
      this.setValue(this.cds.gTCalc_to_save.saveorder, true);
      if (!this.cds.userInfo.seq_no) {
        this.calcsv.converuser(this.cds.userInfoTemp);
        this.ly_userinfo.doReflashScreen();
      }
      this.calcsv.resetData();
    }

    /** 訂單頁資料 */
    this.route.queryParams.subscribe(async (queryParams) => {
      // 導頁付款.
      if (queryParams.action === 'pay') {
        await this.setValue(this.cds.gTCalc_to_save.saveorder, true);
        this.cds.gTCalc_to_save.type = 'travel';
        await this.doPayment(this.cds.gTCalc_to_save, true, queryParams.action);
        this.calcsv.resetData();
        this.cds.userInfo.seq_no ? (this.FType = 'edit') : (this.FType = 'add');
      }
    });

    // 子視窗存狀態碼，避免關閉錯誤
    this.route.queryParams.subscribe(async (queryParams) => {
      if (queryParams.amount) {
        this.unload.forEach((subscribe) => {
          subscribe.unsubscribe();
        });
      } else {
        if (this.unload.length === 0) {
          this.unload.push(
            // 註冊全局的事件
            fromEvent(window, 'beforeunload').subscribe((event: any) => {
              (event || window.event).returnValue =
                '★資料尚未存檔，確定是否要離開？★';
              return '★資料尚未存檔，確定是否要離開？★';
            })
          );
        }
      }
    });

    /** 訂單頁資料 */
    if (
      this.cds.gTrafficTravelInsurance &&
      this.cds.gTrafficTravelInsurance.order_no
    ) {
      this.setValue(this.cds.gTrafficTravelInsurance);
    }
  }

  /* 從訂單編輯來的資料 */
  async setValue(param, isCalctest = true) {
    const mOrder = param;
    /** 國內、外 */
    mOrder.sde_kd === '1' ? (this.FAbroad = true) : (this.FAbroad = false);

    /** 旅遊標籤 */
    const arrPosition = mOrder.tral_loc.split(',');
    if (Array.isArray(arrPosition) && arrPosition.length > 0) {
      this.travelpositionList = [];
      arrPosition.forEach((item, index) => {
        this.travelpositionList.push({
          areaName: item,
          areaId: '00',
          index,
        });
      });
    }

    /** 取代飛機資料 */
    if (mOrder.flight_data && mOrder.flight_data.length > 0) {
      mOrder.flight_data.forEach((element, index) => {
        this.FAirPortList.push({
          airplaneNo: element.flight_no,
          airPortS: element.departure_airport,
          airPortE: element.arrival_airport,
          airplaneDate: element.flight_date,
        });
      });

      if (this.FAirPortList.length < 2) {
        this.createfastpair(this.FAirPortList.length);
      }
    } else {
      this.createfastpair();
    }

    /** 取代被保人資料 */
    mOrder.insr_data.forEach((element, index) => {
      this.FInsuredList.push({
        name: element.insr_name,
        id: element.insr_cert_number,
        birthday: element.insr_birthday,
        recno: index,
        sex: element.sex,
        sameInsure: false,
      });
    });

    /** 取代試算資料 旅遊險沒辦法不試算，金額是動態的 */
    // this.FAmount = mOrder.tot_prem;
    // this.FAmtTax = mOrder.tot_prem_with_vat;
    this.FProd_pol_code = mOrder.prod_pol_code;
    this.FOrder_no = mOrder.order_no;
    this.FAply_no = mOrder.aply_no;
    const mDt = moment(mOrder.pol_dt).format('HH');
    this.FTimeIndex = Number(mDt);
    this.controls.timePick.setValue(mOrder.pol_dt);
    this.FDateRange = `${moment(mOrder.pol_dt).format('DD/MM/YYYY')}-${moment(
      mOrder.pol_due_dt
    ).format('DD/MM/YYYY')}`;

    this.controls.rangeDateS.setValue(
      moment(mOrder.pol_dt).format('YYYY-MM-DD')
    );
    this.controls.rangeDateE.setValue(
      moment(mOrder.pol_due_dt).format('YYYY-MM-DD')
    );
    this.FUserIndex = this.FInsuredList.length - 1;
    this.controls.userPick.setValue(this.FInsuredList.length - 1);

    if (isCalctest) {
      await this.calcTest(false);
    }
    // 確定方案後再一次調整金額
    this.FPlan =
      this.FPlanId.findIndex((item) => item === mOrder.insr_data[0].tral_proj) +
      1;

    this.paymentNumber = mOrder.paymentNumber;

    this.calcPlan();

    /** 清除狀態 */
    if (this.cds.gTCalc_to_save) {
      this.cds.gTCalc_to_save.type = 'none';
    }
    this.cds.gTrafficTravelInsurance = null;
  }

  /** 給Jquery */
  ngAfterViewInit() {
    $('#peopleDown').on('hidden.bs.dropdown', function () {
      const lyadvisory = $('.dropdown-button').parents('.advisory-content');
      $(lyadvisory).removeClass('addHeight');
    });
    $('#peopleDown').on('show.bs.dropdown', function () {
      const lyadvisory = $('.dropdown-button').parents('.advisory-content');
      $(lyadvisory).addClass('addHeight');
    });

    // 當 dropdown 為 modol 最後一個元素並開啟選單時撐高下方高度
    $('.custom-modal-dropdown').on('click', function (e) {
      const btn = $(this).find('.dropdown-toggle');
      if ($(btn).attr('aria-expanded') === 'true') {
        $(this).parent().addClass('custom-dropdown-mb');
        $(this)
          .parents('.modal-body')
          .scrollTop(this.offsetTop - 200);
      }
    });

    // 當 dropdown 為 modol 最後一個元素並關閉選單時移除下方高度
    $('.custom-modal-dropdown').on('hide.bs.dropdown', function (e) {
      $(this).parent().removeClass('custom-dropdown-mb');
    });

    $('#dropdown-menu').on('scroll', function () {
      $('#dropdown-menu').scrollTop();
    });

    this.route.params.subscribe((params) => {
      switch (params.step) {
        case 'calcAmt':
          if (this.controls && this.controls.rangeDateS.value) {
            this.FDateRange = `${moment(this.controls.rangeDateS.value).format(
              'DD/MM/YYYY'
            )}-${moment(this.controls.rangeDateE.value).format('DD/MM/YYYY')}`;
          }
          break;
        case 'personInfo':
          setTimeout(this.alignPaymentText, 500);
          break;
      }
    });
  }

  /** 日期元件接收值 */
  getRDate(event) {
    if (!event) {
      this.controls.rangeDateS.setValue(undefined);
      this.controls.rangeDateE.setValue(undefined);
      this.FTimeIndex = -1;
      this.controls.timePick.setValue('');
      this.controls.timePick.markAsTouched();
      this.resetCalc();
      this.DiffDate = undefined;
      return;
    }
    this.controls.rangeDateS.setValue(event.startDay);
    this.controls.rangeDateE.setValue(event.endDay);

    const m1 = moment(event.startDay);
    const m2 = moment(event.endDay);
    this.DiffDate = m2.diff(m1, 'day');

    // 收到時間修改，判斷截止日其是否為今天，如果是今天或隔天須判別
    // 只能選擇6小時以後的時間
    const now = new Date();
    const startDay = new Date(event.startDay).getDate();

    this.FTimeIndex = now.getHours();
    const nowDate = now.getDate();
    let mindex: any;
    this.FTimeIndex = -1;
    this.controls.timePick.setValue('');
    this.controls.timePick.markAsTouched();
    // 今天
    const mGetMinutes = now.getMinutes();
    const mGetSeconds = now.getSeconds();
    if (nowDate === startDay) {
      // 如果購買時剛好是整點，則時間加 6
      if (mGetMinutes === 0 && mGetSeconds === 0) {
        mindex = now.getHours() + 6;
      } else {
        mindex = now.getHours() + 7;
      }

      this.FTimelist = [];
      for (let i = mindex; i < 24; i++) {
        this.FTimelist.push({
          option: `${CommonService.paddingLeft(i, 2)}:00`,
        });
      }
    } else if (nowDate + 1 === startDay) {
      // 隔天
      mindex = 0;
      if (24 - now.getHours() <= 6) {
        // 如果購買時剛好是整點，則時間用 6 來減
        if (mGetMinutes === 0 && mGetSeconds === 0) {
          mindex = 6 - (24 - now.getHours());
        } else {
          mindex = 7 - (24 - now.getHours());
        }
      }
      this.FTimelist = [];
      for (let i = mindex; i < 24; i++) {
        this.FTimelist.push({
          option: `${CommonService.paddingLeft(i, 2)}:00`,
        });
      }
    } else {
      this.FTimelist = [];
      for (let i = 0; i < 24; i++) {
        this.FTimelist.push({
          option: `${CommonService.paddingLeft(i, 2)}:00`,
        });
      }
    }

    // 緩存日期
    this.cds.gDatapicker.travel = $('#lydatepickR').val();
    this.FTravelDate = $('#lydatepickR').val() as any;
    this.resetCalc();
  }

  /** 時間初始化 */
  InitTimePick() {
    if (moment().get('hours') >= 17) {
      this.FSdate = moment().add(1, 'days');
    } else {
      this.FSdate = moment();
    }

    for (let i = 0; i < 24; i++) {
      this.FTimelist.push({ option: `${CommonService.paddingLeft(i, 2)}:00` });
    }
  }
  /** 人數初始化 */
  InitUserList() {
    for (let i = 1; i < 10; i++) {
      this.FUserList.push({ option: i });
    }
  }

  /** 旅遊地點初始化 */
  InitNationList() {
    const arr = this.cds.gPubCommon.nationList;
    this.FArr_in = arr
      .filter((item) => {
        return item.nationType === '國內';
      })
      .map((item) => {
        item.active = false;
        return item;
      });

    this.FArr_hot = arr
      .filter((item) => {
        return item.nationType === '熱門';
      })
      .map((item) => {
        item.active = false;
        return item;
      });

    this.FArr_noarr = arr
      .filter((item) => {
        return item.nationType === '無法投保';
      })
      .map((item) => {
        item.active = true;
        return item;
      });

    this.FArr_noraml = arr
      .filter((item) => {
        return item.nationType === '';
      })
      .map((item) => {
        item.active = false;
        return item;
      });
  }
  /** 旅遊地點初始化 */
  FilterNationList() {
    const arr = this.cds.gPubCommon.nationList;
    this.FArr_in = arr.filter((item) => {
      return item.nationType === '國內';
    });

    this.FArr_hot = arr.filter((item) => {
      return item.nationType === '熱門';
    });

    this.FArr_noarr = arr.filter((item) => {
      return item.nationType === '無法投保';
    });

    this.FArr_noraml = arr.filter((item) => {
      return item.nationType === '';
    });
  }

  /** 取得錯誤訊息 */
  getErrors(errors) {
    if (errors) {
      const type = Object.keys(errors)[0];
      return (
        {
          fontLengthNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
          vnCertNumber: this.FLang.ER.ER036,
          vn2020ID:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
          birthday18year: this.FLang.ER.ER038,
          vnRgstNo: this.FLang.ER.ER023,
          vnCheckQty: this.FLang.ER.ER024,
        }[type] || this.FLang.ER.ER023
      );
    }
    return this.FLang.ER.ER023;
  }

  /** 建立驗證表單 */
  formValidate() {
    // 建立驗證表單
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator, { updateOn: 'submit' });
    // 偵測物件是否異動
    this.group.valueChanges.subscribe((item) => {
      // 判斷若 userinfo 資料有修改，則將 同購買人資料清空
      if (!this.FInsuredList[0]) {
        return;
      }
      if (!this.FInsuredList[0].sameInsure) {
        return;
      }
      if (
        this.FInsuredList[0].id === this.controls.certificate_number.value &&
        this.FInsuredList[0].name === this.controls.name.value &&
        this.FInsuredList[0].birthday === this.controls.birthday.value &&
        this.FInsuredList[0].sex === this.controls.sex.value
      ) {
        return;
      }
      this.delInsured(0);
    });

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
    // this.FCalcOrder = rep.data;
  }

  resetCalc() {
    this.FAmount = 0;
    this.FCalcTestState = false;
    this.FStep = 'calcAmt';

    if (this.travelpositionList.length >= 10) {
      this.group.controls.Areainput.disable();
    } else {
      this.group.controls.Areainput.enable();
    }
  }

  /**
   * @description 試算
   * @param isCalc [boolean] 是否是真的試算
   */
  async calcTest(isCalc = true) {
    this.group.get('rangeDateS').markAsTouched();
    this.group.get('timePick').markAsTouched();
    this.group.get('Areainput').markAsTouched();
    this.group.get('userPick').markAsTouched();
    this.group.get('rangeDateE').markAsTouched();

    this.group.get('rangeDateS').setValidators([Validators.required]);
    this.group.get('rangeDateS').updateValueAndValidity();
    this.group.get('rangeDateE').setValidators([Validators.required]);
    this.group.get('rangeDateE').updateValueAndValidity();
    this.group.get('timePick').setValidators([Validators.required]);
    this.group.get('timePick').updateValueAndValidity();
    this.group.get('userPick').setValidators([Validators.required]);
    this.group.get('userPick').updateValueAndValidity();

    if (
      !this.controls.rangeDateS.value ||
      !this.controls.rangeDateE.value ||
      this.travelpositionList.length === 0 ||
      this.FTimeIndex < 0 ||
      this.FUserIndex < 0
    ) {
      return;
    }

    const payload = {
      language: this.cds.gLangType,
      client_target: this.cds.userInfo.certificate_number_12
        ? this.cds.userInfo.certificate_number_12
        : this.cds.userInfo.certificate_number_9,
      token: this.cds.gToken,
      sde_kd: this.FAbroad ? '1' : '2',
      aply_no: '',
      type_pol: 'B',
      prod_pol_code: this.FAbroad ? ['AC01005', 'AC01013'] : ['AC01001'],
      pol_dt:
        this.controls.rangeDateS.value +
        ` ${this.FTimelist[this.FTimeIndex].option}:00`,
      pol_due_dt: this.controls.rangeDateE.value + ' 23:59:59',
      tral_proj: this.FAbroad ? ['V1', 'T45', 'T46'] : ['W7', 'W1', 'W2'],
      t4amt: this.FAbroad ? ['100000000', '200000000'] : [],
      people_count: this.FUserList[this.FUserIndex].option,
    };

    const rep: IResponse = await this.apiInsurance.calcTravelInsurance(payload);

    if (rep.status === 200) {
      // 總金額
      this.FCalcTestState = true;
      this.FAbroad ? (this.FPlan = 2) : (this.FPlan = 5);

      if (rep.data && Array.isArray(rep.data)) {
        // 楊國偉說這樣寫辣==
        this.FTravelCalcList = [];

        //  試算順序國外是W1、W2、W7 但顯示時需要改成 W7、W1、W2
        rep.data.forEach((item) => {
          this.FTravelCalcList.push({
            amt: item.tot_prem,
            amttax: item.tot_prem,
            prod_pol_code: item.prod_pol_code,
            t4amt: item.t4amt,
            people_amt: item.people_amt,
          });
        });

        this.calcPlan();
      }

      if (isCalc) {
        setTimeout(() => {
          CommonService.scroll(this.ly_pluInfo.element.nativeElement);
        }, 100);
      }
    }
  }

  /** 畫面事件集中 */
  /** 時間pick */
  onTimeClick(index) {
    this.FTimeIndex = index;
    this.controls.timePick.setValue(index);
    this.resetCalc();
  }

  onUserClick(index) {
    this.FUserIndex = index;
    this.controls.userPick.setValue(index);
    $('.advisory-content').removeClass('addHeight');
    this.resetCalc();
  }

  /** 地區重選 */
  onAreaListClick(index) {
    this.travelpositionList.push(index);
    this.resetCalc();
  }

  /**
   * @description 子母傳值
   * @param 會員建檔資料
   *
   */
  setUsertoGroup(event) {
    Object.keys(event).forEach((item) => {
      if (this.controls[item]) {
        this.controls[item].setValue(event[item].value);
      }
    });
  }
  /** POP */
  showPOP() {
    $('#popContactService').modal('show');
  }

  /** 切換頁面 */
  async goStep(position) {
    switch (position) {
      case 'center':
        break;
      case '2':
        //  產生被保人空資料
        this.createInsuredList();
        // 產生飛機起始資料
        this.createfastpair(2);
        // 填寫個人資料
        if (this.cds.userInfo.seq_no) {
          this.FType = 'edit';
          this.go.navigate([
            '/online-insurance/project-trial-travel/',
            'personInfo',
          ]);
          CommonService.topFunction();
        } else {
          // 註冊會員
          this.FType = 'add';
          this.showLogin();
          CommonService.topFunction();
        }

        break;
      // 付款
      case '3':
        /** 解除封印 允許後檢核 */
        Object.keys(this.controls).forEach((item) => {
          this.controls[item].markAsDirty();
          this.controls[item].markAsTouched();
        });
        /** 檢查userinfo是否完成 */
        let mbol = this.ly_userinfo.checkPayment();

        /** 身分證額外檢核 */
        this.group
          .get('certificate_number')
          .setValidators([
            this.vi.fontLengthNumber,
            Validators.required,
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value
            ),
            this.vi.vn2020ID(this.controls.birthday.value),
          ]);
        this.group.get('certificate_number').updateValueAndValidity();

        /** 旅遊險生日必須滿18歲檢核 */
        this.group
          .get('birthday')
          .setValidators([
            Validators.required,
            this.vi.birthday18year(this.controls.birthday.value),
          ]);
        this.group.get('birthday').updateValueAndValidity();

        if (!mbol) {
          let resultError;
          for (const key in this.controls) {
            if (!resultError && this.controls[key].invalid) {
              resultError = key;
            }
          }
          if (resultError) {
            CommonService.scrollto(`lyc_${resultError}`, 30);
          }
          return;
        }
        if (!this.checkpayment()) {
          return;
        }

        /** 檢核被保人資訊，如果有資訊的話 */
        mbol = this.checkInsr();
        if (mbol) {
          return;
        }

        // 更新會員
        if (this.FSyncUserinfo) {
          this.repPayload = {
            ...this.cds.userInfo,
            email: this.controls.email.value,
            email_bk: this.controls.email_bk.value,
            mobile: this.controls.mobile.value,
            county: this.controls.county.value,
            province: this.controls.province.value,
            addr: this.controls.address.value,
            account: this.controls.email.value, // 忘記密碼帳號
          };

          // 強制將mail交換
          if (this.FType === 'edit') {
            // this.repPayload.email_bk = this.repPayload.email;
            // this.repPayload.email = this.cds.userInfo.email;
          } else {
            // 新增流程須去註冊頁，緩存預計存檔資訊
            this.repPayload.customer_name = this.controls.name.value.trim();
            this.repPayload.birthday = this.controls.birthday.value;
            this.repPayload.seq_no = 0;
            this.repPayload.is_validate_policy = 'N';
            this.repPayload.sex = this.controls.sex.value;
            if (this.FChannel === 'fb') {
              this.repPayload.fb_account = this.FSocialAccount;
              this.repPayload.fb_title = this.FSocialName;
            } else {
              this.repPayload.google_account = this.FSocialAccount;
              this.repPayload.google_title = this.FSocialName;
            }

            // 新增流程驗證Email
            const payload = { email: this.repPayload.email };
            const rep: IResponse = await this.apiMember.MemberVerifyEmail(
              payload
            );
            if (rep.status !== 200) {
              CommonService.scrollto('lyc_email');
              this.FErrorMail = this.FLang.ER.ER033;
              return;
            }
          }
        }

        // 確認付款(這支其實是儲存訂單)
        const payload: any = {
          order_no: this.FOrder_no,
          aply_no: this.FAply_no,
          member_seq_no: this.FUserInfo.seq_no,
          order_status: '1',
          type_pol: 'B',
          prod_pol_code: this.FProd_pol_code,
          prod_pol_code_nm: '',
          pol_dt:
            this.controls.rangeDateS.value +
            ` ${this.FTimelist[this.FTimeIndex].option}:00`,
          pol_due_dt:
            this.controls.rangeDateE.value +
            ` ${this.FTimelist[this.FTimeIndex].option}:00`,
          certificate_type: '1',
          certificate_number: this.controls.certificate_number.value,
          customer_name: this.controls.name.value.trim(),
          birthday: this.controls.birthday.value,
          addr: this.controls.address.value,
          province: this.controls.province.value,
          county: this.controls.county.value,
          mobile: this.controls.mobile.value,
          email: this.controls.email_bk.value || this.controls.email.value, // SA要求的
          sex: this.controls.sex.value,
          tot_prem_with_vat: this.FAmtTax,
          tot_prem: this.FAmount,
          print_type: '1',
          sde_kd: this.FAbroad ? '1' : '2',
          tral_loc: this.FAbroad
            ? this.travelpositionList[0].areaName
            : this.travelpositionList.map((item) => item.areaName).join(','),
          national_id: 'VIETN',
          trip_from: 'VIETN',
          trip_to: 'VIETN',
          add_prem: true,
          bag_loss: true,
          proj_type: '0',
          insr_data: [], // 後給
          flight_data: [],
          language: this.cds.gLangType,
          token: this.cds.gToken,
          client_target: this.controls.certificate_number.value
            ? this.controls.certificate_number.value
            : this.cds.userInfo.certificate_number_12
            ? this.cds.userInfo.certificate_number_12
            : this.cds.userInfo.certificate_number_9,
          paymentNumber: this.paymentNumber,
        };
        // 加工被保人資訊
        payload.insr_data = this.packageInsr();
        // 機場加工
        payload.flight_data = this.packageAir();

        // 只有t1要刪除
        if (this.FPlan === 1) {
          delete payload.flight_data;
        }

        this.cds.gTCalc_to_save = {
          type: 'travel',
          saveorder: payload,
          userinfo: this.repPayload,
          paytype: this.FSelectPayType.type,
        };

        this.doPayment(this.cds.gTCalc_to_save, this.FSyncUserinfo);

        /** 資料送出後將緩存日期清空 */
        if (this.cds.gDatapicker.travel) {
          this.FTravelDate = undefined;
          delete this.cds.gDatapicker.travel;
        }
    }
  }

  syncUserinfoChange(event) {
    this.FSyncUserinfo = event;
  }

  /** 檢核被保人清單年齡 */
  checkInsr() {
    let mbol = false;
    this.FInsuredList.forEach((item, index) => {
      if (item.birthday) {
        item.error = CommonService.checkAge(
          this.FPlan,
          item.birthday,
          this.controls.rangeDateS.value
        );
        if (!mbol && item.error) {
          mbol = true;
        }
      }
    });
    if (mbol) {
      CommonService.scrollto('ly_insured');
    }

    return mbol;
  }

  /** 被保人加工 */
  packageInsr() {
    const jarr = [];
    const mPlan = this.FPlanId[this.FPlan - 1];

    this.FInsuredList.forEach((item, index) => {
      jarr.push({
        seq_no: index,
        insr_name: item.name,
        insr_cert_type: '1',
        insr_cert_number: item.id,
        sex: item.sex,
        insr_birthday: item.birthday,
        tral_proj: mPlan,
        amt: mPlan === 'T45' || mPlan === 'T46' ? this.Ft4AMT : 0,
      });
    });
    return jarr;
  }
  /** 機場加工 */
  packageAir() {
    const jarr = [];
    this.FAirPortList.forEach((item, index) => {
      if (item.airplaneNo) {
        jarr.push({
          seq_no: index,
          flight_no: item.airplaneNo,
          flight_date: item.airplaneDate,
          departure_airport: item.airPortS,
          arrival_airport: item.airPortE,
        });
      }
    });
    return jarr;
  }

  /** 被保人初始化 */
  createInsuredList() {
    const mindex = this.FUserList[this.FUserIndex].option;
    /** 將error清除 */
    this.FInsuredList.forEach((item) => (item.error = ''));
    /** 已經有資料的話是編輯帶來的，無須取代... */
    if (this.FInsuredList.length === mindex) {
      return;
    }
    /** 初始化資料 */
    this.FInsuredList = [];
    for (let index = 0; index < mindex; index++) {
      this.FInsuredList.push({
        name: '',
        id: '',
        birthday: '',
        recno: index,
        sex: '-1',
        sameInsure: false,
        error: '',
      });
    }
  }

  /**
   * @dec 快速理賠初始化
   * @param count 表示當前飛機的填寫數量
   * */
  createfastpair(count = 0) {
    if (this.FAirPortList.length === 0) {
      for (let index = 0; index < 2; index++) {
        this.FAirPortList.push({
          airplaneNo: '',
          airPortS: '',
          airPortE: '',
          airplaneDate: '',
        });
      }
    } else {
      for (let index = count; index < 2; index++) {
        this.FAirPortList.push({
          airplaneNo: '',
          airPortS: '',
          airPortE: '',
          airplaneDate: '',
        });
      }
    }
  }

  onPushfastpair() {
    if (this.FAirPortList.length < 5) {
      this.FAirPortList.push({
        airplaneNo: '',
        airPortS: '',
        airPortE: '',
        airplaneDate: '',
      });
    }
  }

  onfilterPosition(event) {
    const toUpper = function (x) {
      return x.toUpperCase();
    };

    event.stopPropagation();
    let mstr: string;
    event ? (mstr = event.target.value) : (mstr = '');
    this.FNationInput = mstr;
    const arr = this.cds.gPubCommon.nationList;
    this.FArr_in = arr.filter((item) => {
      if (item && item.nationType && item.nationNameCn) {
        return (
          item.nationType === '國內' &&
          item.nationNameCn.toUpperCase().includes(mstr.toUpperCase())
        );
      }
    });

    this.FArr_hot = arr.filter((item) => {
      if (item && item.nationType && item.nationNameCn) {
        return (
          item.nationType === '熱門' &&
          item.nationNameCn.toUpperCase().includes(mstr.toUpperCase())
        );
      }
    });

    this.FArr_noarr = arr.filter((item) => {
      if (item && item.nationType && item.nationNameCn) {
        return (
          item.nationType === '無法投保' &&
          item.nationNameCn.toUpperCase().includes(mstr.toUpperCase())
        );
      }
    });

    this.FArr_noraml = arr.filter((item) => {
      if (item && item.nationNameCn) {
        return (
          item.nationType === '' &&
          item.nationNameCn.toUpperCase().includes(mstr.toUpperCase())
        );
      }
    });

    // if (
    //   (!this.FAbroad && this.FNationInput.length >= 1) ||
    //   (this.travelpositionList.length === 0 && this.FNationInput.length >= 1)
    // ) {
    //   $('#dropdownFilter1').dropdown('show');
    // } else {
    //   $('#dropdownFilter1').dropdown('hide');
    // }
    $('#dropdownFilter1').dropdown('show');
  }

  dropdownFilterChange() {
    $('#dropdownFilter1').dropdown('toggle');
  }

  dropdownFilterClick() {
    $('#nationInput').focus();
  }

  onKeydownNationClick(event) {
    event.preventDefault();
    $('#dropdownFilter1').dropdown('hide');
  }

  onKeyNationClick(event) {
    event.preventDefault();
    let mstr: string;
    event ? (mstr = event.target.value) : (mstr = '');
    if (
      this.FArr_in.length + this.FArr_hot.length + this.FArr_noraml.length ===
      1
    ) {
      const result = this.FArr_in[0] || this.FArr_hot[0] || this.FArr_noraml[0];
      this.onNationClick(result, event);
    }
  }

  /**
   *
   * @description 國家點選
   * 條件1 選了越南不能選其他國家
   * 條件2 選了其他不能選越南
   * 條件3 不可超過10筆
   *
   * 動作 ，判斷陣列0 決定是國外國內
   */
  onNationClick(xitem, event) {
    event.preventDefault();
    if (xitem.active) {
      event.stopPropagation();
    }

    //  條件1
    if (
      this.travelpositionList.length > 0 &&
      this.travelpositionList[0].areaId === 'Vietnam'
    ) {
      return;
    }

    // 條件2
    if (
      this.travelpositionList.length > 0 &&
      this.travelpositionList[0].areaId !== 'Vietnam' &&
      xitem.nationNameEng === 'Vietnam'
    ) {
      return;
    }

    if (this.travelpositionList.length >= 10) {
      return;
    }

    const mi = this.travelpositionList.findIndex((item) => {
      if (xitem.nationNameEng === item.areaId) {
        event.target.value = '';
      }
      return xitem.nationNameEng === item.areaId;
    });

    if (mi < 0) {
      xitem.active = true;
      this.controls.Areainput.setValue('');
      this.travelpositionList.push({
        areaName: xitem.nationNameCn,
        areaId: xitem.nationNameEng,
        index: this.travelpositionList.length,
      });

      this.FAbroad = this.travelpositionList[0].areaId === 'Vietnam';
      this.inputFocus();
      // 有異動選項就重算吧
      this.resetCalc();
    }

    $('#dropdownFilter1').dropdown('hide');
  }

  onDelNationClick(xitem) {
    const mi = this.travelpositionList.findIndex((item) => {
      return xitem.index === item.index;
    });
    this.travelpositionList.splice(mi, 1);
    let mitem;
    mitem = this.FArr_in.find((item) => {
      return xitem.areaId === item.nationNameEng;
    }) as ITravelList;

    mitem =
      (this.FArr_hot.find((item) => {
        return xitem.areaId === item.nationNameEng;
      }) as ITravelList) || mitem;

    mitem =
      (this.FArr_noraml.find((item) => {
        return xitem.areaId === item.nationNameEng;
      }) as ITravelList) || mitem;

    if (mitem) {
      mitem.active = false;
    }

    // 有異動選項就重算吧
    this.resetCalc();
  }

  calcPlan() {
    let mint;

    this.FPlan > 3 ? (mint = this.FPlan - 4) : (mint = this.FPlan - 1);
    this.FProd_pol_code = this.FTravelCalcList[mint].prod_pol_code;
    this.FProd_pol_AMT = this.FTravelCalcList[mint].amt;
    this.Ft4AMT = this.FTravelCalcList[mint].t4amt;
    this.FAmount = this.FTravelCalcList[mint].amt;
    this.FAmtTax = this.FTravelCalcList[mint].amttax;
  }

  /** 動態元件區 */
  showfastpair(index) {
    const component =
      this.componentFactory.resolveComponentFactory(FastpairComponent);
    this.cfastpair.clear();
    this.comfastpair = this.cfastpair.createComponent(component);

    this.comfastpair.instance.modalName =
      index === 0
        ? this.FLang.C02.fastpair.L0001
        : index === 1
        ? this.FLang.C02.fastpair.L0011
        : this.FLang.C02.fastpair.L0012;

    this.comfastpair.instance.FType = 'calc';
    this.comfastpair.instance.data = {
      type: this.FAirPortList[index].airplaneNo ? 'edit' : 'add',
      ...this.FAirPortList[index],
      FPlan: this.FPlan,
      sdate: this.controls.rangeDateS.value,
      edate: this.controls.rangeDateE.value,
      FIndex: index,
    };

    this.comfastpair.instance.id = index;
    $('#modelFast').modal('show');

    // 被保人資料填寫中
    this.comfastpair.instance.xAction.subscribe((item) => {
      this.FAirPortList[index].airplaneNo = item.airplaneNo;
      this.FAirPortList[index].airPortS = item.airPortS;
      this.FAirPortList[index].airPortE = item.airPortE;
      this.FAirPortList[index].airplaneDate = item.airplaneDate;
      $('#modelFast').modal('hide');
    });
  }

  clearfastpair(index) {
    if (index >= 2) {
      this.FAirPortList.splice(index, 1);
    } else {
      this.FAirPortList[index].airplaneNo = '';
      this.FAirPortList[index].airPortS = '';
      this.FAirPortList[index].airPortE = '';
      this.FAirPortList[index].airplaneDate = '';
    }
  }

  showInsured(index) {
    const component =
      this.componentFactory.resolveComponentFactory(InsuredCardComponent);
    this.cInsured.clear();
    this.comInsured = this.cInsured.createComponent(component);

    this.comInsured.instance.data = {
      type: this.FInsuredList[index].id ? 'edit' : 'add',
      ...this.FInsuredList[index],
      FPlan: this.FPlan,
      FAbroad: this.FAbroad,
      FStartDay: this.controls.rangeDateS.value,
      SameUser: index === 0 ? true : false,
      FUserList: this.FInsuredList,
      activeSameUser:
        this.controls.name.value &&
        this.FInsuredList[index].name === this.controls.name.value
          ? true
          : false,
      activeSameId:
        this.controls.certificate_number.value &&
        this.FInsuredList[index].id === this.controls.certificate_number.value
          ? true
          : false,
      activeSameBirthday:
        this.controls.birthday.value &&
        this.FInsuredList[index].birthday === this.controls.birthday.value
          ? true
          : false,
      activeSameSex:
        this.controls.sex.value &&
        this.FInsuredList[index].sex === this.controls.sex.value
          ? true
          : false,
    };

    this.comInsured.instance.id = index;

    // 同投保人資訊
    if (index === 0) {
      this.comInsured.instance.data.someDate = {
        id: this.controls.certificate_number.value,
        name: this.controls.name.value,
        birthday: this.controls.birthday.value,
        sex: this.controls.sex.value,
      };
    }

    $('#modelInsured').modal('show');

    // 被保人資料填寫中
    this.comInsured.instance.xAction.subscribe((item) => {
      this.FInsuredList[index].name = item.name;
      this.FInsuredList[index].birthday = item.birthday;
      this.FInsuredList[index].id = item.id;
      this.FInsuredList[index].sex = item.sex;
      this.FInsuredList[index].sameInsure = item.some;
      $('#modelInsured').modal('hide');
    });
  }

  delInsured(index) {
    this.FInsuredList[index].name = '';
    this.FInsuredList[index].birthday = '';
    this.FInsuredList[index].id = '';
    this.FInsuredList[index].sex = '';
    this.FInsuredList[index].sameInsure = false;
    this.FInsuredList[index].error = '';
  }

  showLogin(xtype: string = 'both', checkID: boolean = false) {
    const component =
      this.componentFactory.resolveComponentFactory(LoginComponent);

    this.content.clear();
    const com = this.content.createComponent(component);
    $('#model-Login').modal('show');
    /** 彈窗否 */
    com.instance.xPop = true;
    com.instance.xtype = xtype;
    /** 是否是因為檢核ID而發起 */
    com.instance.xCheckid = checkID;
    // 立即註冊
    com.instance.xRegister.subscribe((item) => {
      this.cds.gPubCommon.gOtpAuto = 'register';
      $('#model-Login').modal('hide');
      this.go.navigate([
        '/online-insurance/project-trial-travel/',
        'personInfo',
      ]);
    });

    // 忘記密碼
    com.instance.xforgetPassword.subscribe((item) => {
      $('#model-Login').modal('hide');
      this.go.navigateByUrl('login/find-account');
    });

    // 登入
    com.instance.xlogin.subscribe((xitem) => {
      $('#model-Login').modal('hide');
      this.go.navigate([
        '/online-insurance/project-trial-travel/',
        'personInfo',
      ]);
      this.cds.userInfo.seq_no ? (this.FType = 'edit') : (this.FType = 'add');
      this.FUserInfo = this.cds.userInfo;
      /** 第三方登入 */
      if (xitem && xitem.type === 'social') {
        this.cds.gPubCommon.gOtpAuto = 'bind';
        this.FSocialName = xitem.name;
        this.FSocialAccount = xitem.account;
        this.FChannel = xitem.channel;
      }
      this.ly_userinfo?.onUpdateCDStoControl(this.cds.userInfo);
      this.FType = this.ly_userinfo.data.xType;
    });
  }

  /** 切版原始碼 */

  setPiceItemHeight(contain) {
    const titles = contain.find('.compare-title');
    const prices = contain.find('.compare-price');

    $(prices).each(function () {
      const priceItems = $(this).find('.compare-row').children();
      for (let i = 0; i < priceItems.length; i++) {
        const titleHeight = $(titles[i]).height();
        const item = $(priceItems[i]);
        const itemHeight = item.height();
        if (itemHeight !== titleHeight) {
          item.height(titleHeight);
        }
      }
    });
  }

  checkpayment() {
    let mbol: any;
    // 檢查被保人是否填寫完成
    mbol = this.FInsuredList.every((item) => item.name !== '');

    if (!mbol) {
      if ($(document).innerWidth() >= 992) {
        CommonService.scrollto('lyc_insrinfo', 52);
      } else {
        CommonService.scrollto('lyc_insrinfo');
      }
      this.FInsuredList.forEach((item) => {
        if (item.name === '') {
          item.error = this.FLang.ER.ER017;
        }
      });
      return;
    }
    if (!this.ckPayment) {
      this.doTipError();
      CommonService.scrollto(`lyc_pay`, 50);
      return;
    }

    return mbol && this.group.valid && this.ckPayment;
  }
  doTipError() {
    this.FPayNoError = '';
    if (this.paymentNumber < 0) {
      this.FPayNoError = this.FLang.PH.PH044;
    }
    this.FAgreeError = '';
    if (!this.ckPayment) {
      this.FAgreeError = this.FLang.PH.PH045;
    }
  }

  /** 付款切換 */
  paymentSwitch(index) {
    this.FSelectPayType = this.payList[index];
    this.paymentNumber = index;
  }

  /** 國內外下載文件 */
  downloadFiles(aborad: any, cardNumber: number) {
    return aborad
      ? cardNumber === 1
        ? 'assets/pdf/T3.pdf'
        : 'assets/pdf/T4.pdf'
      : 'assets/pdf/T1.pdf';
  }

  /** 支付方式 */
  dataTarget(index) {
    return '#Modal-0' + (index + 4);
  }

  /**  */
  inputFocus() {
    $('#nationInput').focus();
  }

  /** userInfo */
  async userinfo_action(payload) {
    if (payload) {
      const mstr = await this.apiMember.authThirdLogin(payload);
      this.showLogin(mstr, true);
    }
  }

  /**
   * @description 付款流程
   * @param {object} payload 物件
   * @property {string} type  [ car | motor | travel ],
   * @property {any} saveorder  [訂單存檔資料]
   * @property {any} userinfo  [當前用戶資訊]
   * @param {boolean} updateUser  [是否要更新會員]
   */
  async doPayment(payload, updateUser, action = null) {
    /** 首次註冊流程 透過seq_no === 0 判定是否執行 */
    if (updateUser && payload.userinfo.seq_no === 0) {
      this.pay.regeditFlow(
        payload.userinfo,
        payload.saveorder.certificate_number
      );
      this.go.navigate(['/login/otp-verification']);
      return;
    }

    /** 1 開始存檔 */
    const rep = await this.pay.savetxend(payload, updateUser);
    /** 1.1 無論有無核保成功都需取得 order_no aply_no */
    const { order_no, aply_no } = rep.data;
    this.FOrder_no = order_no;
    this.FAply_no = aply_no;

    if (rep.status === 200) {
      if (action === 'pay') {
        this.go.navigate(
          ['/online-insurance/project-trial-travel/', 'payment'],
          { replaceUrl: true }
        );
      } else {
        this.go.navigate([
          '/online-insurance/project-trial-travel/',
          'payment',
        ]);
      }

      /** 2 付款資訊緩存 */
      this.cds.gBeforPayinfo = {
        order_no,
        aply_no,
        prod_pol_code: payload.saveorder.prod_pol_code,
        amt: payload.saveorder.tot_prem_with_vat,
        uamt: payload.saveorder.tot_prem,
      };

      /** 2.1 V1 改版成後啟動 */
      if (CommonService.isIOS()) {
        this.FConfirm = this.calcsv.openPayConfirm();
        setTimeout(() => {
          $('#lyConfirm').modal('show');
        }, 300);
      } else {
        /** 2.2 開始付款 */
        this.ActionYesNo(true);
      }
    } else {
      /** 核保失敗 */
      this.FPayTransaction = true;
      // $('#paymentFailModal').modal('show');
    }
  }
  ActionYesNo(xresult) {
    if (xresult) {
      if (!this.FsubscribePayAction || this.FsubscribePayAction.closed) {
        this.FsubscribePayAction = this.pay.getPayObserve().subscribe(
          (params) => {
            if (params.status === '1') {
              this.FPayTransaction = false;
              this.FPayResult = true;
            }
            this.pay.editOrderState(
              params,
              params.status === '1' ? '4' : '5',
              this.paymentNumber as number
            );
            this.pay.showResult(params.status);
            // 付款成功後清除付款方式狀態
            this.cds.gTCalc_to_save.paytype = undefined;
          },
          (error) => {
            switch (error.status) {
              case -9:
                this.FPayTransaction = true;
                $('#paymentFailModal').modal('show');
                break;
            }
            console.log(error);
          }
        );
      }
      this.pay.doPayment(
        this.cds.gTCalc_to_save.saveorder.tot_prem_with_vat,
        this.FAply_no + moment().format('yyMMDDHHmmss'),
        this.cds.gTCalc_to_save.paytype
      );
    } else {
      this.go.navigate([
        '/online-insurance/project-trial-travel/',
        'personInfo',
      ]);
    }
    $('#lyConfirm').modal('hide');
  }

  alignPaymentText() {
    const paymentData = $('.payment-data');
    if (window.innerWidth <= 768) {
      if (paymentData.length > 0) {
        const maxWidth = Math.max(
          ...paymentData.map((key) => paymentData[key].offsetWidth)
        );
        paymentData.width(maxWidth);
      }
    } else {
      paymentData.css('width', 'auto');
    }
  }

  /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event) {
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
}
