import { DatePipe } from '@angular/common';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import {
  Validators,
  FormGroup,
  FormBuilder,
  AbstractControl,
} from '@angular/forms';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';

import * as moment from 'moment';
import { CommonService } from 'src/app/core/services/common.service';
@Component({
  selector: 'app-insured-card',
  templateUrl: './insured-card.component.html',
  styleUrls: ['./insured-card.component.scss'],
})
export class InsuredCardComponent implements OnInit {
  constructor(
    private vi: ValidatorsService,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private cds: DatapoolService
  ) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  /** 生日規則
   * 1. 最大歲數不得超過 100歲
   * 2. 最小歲數 日期不得小於當日
   * 3. 日期若沒資料時預設起始歲數30歲
   */
  get mixDate() {
    return moment(new Date()).subtract(100, 'years');
  }

  get maxDate() {
    return new Date();
  }

  get startDate() {
    return this.FBirthday
      ? moment(this.controls.birthday.value).toDate()
      : moment(new Date()).subtract(30, 'years');
  }
  @Input() data: any;
  @Input() id: number;
  @Output() xAction = new EventEmitter();
  FLang: any;
  FLangType: string;

  // 表單檢核
  controls: { [key: string]: AbstractControl };
  // controls;
  validators: {};
  group: FormGroup;
  FType: string;
  dayError: string;

  someUser: boolean;
  // 下方參數回傳需要
  FSomeUser: boolean;
  FTitleList: any;
  FTitle: string;

  // 用於處理同使用者 卻是首次登入的用戶.
  FBirthday: string;
  FID: string;
  FName: string;
  FSex: string;

  errorWord = '';

  FIdCheck = false;

  /** 被保人清單 */
  FUserList;

  // 生日是否開啟文字輸入功能 false為開啟
  FReadonly: boolean;

  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  /** radio 性別必填驗證 */
  checkSex(control: AbstractControl) {
    if (control.value === '0' || control.value === '1') {
      return null;
    } else {
      return { checkSex: true };
    }
  }
  ngAfterViewInit() {
    this.controls.id.valueChanges.subscribe(() => {
      this.errorWord = '';
    });
  }
  ngOnInit(): void {
    this.FTitleList = [
      this.FLang.C02.insured.L0001,
      this.FLang.C02.insured.L0011,
      this.FLang.C02.insured.L0012,
    ];
    this.FType = this.data.type;
    this.FTitle = this.FTitleList[this.data.index];
    this.someUser = this.data.SameUser;

    this.FUserList = this.data.FUserList;

    this.FSomeUser =
      this.data.activeSameUser &&
      this.data.activeSameId &&
      this.data.activeSameBirthday &&
      this.data.activeSameSex
        ? true
        : false;

    /** 表單驗證項目 */
    this.validators = {
      id: [this.data.id, [this.vi.fontLengthNumber, Validators.required]],
      name: [
        this.data.name,
        [
          Validators.required,
          this.vi.languageFormat,
          this.vi.languagelength(160),
        ],
      ],
      birthday: [this.data.birthday, [Validators.required]],
      sex: [
        this.data.sex,
        {
          validators: [Validators.required, this.checkSex],
          updateOn: 'change',
        },
      ],
    };
    this.formValidate();

    // 如果曾經勾起同登入者
    if (this.FSomeUser) {
      Object.keys(this.controls).forEach((key) => {
        this.controls[key].disable();
      });
    }

    // 日期元件寫入初始值
    // const mDs = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
    const mDD = this.datePipe.transform(this.data.birthday, 'dd/MM/yyyy');
    this.FBirthday = mDD || '';
    // 同投保人專用

    if (!this.data.someDate) {
      return;
    }

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      this.FReadonly = false;
    } else {
      this.FReadonly = true;
    }
  }

  // 判斷來源資料是否足夠
  checkUserinfo() {
    // 條件 個人資料區塊的身分證、姓名、生日及性別欄位為空
    return !(
      this.data.someDate.id &&
      this.data.someDate.name &&
      this.data.someDate.sex &&
      this.data.someDate.birthday
    );
  }

  /** 建立驗證表單 */
  formValidate() {
    // 建立驗證表單
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator, { updateOn: 'blur' });

    // 偵測物件是否異動
    this.group.valueChanges.subscribe((item) => {});

    // 日期異動時檢測
    this.group.get('birthday').valueChanges.subscribe((item) => {
      if (item && this.FIdCheck) {
        this.group
          .get('id')
          .setValidators([
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value
            ),
            this.vi.vn2020ID(this.controls.birthday.value),
            this.vi.fontLengthNumber,
          ]);
        this.group.get('id').updateValueAndValidity();
      }
    });

    // 性別異動時檢測
    this.group.get('sex').valueChanges.subscribe((item) => {
      if (item && this.FIdCheck) {
        this.group
          .get('id')
          .setValidators([
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value
            ),
            this.vi.fontLengthNumber,
          ]);
        this.group.get('id').updateValueAndValidity();
      }
    });

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }

  onAction() {
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    this.group
      .get('id')
      .setValidators([
        this.vi.fontLengthNumber,
        Validators.required,
        this.vi.vnCertNumber(
          this.controls.birthday.value,
          this.controls.sex.value
        ),
        this.vi.vn2020ID(this.controls.birthday.value),
      ]);
    this.group.get('id').updateValueAndValidity();
    /* 開啟日期與性別 身分證檢測 */
    this.FIdCheck = true;

    if ((this.group.invalid && !this.dayError) || this.errorWord) {
      return;
    }

    // 檢核清單內是否有重複清單
    if (
      this.FUserList &&
      Array.isArray(this.FUserList) &&
      this.FUserList.length > 0
    ) {
      const mbol = this.FUserList.find(
        (item, index) => item.id === this.controls.id.value && this.id !== index
      );
      if (mbol) {
        this.errorWord = this.FLang.C02.insured.L0015;
        return;
      }
    }

    // 判斷年齡 年齡規則
    // 國外旅遊  AC01001 (T1)
    // 銀(年齡6周~80歲)W7 index 4
    // 金(年齡6周~80歲)W1 index 5
    // 鑽石(年齡18-70)W2 index 6

    // 國內旅遊
    // AC01005  - AC01013
    // 銀(年齡6周~80歲) (T3) index 1
    // 金(年齡6周~80歲)(T4) index 2
    // 鑽石(年齡6周~80歲) (T4) index 3

    // 國內 統一都是6s~80
    // 國外 看規則
    const mbirthday = this.datePipe.transform(
      this.controls.birthday.value,
      'yyyy-MM-dd'
    );

    const startday = this.datePipe.transform(this.data.FStartDay, 'yyyy-MM-dd');
    this.dayError = CommonService.checkAge(
      this.data.FPlan,
      mbirthday,
      startday
    );

    if (this.dayError) {
      return;
    }
    const payload = {
      id: this.controls.id.value,
      name: this.controls.name.value,
      birthday: this.controls.birthday.value,
      sex: this.controls.sex.value,
      some: this.FSomeUser,
    };
    this.xAction.emit(payload);
  }

  checkIsMember() {
    return this.cds.userInfo.seq_no ? true : false;
  }

  setUserDate(id, name, birthday, sex) {
    this.controls.id.setValue(id);
    this.controls.name.setValue(name);
    this.controls.birthday.setValue(birthday);
    this.controls.sex.setValue(sex);
  }

  onSomeUser(id) {
    // 動作是反向的 事件先觸發，值才會變  someUser
    this.FID = this.data.someDate.id;
    this.FSex = this.data.someDate.sex;

    if (!this.FSomeUser) {
      if (this.checkIsMember()) {
        this.setUserDate(
          this.cds.userInfo.certificate_number_12 ||
            this.cds.userInfo.certificate_number_9,
          this.cds.userInfo.customer_name.trim(),
          this.cds.userInfo.birthday,
          this.cds.userInfo.sex
        );
      } else {
        const { id, name, birthday, sex } = this.data.someDate;
        this.setUserDate(id, name, birthday, sex);
      }

      /** 顯示用跟存檔用不同 */
      const mbirthday = this.datePipe.transform(
        this.cds.userInfo.birthday || this.data.someDate.birthday,
        'dd/MM/yyyy'
      );
      // 顯示用跟存檔用不同
      const mbirthday1 = this.datePipe.transform(
        this.cds.userInfo.birthday || this.data.someDate.birthday,
        'dd-MM-yyyy'
      );
      // this.FName && this.FBirthday && this.FID &&
      Object.keys(this.controls).forEach((key) => {
        this.controls[key].disable();
      });
      this.FBirthday = mbirthday;
    } else {
      this.controls.name.setValue('');
      this.controls.id.setValue('');
      this.controls.sex.setValue('-1');
      this.controls.birthday.setValue('');

      Object.keys(this.controls).forEach((key) => {
        this.controls[key].enable();
        this.controls[key].markAsUntouched();
      });
      this.FBirthday = '';

      // checkbox取消時清空日期
      $(`#inusred${id + 1}`).val('');
    }
  }

  closemodel() {
    $('#modelInsured').modal('hide');
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required' || type === 'checkSex') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          id: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES004,
          name: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          birthday:
            this.FLang.ER.select.ER001 +
            ' ' +
            this.FLang.ER.select.ER001_.ES005,
          sex:
            this.FLang.ER.select.ER001 +
            ' ' +
            this.FLang.ER.select.ER001_.ES001,
        }[control_key];
      }
      /** 新檢核end */

      return (
        {
          vnCertNumber: this.FLang.ER.ER036,
          vn2020ID:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
          fontLengthNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
          required: this.FLang.ER.ER017,
          addr: this.FLang.ER.ER023,
          languageFormat:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          languagelength:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES001,
        }[type] || this.FLang.ER.ER023
      );
    }
  }

  getDate(event) {
    if (!event) {
      return;
    }
    const datePipe = event.indexOf('-');
    let date;
    if (datePipe === -1) {
      date = this.datePipe.transform(event, 'yyyy-MM-dd');
    }

    if (date) {
      this.controls.birthday.setValue(date);
      this.FBirthday = moment(date).format('DD/MM/YYYY');
    } else {
      this.controls.birthday.setValue(event);
      this.FBirthday = moment(event).format('DD/MM/YYYY');
    }

    this.controls.birthday.markAsTouched();
  }

  /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event){
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
}
