import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss'],
})
export class CardsComponent implements OnInit {
  @Input() cardList;
  FLang: any = this.cds.gLang.C01;
  name: any;

  constructor(private router: Router, private cds: DatapoolService) {}

  ngOnInit(): void {}

  /** 險種顏色 */
  filterClassStyle(index) {
    return index === this.FLang.C0005 ? 'label-explain' : '';
  }

  /** 國內外旅遊 */
  filterTravel(kind) {
    switch (kind) {
      case 'internal':
        return this.FLang.C0005_.card1.s001;
      case 'oversea':
        return this.FLang.C0005_.card2.s001;
      default:
        return '';
    }
  }

  titleClassStyle(index) {
    const style = 'label';
    return index === this.FLang.C0005
      ? style + ' is-orange'
      : style + ' is-green';
  }

  switchLink(type, kind) {
    // 由此流程進入必屬於新增流程
    if (this.cds.gTCalc_to_save) {
      this.cds.gTCalc_to_save.type = 'none';
    }
    this.cds.gTrafficicinsurance = null;
    this.cds.gTrafficTravelInsurance = null;

    if (type === 'travel') {
      this.router.navigate([`online-insurance/project-trial-${type}/calcAmt`], {
        queryParams: {
          xtype: kind === 'internal' ? 'in' : 'out',
        },
      });
    } else {
      this.router.navigate([`online-insurance/project-trial-${type}/calcAmt`]);
    }
    localStorage.setItem('status', 'card');
  }
}
