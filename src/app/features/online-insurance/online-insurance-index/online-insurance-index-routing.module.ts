import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnlineInsuranceIndexComponent } from './online-insurance-index.component';

const routes: Routes = [{ path: '', component: OnlineInsuranceIndexComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnlineInsuranceIndexRoutingModule { }
