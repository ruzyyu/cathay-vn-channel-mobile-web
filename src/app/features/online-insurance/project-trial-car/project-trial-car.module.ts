import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectTrialCarRoutingModule } from './project-trial-car-routing.module';
import { ProjectTrialCarComponent } from './project-trial-car.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ProjectTrialCarComponent],
  imports: [CommonModule, ProjectTrialCarRoutingModule, SharedModule],
})
export class ProjectTrialCarModule {}
