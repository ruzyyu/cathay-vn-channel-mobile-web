import { CommonService } from './../../../core/services/common.service';
import { Router } from '@angular/router';
import {
  Component,
  OnInit,
  AfterContentInit,
  AfterViewInit,
  HostListener,
} from '@angular/core';
import { ITravelPageData } from 'src/app/core/interface';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { V1Component } from 'src/app/shared/components/travel/v1/v1.component';
import { T44Component } from 'src/app/shared/components/travel/t44/t44.component';
import { T44v1Component } from 'src/app/shared/components/travel/t44v1/t44v1.component';
import { W1Component } from 'src/app/shared/components/travel/w1/w1.component';
import { W2Component } from 'src/app/shared/components/travel/w2/w2.component';
import { W7Component } from 'src/app/shared/components/travel/w7/w7.component';

@Component({
  selector: 'app-travel-insurance',
  templateUrl: './travel-insurance.component.html',
  styleUrls: ['./travel-insurance.component.scss'],
})
export class TravelInsuranceComponent
  implements OnInit, AfterContentInit, AfterViewInit {
  constructor(private router: Router, private cds: DatapoolService) {
    this.FLangType = this.cds.gLangType;
  }
  FLangType: any;

  activePriceTable: any;

  pageData: ITravelPageData = {
    status: 0,
  };

  // 取參數值
  type: string;

  // 元件狀態器
  chooseComponent = '';
  mapping = new Map<string, any>([
    ['v1', V1Component],
    ['t44', T44Component],
    ['t44v1', T44v1Component],
    ['W1', W1Component],
    ['W2', W2Component],
    ['W7', W7Component],
  ]);
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.amendButtonPosition();
  }
  ngOnInit(): void {}

  ngAfterViewInit() {
    CommonService.bindSlick(1);
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
    this.amendButtonPosition();
  }

  /* 判斷Banner尺寸按鈕位置，設置四個尺寸
   * 1. 1200~2300
   * 2. 1199~768
   * 3. 768~375
   * 4. 375~320 */
  amendButtonPosition() {
    if (window.innerWidth <= 2300 && window.innerWidth >= 1200) {
      const left = 602 - (2300 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth <= 1199 && window.innerWidth >= 768) {
      const left = 257 - (1199 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth < 768 && window.innerWidth > 375) {
      const left = 284 - (768 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
    if (window.innerWidth <= 375) {
      const left = 107 - (375 - window.innerWidth) / 2;
      $('.banner_button').css('left', `${left}px`);
    }
  }

  // 20201222 modi by 子彥
  ngAfterContentInit() {
    const vm = this;
    $(() => {
      CommonService.bindSlick(1);
      $('a[role="tab"]').on('shown.bs.tab', function (e) {
        const ariaControls = $(e.target).attr('aria-controls');

        // 兩個 tab 連動變換樣式
        $('.commodity-tab').each(function () {
          const tabs = $(this).children();
          for (const tab of tabs) {
            const item = $(tab).children();
            if (item.attr('aria-controls') === ariaControls) {
              item.tab('show');
            }
          }
        });

        // 取得切換之價格表，並重新設置項目高度
        const priceContain = $('#' + ariaControls).find('.table-compare');
        vm.setPiceItemHeight(priceContain);
      });

      $(window).resize(() => {
        this.toggleShowTab();
        this.setPiceItemHeight(this.activePriceTable);
      });

      $(document).scroll(() => {
        this.toggleShowTab();
      });

      // 取得當前顯示價格表
      const priceTable = $('.table-compare');
      this.setPiceItemHeight(priceTable);
    });
  }

  // 判斷是否進入 tab 區域
  isEnterTab(tab, customHeight?) {
    const winScrollTop = $(window).scrollTop();
    const tabContainerHeight = tab.parent().height();
    const tabOffsetX = tab?.offset()?.top || 0;
    const tabContainerStart = tabOffsetX + (customHeight || 0);
    const tabContainerEnd = tabOffsetX + tabContainerHeight;
    return winScrollTop > tabContainerStart && winScrollTop < tabContainerEnd;
  }

  // 顯示、隱藏固定在 window 上方 tab
  toggleShowTab() {
    const tabOuter = $('.commodity-tab-wrapper');
    const tab = $('.second-tab.commodity-tab');
    const tabH = tab?.outerHeight();
    const outsideTab = $('.bookmark-top');
    const isPC = $(window).width() > 767;

    tabOuter.css('min-height', tabH);
    if (isPC) {
      // 客製出現高度
      this.isEnterTab(tabOuter, 20)
        ? outsideTab.addClass('show')
        : outsideTab.removeClass('show');
      return;
    }
    this.isEnterTab(tabOuter)
      ? tab.addClass('fixed-top')
      : tab.removeClass('fixed-top');
  }

  // 價格表項目與標題等高
  setPiceItemHeight(contain) {
    const titles = contain?.find('.compare-title');
    const prices = contain?.find('.compare-price');
    $(prices).each(() => {
      const priceItems = $(this).find('tbody').children();
      for (let i = 0; i < priceItems.length; i++) {
        const titleHeight = $(titles[i]).height();
        const itemRow = $(priceItems[i]);
        const item = itemRow.children().first();
        const itemHeight = item.height();
        if (itemHeight !== titleHeight) {
          item.height(titleHeight + 1);
        }
      }
    });
  }
  // 20201222 modi by 子彥

  // banner 跳轉
  bannerUrl() {
    // 不分國內外跳轉至試算頁
    this.router.navigate(['/online-insurance/project-trial-travel/calcAmt']);
  }

  /** banner 圖片 語言 switch */
  bannerImg(size: number) {
    const img1920 = 'assets/img/commodity/bn-travel-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-travel-1200-x-400';
    const img800 = 'assets/img/commodity/bn-travel-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '.png';
        case 2:
          return img1200 + '.png';
        case 3:
          return img800 + '.png';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en.png';
        case 2:
          return img1200 + '-en.png';
        case 3:
          return img800 + '-en.png';
      }
    }
  }

  /** banner 圖片 語言 switch */
  bannerImgBig(size: number) {
    const img1920 = 'assets/img/commodity/bn-travel-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-travel-1200-x-400';
    const img800 = 'assets/img/commodity/bn-travel-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '@2x.png 2x';
        case 2:
          return img1200 + '@2x.png 2x';
        case 3:
          return img800 + '@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en@2x.png 2x';
        case 2:
          return img1200 + '-en@2x.png 2x';
        case 3:
          return img800 + '-en@2x.png 2x';
      }
    }
  }

  checkI18n() {
    if (this.FLangType === 'vi-VN') {
      return 'banner_vn';
    }
    if (this.FLangType === 'zh-TW') {
      return 'banner_tw';
    }
    if (this.FLangType === 'en-US') {
      return 'banner_en';
    }
  }
}
