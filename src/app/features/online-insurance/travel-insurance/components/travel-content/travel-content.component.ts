import { IPrice } from './../../../project-trial-travel/project-trial-travel.component';
import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';
import { ITravelList } from '../../../project-trial-travel/project-trial-travel.component';
import { ITravelPageData, UserInfo } from 'src/app/core/interface';

@Component({
  selector: 'app-travel-content',
  templateUrl: './travel-content.component.html',
  styleUrls: ['./travel-content.component.scss'],
})
export class TravelContentComponent implements OnInit {
  @Output() status = new EventEmitter<string>();
  @Input() pageData: ITravelPageData;

  @Input() xchooseComponent: string;
  @Output() xchooseComponentChange = new EventEmitter();
  // 變數宣告
  items = [];
  FLang: any;

  FLangType: string;
  FUserInfo: UserInfo;

  /** 表單變數 */
  controls: any;
  FTimelist: Array<any> = [];
  // 時間緩存器
  FTimeIndex: any = -1;
  // 境內境外 true 境內
  FAbroad: boolean;
  // 旅遊地點緩存
  travelpositionList: Array<ITravelList>;
  // 試算結果
  FTravelCalcList: Array<IPrice> = [];

  // 狀態器
  FPlan = 2;

  chooseComponent = '';

  isTouched: boolean;

  constructor(private numberPipe: NumberPipe, private cds: DatapoolService) {
    this.FLang = this.cds.gLang.D03;
    this.FLangType = this.cds.gLangType;
    this.isTouched = false;
  }

  @HostListener('document:scroll')
  onScroll() {
    if (this.isTouched) {
      $('.slickcard').slick('slickSetOption', 'swipe', false);
    }
  }

  @HostListener('document:touchstart')
  onTouchstart() {
    this.isTouched = true;
  }

  @HostListener('document:touchend')
  onTouchend() {
    $('.slickcard').slick('slickSetOption', 'swipe', true);
    this.isTouched = false;
  }

  ngOnInit(): void {
    this.items = [
      {
        id: 1,
        img: 'assets/img/commodity/img-bx-domestic.png',
        srcset: 'assets/img/commodity/img-bx-domestic@2x.png 2x',
        title: this.FLang.N0003,
        icons: [
          {
            defaultImg: 'assets/img/commodity/ic-claim-accident.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-accident@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-accident-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-accident-w@2x.png 2x',
          },
          {
            defaultImg: 'assets/img/commodity/ic-claim-delay.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-delay@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-delay-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-delay-w@2x.png 2x',
          },
          {
            defaultImg: 'assets/img/commodity/ic-claim-cancel.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-cancel@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-cancel-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-cancel-w@2x.png 2x',
          },
          {
            defaultImg: 'assets/img/commodity/ic-claim-bagdelay.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-bagdelay@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-bagdelay-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-bagdelay-w@2x.png 2x',
          },
          {
            defaultImg: 'assets/img/commodity/ic-claim-ambulance.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-ambulance@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-ambulance-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-ambulance-w@2x.png 2x',
          },
        ],
        content: [
          {
            title: this.FLang.N0004,
            directions: this.FLang.N0004_.s001,
            tag: this.FLang.N0004_.s002,
            money: this.FLang.N0004_.s003,
          },
          {
            title: this.FLang.N0005,
            directions: this.FLang.N0005_.s001,
            tag: this.FLang.N0005_.s002,
            money: this.FLang.N0005_.s003,
          },
          {
            title: this.FLang.N0006,
            directions: this.FLang.N0006_.s001,
            tag: this.FLang.N0006_.s002,
            money: this.FLang.N0006_.s003,
          },
          {
            title: this.FLang.N0007,
            directions: this.FLang.N0007_.s001,
            tag: this.FLang.N0007_.s002,
            money: this.FLang.N0007_.s003,
          },
          {
            title: this.FLang.N0008,
            directions: this.FLang.N0005_.s001,
            tag: this.FLang.N0008_.s002,
            money: this.FLang.N0008_.s003,
          },
        ],
      },
      {
        id: 2,
        title: this.FLang.N0009,
        img: 'assets/img/commodity/img-bx-foreign.png',
        srcset: 'assets/img/commodity/img-bx-foreign@2x.png 2x',
        icons: [
          {
            defaultImg: 'assets/img/commodity/ic-claim-accident.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-accident@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-accident-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-accident-w@2x.png 2x',
          },
          {
            defaultImg: 'assets/img/commodity/ic-claim-reciept.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-reciept@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-reciept-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-reciept-w@2x.png 2x',
          },
          {
            defaultImg: 'assets/img/commodity/ic-claim-document.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-document@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-document-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-document-w@2x.png 2x',
          },
          {
            defaultImg: 'assets/img/commodity/ic-claim-delay.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-delay@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-delay-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-delay-w@2x.png 2x',
          },
          {
            defaultImg: 'assets/img/commodity/ic-claim-other.png',
            defaultSrcset: 'assets/img/commodity/ic-claim-other@2x.png 2x',
            activeImg: 'assets/img/commodity/ic-claim-other-w.png',
            activeSrcset: 'assets/img/commodity/ic-claim-other-w@2x.png 2x',
          },
        ],
        content: [
          {
            title: this.FLang.N0010,
            directions: this.FLang.N0010_.s001,
            tag: this.FLang.N0010_.s002,
            money: this.FLang.N0010_.s003,
          },
          {
            title: this.FLang.N0011,
            directions: this.FLang.N0011_.s001,
            tag: this.FLang.N0011_.s002,
            money: this.FLang.N0011_.s003,
          },
          {
            title: this.FLang.N0012,
            directions: this.FLang.N0012_.s001,
            tag: this.FLang.N0012_.s002,
            money: this.FLang.N0012_.s003,
          },
          {
            title: this.FLang.N0013,
            directions: this.FLang.N0013_.s001,
            tag: this.FLang.N0013_.s002,
            money: this.FLang.N0013_.s003,
          },
          {
            title: this.FLang.N0014,
            directions: this.FLang.N0014_.s001,
            tag: this.FLang.N0014_.s002,
            money: this.FLang.N0014_.s003,
          },
        ],
      },
    ];
  }

  idClass(index) {
    return 'commodity-' + this.numberPipe.transform(index + 1);
  }

  ariaNumber(index) {
    return 'commodity-' + this.numberPipe.transform(index + 1) + '-tab';
  }

  cssClass(index) {
    const style = 'tab-pane fade bg-commodity p-0';
    return index === 0 ? style + ' show active' : style;
  }
}
