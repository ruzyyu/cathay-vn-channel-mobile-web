import { Component, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-card-slide',
  templateUrl: './card-slide.component.html',
  styleUrls: ['./card-slide.component.scss'],
})
export class CardSlideComponent implements OnInit {
  // 變數宣告
  FLang: any;
  FLangType: string;

  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {
    this.doSlick();
    this.initCarousel();

    $(window).resize(() => {
      this.setPiceItemHeight();
    });

    $(window).ready(() => {
      this.setPiceItemHeight();
    });
  }

  doSlick() {
    $('.teaching-phone .slickcard')
      .not('.slick-initialized')
      .slick({
        dots: true,
        infinite: false,
        variableWidth: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 1000,
        responsive: [
          {
            breakpoint: 1440.98,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
              focusOnSelect: true,
            },
          },
          {
            breakpoint: 991.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: false,
              dots: true,
              focusOnSelect: true,
            },
          },
          {
            breakpoint: 767.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerMode: true,
              focusOnSelect: true,
            },
          },
        ],
      });

    $('.teaching-phone .slickcard').on(
      'beforeChange',
      (event, slick, currentSlide, nextSlide) => {
        $('#cardSlide').carousel(nextSlide);
      }
    );
  }

  // 初始化輪播套件 (手機圖片輪播)
  initCarousel() {
    // 初始化輪播套件
    $('#cardSlide').carousel();

    // 小網 tag跟輪播同時動作
    $('#cardSlide').on('slide.bs.carousel', (e: any) => {
      $('.teaching-phone .slickcard').slick('slickGoTo', e.to);
    });
  }

  setPiceItemHeight() {
    const addHeight = $('.compare-price .addheight');
    const compareTitle = $('.compare-title');
    for (let i = 0; i < compareTitle.length; i++) {
      $(addHeight[i]).css(
        'height',
        $(compareTitle[i]).innerHeight() + 1 + 'px'
      );
    }
  }

  /** 調整 投保3步驟 mobile 高度問題 */
  adjustHeight() {
    const style = 'card-height t-step-row';
    switch (this.FLangType) {
      case 'en-US':
        return 't-step-row-en ' + style;
      case 'vi-VN':
        return 't-step-row-vn ' + style;
      default:
        return 't-step-row-tw ' + style;
    }
  }

  // 調整 投保3步驟 web 第一張
  adjustHeightWeb() {
    const style = 'card-height t-step-row active';
    switch (this.FLangType) {
      case 'en-US':
        return 't-step-row-en ' + style;
      case 'vi-VN':
        return 't-step-row-vn ' + style;
      default:
        return 't-step-row-tw ' + style;
    }
  }

  stepImg(size: number) {
    const img = 'assets/img/commodity/img-screen';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img + '-step-1.png';
        case 2:
          return img + '-step-2.png';
        case 3:
          return img + '-step-3.png';
      }
    } else {
      switch (size) {
        case 1:
          return img + '-en-step-1.png';
        case 2:
          return img + '-en-step-2.png';
        case 3:
          return img + '-en-step-3.png';
      }
    }
  }

  stepImgBig(size: number) {
    const img = 'assets/img/commodity/img-screen';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img + '-step-1@2x.png 2x';
        case 2:
          return img + '-step-2@2x.png 2x';
        case 3:
          return img + '-step-3@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img + '-en-step-1@2x.png 2x';
        case 2:
          return img + '-en-step-2@2x.png 2x';
        case 3:
          return img + '-en-step-3@2x.png 2x';
      }
    }
  }
}
