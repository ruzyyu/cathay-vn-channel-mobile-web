import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/core/services/common.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.scss'],
})
export class BookmarkComponent implements OnInit {
  // 變數宣告
  FLang: any;

  constructor(private cds: DatapoolService, private router: Router) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {}

  tagUrl(index) {
    // 刷新 slick
    CommonService.refreshSlick();

    switch (index) {
      case 1:
        this.router.navigate(['/online-insurance/travel-insurance'], {
          queryParams: { xtype: 'in' },
          fragment: 'bookmark-anchor',
        });
        break;
      case 2:
        this.router.navigate(['/online-insurance/travel-insurance'], {
          queryParams: { xtype: 'out' },
          fragment: 'bookmark-anchor',
        });
        break;
      default:
        this.router.navigate(['/online-insurance/travel-insurance'], {
          queryParams: { xtype: 'in' },
          fragment: 'bookmark-anchor',
        });
        break;
    }
  }
}
