import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NumberPipe } from 'src/app/shared/pipes/css-pipe.pipe';
import { CommonService } from 'src/app/core/services/common.service';

@Component({
  selector: 'app-liability-insurance',
  templateUrl: './liability-insurance.component.html',
  styleUrls: ['./liability-insurance.component.scss'],
})
export class LiabilityInsuranceComponent implements OnInit, AfterViewInit {
  /** 變數 */
  activeIndex = 0;
  FLang: any;
  FLangType: string;
  items: any;
  insuranceRangeList: any;
  faqList: any;

  constructor(private numberPipe: NumberPipe, private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {
    this.items = {
      icons: [
        {
          defaultImg: 'assets/img/commodity/ic-claim-liability-pollution.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-liability-pollution@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-liability-pollution-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-liability-pollution-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-liability-poison.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-liability-poison@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-liability-poison-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-liability-poison-w@2x.png 2x',
        },
        {
          defaultImg: 'assets/img/commodity/ic-claim-liability-shelf.png',
          defaultSrcset:
            'assets/img/commodity/ic-claim-liability-shelf@2x.png 2x',
          activeImg: 'assets/img/commodity/ic-claim-liability-shelf-w.png',
          activeSrcset:
            'assets/img/commodity/ic-claim-liability-shelf-w@2x.png 2x',
        },
      ],
      content: [
        {
          title: this.FLang.E02.L0002,
          directions: this.FLang.E02.L0002_.s001,
        },
        {
          title: this.FLang.E02.L0003,
          directions: this.FLang.E02.L0003_.s001,
        },
        {
          title: this.FLang.E02.L0004,
          directions: this.FLang.E02.L0004_.s001,
        },
      ],
    };

    this.insuranceRangeList = [
      {
        title: this.FLang.E02.L0006,
        directions: this.FLang.E02.L0006_.s001,
        content: this.FLang.E02.L0006_.s002,
        defaultImg: 'assets/img/commodity/liability-item-01.png',
        defaultSrcset: 'assets/img/commodity/liability-item-01@2x.png 2x',
        activeImg: 'assets/img/commodity/liability-item-01-m.png',
        activeSrcset: 'assets/img/commodity/liability-item-01-m@2x.png 2x',
      },
      {
        title: this.FLang.E02.L0007,
        directions: this.FLang.E02.L0007_.s001,
        content: this.FLang.E02.L0007_.s002,
        defaultImg: 'assets/img/commodity/liability-item-02.png',
        defaultSrcset: 'assets/img/commodity/liability-item-02@2x.png 2x',
        activeImg: 'assets/img/commodity/liability-item-02-m.png',
        activeSrcset: 'assets/img/commodity/liability-item-02-m@2x.png 2x',
      },
      {
        title: this.FLang.E02.L0008,
        directions: this.FLang.E02.L0008_.s001,
        content: this.FLang.E02.L0008_.s002,
        defaultImg: 'assets/img/commodity/liability-item-03.png',
        defaultSrcset: 'assets/img/commodity/liability-item-03@2x.png 2x',
        activeImg: 'assets/img/commodity/liability-item-03-m.png',
        activeSrcset: 'assets/img/commodity/liability-item-03-m@2x.png 2x',
      },
    ];

    this.faqList = [
      {
        title: this.FLang.E02.L0010,
        content: this.FLang.E02.L0010_.s001,
      },
      {
        title: this.FLang.E02.L0011,
        content: this.FLang.E02.L0011_.s001,
      },
      {
        title: this.FLang.E02.L0012,
        content: this.FLang.E02.L0012_.s001,
      },
    ];
  }

  ngAfterViewInit(): void {
    CommonService.autoScrollToAccordionHeader('#accordionQuestion');
  }

  /** Number 轉換 */
  dataFilterNumber(index) {
    return 'select-' + this.numberPipe.transform(index + 2);
  }

  clickStyle(index) {
    this.activeIndex = index;
  }

  cssClassNumber(index) {
    let classPipe = this.numberPipe.transform(index + 2);
    if (index === this.activeIndex) {
      classPipe += ' active';
    }
    return classPipe;
  }

  /** 圖片位置 */
  switchPosition(index) {
    const style = 'col-sm-12 col-lg-6';
    return index % 2 === 0 ? style : style + ' order-lg-first';
  }

  /** FAQ list */
  titleId(index: number) {
    return 'FaqQuestion' + this.numberPipe.transform(index + 1);
  }
  titleDataTarget(index: number) {
    return '#collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  titleAriaControls(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoId(index: number) {
    return 'collapseFaqQue' + this.numberPipe.transform(index + 1);
  }
  infoAriaLabelledby(index: number) {
    return 'FaqQuestionQue' + this.numberPipe.transform(index + 1);
  }
  titleContent(item: any) {
    return `
    <div class="tab-title d-flex">
      <h4>${item.title}</h4>
      <i class="icons-ic-faq-arrow-up i-icon i-up"></i>
      <i class="icons-ic-faq-arrow-down i-icon i-down"></i>
    </div>
    `;
  }

  infoContent(item: any) {
    return `
    <div class="que-body d-flex">
      <div class="answer custom_text_align white-space-normal">${item.content}</div>
    </div>
    `;
  }

  /** banner 圖片 語言 switch */
  bannerImg(size: number) {
    const img1920 = 'assets/img/commodity/bn-liability-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-liability-1200-x-400';
    const img800 = 'assets/img/commodity/bn-liability-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '.png';
        case 2:
          return img1200 + '.png';
        case 3:
          return img800 + '.png';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en.png';
        case 2:
          return img1200 + '-en.png';
        case 3:
          return img800 + '-en.png';
      }
    }
  }

  /** banner 圖片 語言 switch */
  bannerImgBig(size: number) {
    const img1920 = 'assets/img/commodity/bn-liability-1920-x-500';
    const img1200 = 'assets/img/commodity/bn-liability-1200-x-400';
    const img800 = 'assets/img/commodity/bn-liability-800-x-470';
    if (this.FLangType === 'vi-VN') {
      switch (size) {
        case 1:
          return img1920 + '@2x.png 2x';
        case 2:
          return img1200 + '@2x.png 2x';
        case 3:
          return img800 + '@2x.png 2x';
      }
    } else {
      switch (size) {
        case 1:
          return img1920 + '-en@2x.png 2x';
        case 2:
          return img1200 + '-en@2x.png 2x';
        case 3:
          return img800 + '-en@2x.png 2x';
      }
    }
  }
}
