import { Component, Input, OnInit } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
})
export class ProjectComponent implements OnInit {
  @Input() childItemsOnline;
  @Input() childItemsOffline;
  FLang: any;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.D01;
  }

  ngOnInit(): void {}

  styleClass(index) {
    const style = 'col-sm-12 col-lg-6';
    return index === 0 ? style + ' order-lg-first' : style;
  }
}
