import { Location } from '@angular/common';
import { IResponse } from './../../../core/interface/index';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalService } from 'src/app/core/services/api/local/local.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss'],
})
export class NewsDetailComponent implements OnInit {
  FLang: any;
  newsContent: any;

  // news 類別
  tagId: any;
  // news 卡代號
  id: any;

  constructor(
    private router: Router,
    private cds: DatapoolService,
    private localService: LocalService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {
    /** 最新消息內容 */
    this.newsContent = {
      content: undefined,
      type_id: undefined,
      type: undefined,
      image_url: [undefined, undefined],
      start_date: undefined,
      end_date: undefined,
      title: undefined,
    };

    // news card 取參數 利用參數去判斷
    this.route.queryParams.subscribe((queryParams) => {
      // 最新消息列表編號 優惠活動 = 2  媒體新聞 = 3
      this.tagId = queryParams.type;
      // 最新消息 內容id
      this.id = queryParams.option;
      if (this.id) {
        this.getNewsContent(this.id);
      }
    });

    // this.newsContent.filter((type) => {
    //   console.log(this.tagId);
    //   console.log(type.type_id.toString());
    //   return this.tagId === type.type_id.toString();
    // });
  }

  /** 取得最新消息內容
   * id: 資料 Id
   */

  /** 這裡邏輯還要改 改用參數去判斷 By Agnes 2021-03-04 */

  async getNewsContent(id) {
    const rep: IResponse = await this.localService.NewsGetContent({ id });
    this.newsContent = rep.data || [];

    if (rep.status === 200) {
      this.newsContent = rep.data;

      if (this.newsContent.default_image && this.newsContent.image_url.length === 0) {
        switch (this.newsContent.default_image) {
          case '1':
            this.newsContent.image_url.push('assets/img/new/img-latestnews-01.jpg');
            break;
          case '2':
            this.newsContent.image_url.push('assets/img/new/img-latestnews-02.jpg');
            break;
          case '3':
            this.newsContent.image_url.push('assets/img/new/img-latestnews-03.jpg');
            break;
          case '4':
            this.newsContent.image_url.push('assets/img/new/img-latestnews-04.jpg');
            break;
          case '5':
            this.newsContent.image_url.push('assets/img/new/img-latestnews-05.jpg');
            break;
          default:
            return;
        }
      }
    }
  }

  goPrev() {
    this.location.back();
  }

  goActivity() {
    /** 暫放連結 */
    this.router.navigateByUrl('news?type=3');
  }

  styleClass(typeId: number) {
    const style = 'label';
    return typeId === 3 ? style + ' is-orange' : style + ' is-green';
  }
}
