import { TestBed, waitForAsync } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';
import { HtmlTransformPipe } from './html-transform.pipe';

describe('HtmlTransformPipe', () => {
	beforeEach(
		waitForAsync(() => {
			TestBed.configureTestingModule({
				declarations: [HtmlTransformPipe],
				providers: [DomSanitizer],
			}).compileComponents();
		})
	);

	it('create an instance', () => {
		const pipe = TestBed.inject(HtmlTransformPipe);
		expect(pipe).toBeTruthy();
	});
});
