import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pluidName',
})
export class PluidNamePipe implements PipeTransform {
  idmap = {
    CF02001: {
      zhName: '機車強制險',
      vhName: 'Bảo hiểm bắt buộc TNDS xe máy',
      enName: 'Compulsory Insurance',
    },
    CF01001: {
      zhName: '機車任意險',
      vhName: 'Bảo hiểm tự nguyện xe máy',
      enName: 'Voluntary Insurance',
    },
    CF02002: {
      zhName: '汽車強制險',
      vhName: 'Bảo hiểm bắt buộc TNDS ô tô',
      enName: 'Compulsory Auto Insurance',
    },
    CF01002: {
      zhName: '汽車任意險',
      vhName: 'Bảo hiểm tự nguyện ô tô',
      enName: 'Voluntary Auto Insurance',
    },
    CF01003: {
      zhName: '汽車任意險',
      vhName: 'Bảo hiểm tự nguyện ô tô',
      enName: 'Voluntary Auto Insurance',
    },
    CF01007: {
      zhName: '汽車任意(強制加保駕駛乘客傷害)',
      vhName: 'Ô Tô',
      enName:
        'Car Insurance(mandatory additional insurance for driving passenger injury)',
    },
    AC01001: {
      zhName: '旅綜險(國外)',
      vhName: 'Bảo hiểm du lịch (Nước Ngoài)',
      enName: 'Travel Insurance (Foreign)',
    },
    AC01002: {
      zhName: '旅綜險(國外)',
      vhName: 'Bảo hiểm du lịch (Nước ngoài)',
      enName: 'Travel Insurance (Foreign)',
    },
    AC01005: {
      zhName: '旅綜險(國內)',
      vhName: 'Bảo hiểm du lịch (Trong nước)',
      enName: 'Travel Insurance (Domestic)',
    },
    AC01013: {
      zhName: '旅綜險(國內)',
      vhName: 'Bảo hiểm du lịch (Trong Nước)',
      enName: 'Travel Insurance (Domestic)',
    },
    AC01014: {
      zhName: '旅綜險(國外)',
      vhName: 'Bảo hiểm du lịch (Nước Ngoài)',
      enName: 'Travel Insurance (Domestic)',
    },
    AC01015: {
      zhName: '旅綜險(國外)',
      vhName: 'Bảo hiểm du lịch (Nước Ngoài)',
      enName: 'Travel Insurance (Domestic)',
    },
  };
  transform(value: string, ...param: string[]): string {
    const lang = param[0];

    let text: string | null = null;

    if (lang === 'zh-TW') {
      text = this.idmap[value].zhName;
    }
    if (lang === 'vi-VN') {
      text = this.idmap[value].vhName;
    }
    if (lang === 'en-US') {
      text = this.idmap[value].enName;
    }

    // 斷行處理
    if (text) {
      const list = text.split('(');

      if (list.length > 1) {
        list[1] = `<br class="d-block d-lg-none" />(${list[1]}`;
      }

      return list.join('');
    }

    return null;
  }
}
