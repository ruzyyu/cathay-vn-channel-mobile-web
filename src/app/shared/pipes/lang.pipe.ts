import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lang',
})
export class LangPipe implements PipeTransform {
  transform(value: any, ...param: any[]): string {
    const lang = param[1];
    const item = param[0];

    if (lang === 'zh-TW') { return item.zhName; }
    if (lang === 'VN') { return item.vnName; }
    if (lang === 'EN') { return item.enName; }
    return '';
  }
}
