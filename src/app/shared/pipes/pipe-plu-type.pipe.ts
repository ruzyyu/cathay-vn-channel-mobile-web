import { map } from 'rxjs/operators';
import { Pipe, PipeTransform } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Pipe({
  name: 'pipePluType',
})
export class PipePluTypePipe implements PipeTransform {
  constructor(private cds: DatapoolService) {}
  transform(value: any, ...args: any): unknown {
    const [mtype, args1] = args;
    if (!value) {
      return value;
    }

    if (mtype === 'vkd' && this.cds.gPubCommon.carList) {
      return this.cds.gPubCommon.carList.find((item) => {
        return item.vechicleKdValue === value;
      }).zhName;
    } else if (mtype === 'ckd' && this.cds.gPubCommon.carUseList) {
      return this.cds.gPubCommon.carUseList.find((item) => {
        return item.carPwrKdValue === value;
      }).zhName;
    } else if (mtype === 'motor' && this.cds.gPubCommon.motoList) {
      return this.cds.gPubCommon.motoList.find((item) => {
        return item.carPwrKdValue === args1 && item.vehcKdValue === value;
      }).zhName;
    }
    return value;
  }
}
