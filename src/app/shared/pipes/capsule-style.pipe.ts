import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'capsuleStyle',
})
export class CapsuleStylePipe implements PipeTransform {
  transform(value: string, ...param: string[]): string {
    const item = {
      takeEffect: 'is-green-middle',
      notYetEffect: 'is-orange',
      invalid: 'is-grey',
    };

    const d1 = moment(param[0]); // 起始時間
    const d2 = moment(param[1]);
    const now = moment();

    if (now >= d1 && now <= d2) {
      return item.takeEffect;
    }
    if (now < d1) {
      return item.notYetEffect;
    }
    if (now > d2) {
      return item.invalid;
    }
    return '';
  }
}
