import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

@Component({
  selector: 'app-leave-page',
  templateUrl: './leave-page.component.html',
  styleUrls: ['./leave-page.component.scss'],
})
export class LeavePageComponent implements OnInit {
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang;
  }

  @Input() title;

  @Input() info;

  @Output() FLeavePage = new EventEmitter();

  FLang: any;

  ngOnInit(): void {}

  openPage(){
    this.FLeavePage.emit(true);
  }
}
