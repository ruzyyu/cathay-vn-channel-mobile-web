import { AlertServiceService } from 'src/app/core/services/alert/alert-service.service';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialService } from '../../../core/services/social.service';
import { UserInfo } from '../../../core/interface/index';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';

@Component({
  selector: 'app-third-bind',
  templateUrl: './third-bind.component.html',
  styleUrls: ['./third-bind.component.scss'],
})
export class ThirdBindComponent implements OnInit {
  @Input() googleStatusPOP;
  @Input() facebookStatusPOP;
  constructor(
    private alert: AlertServiceService,
    private router: Router,
    private fb: FormBuilder,
    private apiMember: MemberService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private auth: SocialService
  ) {
    this.FLang = this.cds.gLang;
  }
  /** 宣告區Start */
  userInfo: UserInfo;
  validators: {};
  group: FormGroup;
  controls;
  FLoginError = false;
  FIDError = false;
  FLang: any;
  /** 宣告區End */
  /** 建立驗證表單 */
  formValidate() {
    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        { value: this.validators[item][0], disabled: false },
        this.validators[item][1],
      ];
    });
    this.group = this.fb.group(validator);
    // 錯誤訊息顯示
    this.controls = this.group.controls;

    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });
  }
  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          user_account:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES011,
          user_password:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES010,
        }[control_key];
      }
      /** 新檢核end */

      return (
        {
          fontLengthNumber:
            this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
          LengthInterval: this.FLang.ER.ER022,
          passwordRules: this.FLang.ER.ER022,
          email: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES003,
        }[type] || this.FLang.ER.ER017
      );
    }
    return this.FLang.ER.ER017;
  }
  /** 登入成功 */
  goMember() {
    this.doCloseAlert();
    this.router.navigate(['login/register-id'], {
      queryParams: {
        module: true,
      },
    });
    this.router.navigate(['member']);
  }
  /** 關閉alert */
  doCloseAlert() {
    $('#popThirdBind').modal('hide');
  }
  goFindAccount() {
    this.doCloseAlert();
    this.router.navigateByUrl('login/find-account');
  }
  ngOnInit(): void {
    /** 表單驗證項目 */
    this.validators = {
      user_account: ['', [Validators.required, this.vi.email]],
      user_password: [
        '',
        [Validators.required, this.vi.vnLength8to12, this.vi.vnPasswordRules],
      ],
    };
    this.formValidate();
  }
  async doSubmit() {
    /** 解除封印 允許後檢核 */
    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });

    /** 檢查判斷是否驗證成功 */
    if (this.group.status !== 'VALID') {
      return;
    }

    const payload: any = {
      email: this.controls.user_account.value,
      kor: this.controls.user_password.value,
    };
    const login = await this.apiMember.MemberCathayLogin(payload);
    /** 登入成功 */
    if (login.status === 200) {
      this.cds.userInfo = login.data;
      /** 判斷會員ID是否等於前頁檢測ID */
      if (
        this.cds.userInfoTemp.certificate_number_9 !==
        login.data.certificate_number_9
      ) {
        this.FLoginError = false;
        return (this.FIDError = true);
      }
      this.goMember();
    } else {
      /** 60002 找不到資料, 50004 密碼錯誤,60004 尚未建立密碼 */
      const errorStatus = [60002, 50004, 60004];
      const errorCheck = !!errorStatus.find(
        (item) => item === login.error.status
      );
      if (errorCheck) {
        this.FLoginError = true;
        this.FIDError = false;
      } else {
        /** 例外錯誤處理 */
        this.FLoginError = true;
        this.FIDError = false;
        this.doCloseAlert();
        this.router.navigateByUrl('login');
      }
    }
  }
  /** google 登入 */
  googlelogin() {
    const many: any = this.auth
      .signInWithGoogle()
      .then((res) => {
        this.auth.user = res;
        const payload: any = {
          fb_account: '',
          google_account: this.auth.user.id,
        };
        this.checkSocialLogin(payload);
      })
      .catch((err) => {
        this.doCloseAlert();
        this.router.navigateByUrl('login');
      });
  }
  /** google 登出 */
  googleSignOut() {
    this.auth
      .signOutWithGoogle()
      .then()
      .catch((err) => {
        if (err === 'Not logged in') {
          this.doCloseAlert();
          this.router.navigateByUrl('login');
        }
      });
  }
  /** Facebook 登入 */
  fblogin() {
    const many: any = this.auth
      .signInWithFB()
      .then((res) => {
        this.auth.user = res;
        const payload: any = {
          fb_account: this.auth.user.id,
          google_account: '',
        };
        this.checkSocialLogin(payload);
      })
      .catch((err) => {
        this.doCloseAlert();
        this.router.navigateByUrl('login');
      });
  }
  /** 社群登入判斷 */
  async checkSocialLogin(payload) {
    payload = await this.apiMember.MemberOAUTH(payload);
    if (payload.status === 200) {
      this.cds.userInfo = payload.data;
      this.doCloseAlert();
      this.router.navigate(['login/register-id'], {
        queryParams: {
          module: true,
        },
      });
      this.router.navigate(['member']);
    } else {
      if (payload.error.status === 60002 || payload.error.status === 60004) {
        this.doCloseAlert();
        this.router.navigateByUrl('login/id-verification');
      } else {
        /** 例外錯誤處理 */
        this.doCloseAlert();
        this.router.navigateByUrl('login');
      }
    }
  }
}
