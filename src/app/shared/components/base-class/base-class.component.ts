import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-base-class',
  templateUrl: './base-class.component.html',
  styleUrls: ['./base-class.component.scss'],
})
export class BaseClassComponent {
  constructor(private metaTitle: Title) {}

  public setTitle(newTitle?: string) {
    let title = this.getHeadBaseTitle();

    if (newTitle) {
      title += ` - ${newTitle}`;
    }

    this.metaTitle.setTitle(title);
  }

  getHeadBaseTitle() {
    return 'VN Cathay insurance';
  }
}
