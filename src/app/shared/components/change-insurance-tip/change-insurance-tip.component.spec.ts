import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeInsuranceTipComponent } from './change-insurance-tip.component';

describe('ChangeInsuranceTipComponent', () => {
  let component: ChangeInsuranceTipComponent;
  let fixture: ComponentFixture<ChangeInsuranceTipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeInsuranceTipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeInsuranceTipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
