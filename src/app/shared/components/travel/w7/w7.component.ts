import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-w7',
  templateUrl: './w7.component.html',
  styleUrls: ['./w7.component.scss'],
})
export class W7Component implements OnInit {
  FLang: any;
  FLangType: string;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.C02.W7;
    this.FLangType = this.cds.gLangType;
  }

  ngOnInit(): void {}
}
