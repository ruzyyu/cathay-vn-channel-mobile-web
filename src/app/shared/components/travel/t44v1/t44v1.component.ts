import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-t44v1',
  templateUrl: './t44v1.component.html',
  styleUrls: ['./t44v1.component.scss'],
})
export class T44v1Component implements OnInit {
  FLang: any;
  FLangType: string;
  constructor(private cds: DatapoolService) {
    this.FLang = this.cds.gLang.C02;
    this.FLangType = this.cds.gLangType;
  }
  ngOnInit(): void {}
}
