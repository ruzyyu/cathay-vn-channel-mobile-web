import { CommonService } from './../../../core/services/common.service';
import { DatapoolService } from './../../../core/services/datapool/datapool.service';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
} from '@angular/core';

@Component({
  selector: 'app-plancard',
  templateUrl: './plancard.component.html',
  styleUrls: ['./plancard.component.scss'],
})
export class PlancardComponent implements OnInit, AfterViewInit {
  @Input() xPlan: number;
  @Input() xAbroad: boolean;
  @Input() xTravelCalcList: Array<any>;
  @Input() xIsShop: boolean;
  @Output() xPlanChange = new EventEmitter();

  @Input() xchooseComponent: string;
  @Output() xchooseComponentChange = new EventEmitter();

  @Output() xcalcPlan = new EventEmitter();
  FLang: any;
  FLangType: string;
  /** 參數設定slide */
  slideConfig: any;
  constructor(private cds: DatapoolService) {}

  ngOnInit(): void {
    this.xAbroad
      ? (this.FLang = this.cds.gLang.C02.plancard)
      : (this.FLang = this.cds.gLang.C02.plancard_out);

    this.FLangType = this.cds.gLangType;
  }

  ngAfterViewInit() {
    const init = (this.xPlan - 1) % 3;
    CommonService.bindSlick(init);
    /**  滑動元件的參數 */
    this.initAdjustHeight();
  }

  checkPlan(value: number) {
    this.xPlan = value;
    this.xPlanChange.emit(this.xPlan);
    this.xcalcPlan.emit();
  }

  changeComponent(value: string) {
    this.xchooseComponent = value;
    this.xchooseComponentChange.emit(this.xchooseComponent);
  }

  /** 條款下載 */
  download(type, status) {
    if (type === 0) {
      switch (status) {
        case 1:
          window.open('assets/pdf/T1-T3.pdf', 'T1-T3');
          break;
        case 2:
          window.open('assets/pdf/T1-T3.pdf', 'T1-T3');
          break;
        case 3:
          window.open('assets/pdf/T1-T3.pdf', 'T1-T3');
          break;
      }
    } else {
      window.open('assets/pdf/T4.pdf', 'T4');
    }
  }

  /** 調整牌卡高度 */
  initAdjustHeight() {
    this.adjustHeight();

    $(window).resize(() => {
      this.adjustHeight();
    });
  }

  adjustHeight() {
    const titles = $('.compare-item .compare-title');

    $('.table-compare .compare-row').each(function () {
      $(this)
        .find('.compare-row-line')
        .each(function (index: number) {
          const height = titles[index].clientHeight + 1.5;
          $(this).css('height', height);
        });
    });
  }
}
