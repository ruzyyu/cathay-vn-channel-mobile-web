import { Router } from '@angular/router';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Lang } from 'src/types';
@Component({
  selector: 'app-renew-insurance',
  templateUrl: './renew-insurance.component.html',
  styleUrls: ['./renew-insurance.component.scss'],
})
export class RenewInsuranceComponent implements OnInit {
  @Output() Action = new EventEmitter();
  FLang: Lang;
  FLangType: string;

  get FDueSoonCard() {
    return this.cds.gDueSoonCard;
  }

  constructor(private cds: DatapoolService, private go: Router) {
    this.FLang = this.cds.gLang.F04;
    this.FLangType = this.cds.gLangType;
    this.FLang = this.cds.gLang;
  }

  ngOnInit(): void {}

  doSubmit(option) {
    this.Action.emit(option.cntr_no);
  }
  goMember() {
    this.go.navigateByUrl('member');
  }
}
