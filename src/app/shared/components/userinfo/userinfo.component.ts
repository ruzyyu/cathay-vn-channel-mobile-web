import { MemberService } from './../../../core/services/api/member/member.service';
import { PublicService } from 'src/app/core/services/api/public/public.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { ValidatorsService } from 'src/app/core/services/utils/validators.service';
import { DatePipe } from '@angular/common';
import { IResponse, UserInfo } from 'src/app/core/interface';
import { IConfirm, ICounty, IProvince } from './../../../core/interface/index';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  Output,
  HostListener,
} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-userinfo',
  templateUrl: './userinfo.component.html',
  styleUrls: ['./userinfo.component.scss'],
})
export class UserinfoComponent implements OnInit {
  /**
   * @field data
   * @description 判別 [新增會員]、[修改會員]
   * @param data.xType 'add' | 'edit'
   * @description 判別 [渠道的判別]、[N是給旅遊用的]、[C是理賠]
   * @param data.xchannel 'car' | 'motor' | 'trial' | 'claim
   */
  @Input() data: any;
  /** 是否更新會員資料 */
  @Input() xSyncUserinfo: boolean;
  /** 同步按鈕 */
  @Output() xUserInfoChange = new EventEmitter();

  @Output() xSyncUserinfoChange = new EventEmitter();
  /** id檢核 */
  @Output() xAction = new EventEmitter();

  /** 宣告區Start */
  FConfirm: IConfirm;
  userInfo: UserInfo;
  validators: {};
  group: FormGroup;
  controls;
  FCountyList: Array<ICounty>;
  FProvinceList: Array<IProvince>;
  FLang: any;
  FBirthday: string;
  FcheckIdError: string;
  Field: any = ['certificate_number', 'name', 'sex', 'birthday'];
  regtext = /[0-9~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]/g;
  // 生日是否開啟文字輸入功能 false為開啟
  FReadonly: boolean;
  // 判斷旅遊險 會員登入 是否滿18歲
  FDateType = true;
  FIdCheck = false;
  constructor(
    private fb: FormBuilder,
    private apiPublic: PublicService,
    private cds: DatapoolService,
    private vi: ValidatorsService,
    private datePipe: DatePipe,
    private apiMember: MemberService
  ) {
    this.FLang = this.cds.gLang;
  }

  // 小網日期不得開啟填寫功能
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 564) {
      if (this.data.xType !== 'edit') {
        this.FReadonly = false;
      }
    } else {
      this.FReadonly = true;
    }
  }

  /** 建立驗證表單 */
  formValidate() {
    let mtype = [];
    if (this.data.xType === 'edit') {
      mtype = [...this.Field];
    }

    const validator = {};
    Object.keys(this.validators).forEach((item) => {
      validator[item] = [
        {
          value: this.validators[item][0],
          disabled: mtype.includes(item) ? true : false,
        },
        this.validators[item][1],
      ];
    });

    this.group = this.fb.group(validator);

    // 錯誤訊息顯示
    this.controls = this.group.controls;
    Object.keys(this.controls).forEach((key) => {
      if (this.controls[key].value) {
        this.controls[key].markAsTouched();
      }
    });

    this.group.valueChanges.subscribe(() => {
      this.cds.userInfo = {
        ...this.cds.userInfo,
        email: this.controls.email.value,
        email_bk: this.controls.email_bk.value,
        mobile: this.controls.mobile.value,
        county: this.controls.county.value,
        province: this.controls.province.value,
        addr: this.controls.address.value,
      };

      this.xUserInfoChange.emit(this.controls);
    });

    // 日期異動時檢測
    this.group.get('birthday').valueChanges.subscribe((item) => {
      if (item && this.FIdCheck) {
        this.group
          .get('certificate_number')
          .setValidators([
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value
            ),
            this.vi.vn2020ID(this.controls.birthday.value),
            this.vi.fontLengthNumber,
          ]);
        this.group.get('certificate_number').updateValueAndValidity();
      }
    });

    // 性別異動時檢測
    this.group.get('sex').valueChanges.subscribe((item) => {
      if (item && this.FIdCheck) {
        this.group
          .get('certificate_number')
          .setValidators([
            this.vi.vnCertNumber(
              this.controls.birthday.value,
              this.controls.sex.value
            ),
            this.vi.fontLengthNumber,
          ]);
        this.group.get('certificate_number').updateValueAndValidity();
      }
    });

    /** init trigger */
    this.xUserInfoChange.emit(this.controls);
  }

  /** 取得錯誤訊息 */
  getErrors(xcontrol) {
    if (xcontrol.errors) {
      const type = Object.keys(xcontrol.errors)[0];

      /** 新檢核start */
      if (type === 'required') {
        const control_key = Object.keys(this.group.controls).find(
          (item) => this.group.controls[item] === xcontrol
        );

        return {
          certificate_number:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES004,
          name: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
          birthday:
            this.FLang.ER.select.ER001 +
            ' ' +
            this.FLang.ER.select.ER001_.ES005,
          sex:
            this.FLang.ER.select.ER001 +
            ' ' +
            this.FLang.ER.select.ER001_.ES001,
          mobile: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES006,
          email: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES007,
          email_bk:
            this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES008,
          province:
            this.FLang.ER.select.ER001 + ' ' + this.FLang.ER.enter.ER001_.ES002,
          county:
            this.FLang.ER.select.ER001 + ' ' + this.FLang.ER.enter.ER001_.ES003,
          address: this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES009,
        }[control_key];
      }
      /** 新檢核end */
      return {
        maxlength: this.FLang.ER.ER043,
        vnPhone: this.FLang.ER.ER041,
        email: this.FLang.ER.ER042,
        birthday18year: this.FLang.ER.ER038,
        fontLengthNumber:
          this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
        languageFormat:
          this.FLang.ER.enter.ER001 + this.FLang.ER.enter.ER001_.ES002,
        languagelength:
          this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES001,
        required: this.FLang.ER.ER017,
        vnCertNumber: this.FLang.ER.ER036,
        vn2020ID: this.FLang.ER.enter.ER002 + this.FLang.ER.enter.ER002_.ES007,
      }[type];
    }
    return this.FLang.F07.content.F0033;
  }

  async ngOnInit() {
    // 順序需依照UI的順序擺放，為了後續的滑動檢核是順的
    this.validators = {
      certificate_number: [
        this.cds.userInfo.certificate_number_12 ||
          this.cds.userInfo.certificate_number_9,
        [Validators.required, this.vi.fontLengthNumber],
      ],
      name: [
        this.cds.userInfo.customer_name,
        [
          Validators.required,
          this.vi.languageFormat,
          this.vi.languagelength(160),
        ],
      ],
      birthday: [this.cds.userInfo.birthday, [Validators.required]],
      sex: [this.cds.userInfo.sex || '', [Validators.required]],
      mobile: [
        this.cds.userInfo.mobile,
        [Validators.required, this.vi.vnPhone],
      ],
      email: [this.cds.userInfo.email, [this.vi.email, Validators.required]],
      // this.data.xType === 'edit'
      email_bk: [
        this.cds.userInfo.email_bk,
        this.data.xType === 'edit'
          ? [this.vi.email, Validators.required]
          : [this.vi.email],
      ],
      province: [this.cds.userInfo.province || '', [Validators.required]],
      county: [this.cds.userInfo.county || '', [Validators.required]],
      address: [
        this.cds.userInfo.addr,
        [Validators.required, Validators.maxLength(240)],
      ],
      updated: [this.xSyncUserinfo || true, []],
    };

    this.formValidate();
    this.FBirthday = this.datePipe.transform(
      this.cds.userInfo.birthday || '',
      'dd/MM/yyyy'
    );
    $('#user_birthday').val('');
    if (this.data.xType === 'edit') {
      $('#user_birthday').val(this.FBirthday);
    }
    // 省縣
    await this.getprovinceList();

    // 小網日期不得開啟填寫功能
    if ($(document).innerWidth() > 564) {
      if (this.data.xType !== 'edit') {
        this.FReadonly = false;
      }
    } else {
      this.FReadonly = true;
    }
  }

  get certificate_number() {
    return this.group.get('certificate_number');
  }
  get name() {
    return this.group.get('name');
  }
  get email() {
    return this.group.get('email');
  }
  get email_bk() {
    return this.group.get('email_bk');
  }
  get sex() {
    return this.group.get('sex');
  }
  get mobile() {
    return this.group.get('mobile');
  }
  get birthday() {
    return this.group.get('birthday');
  }
  get address() {
    return this.group.get('address');
  }
  get province() {
    return this.group.get('province');
  }
  get county() {
    return this.group.get('county');
  }

  /** 省縣 */
  async getprovinceList() {
    // 縣市初始化
    if (!this.cds.gPubCommon.provinceList) {
      await this.apiPublic.gCountyList();
    }

    if (this.cds.gPubCommon && this.cds.gPubCommon.provinceList) {
      this.FProvinceList = this.cds.gPubCommon.provinceList
        ? this.cds.gPubCommon.provinceList
        : [];
    } else {
      this.FProvinceList = [];
    }

    if (this.FProvinceList.length > 0) {
      let mindex = Number(this.cds.userInfo.province) - 1 || 0;
      if (mindex < 0) {
        mindex = 0;
      }
      this.FCountyList = this.FProvinceList[mindex].countys;
    }

    // 重新賦予資料
  }

  notifyCounty(event) {
    this.controls.county.setValue('');
    this.controls.county.markAsUntouched();
    const index = event.target.selectedIndex - 1;
    this.FCountyList = this.FProvinceList[index].countys;
  }

  getDate(event) {
    if (!event) {
      return;
    }
    let birthday;
    if (event && event.indexOf('/') !== -1) {
      birthday = this.datePipe.transform(event, 'yyyy-MM-dd');
    } else {
      birthday = event;
    }
    this.group.controls.birthday.setValue(birthday);
    this.group.controls.birthday.markAsTouched();
    this.FBirthday = moment(event).format('DD/MM/YYYY');
  }

  async checkId() {
    this.FcheckIdError = '';
    if (
      this.controls.certificate_number.value.length === 9 ||
      this.controls.certificate_number.value.length === 12
    ) {
      const payload = {
        certificate_number: this.controls.certificate_number.value,
      };
      const rep: IResponse = await this.apiMember.MemberVerifyID2(payload);

      if (rep.status !== 200) {
        this.FcheckIdError = this.FLang.ER.ER037;
        this.controls.certificate_number.setValue('');
        this.controls.certificate_number.markAsTouched();
        this.xAction.emit(payload);
      }
    }
  }

  onSyncClick() {
    this.xSyncUserinfoChange.emit(!this.controls.updated.value);
  }

  doReflashScreen() {
    this.group.patchValue({
      certificate_number:
        this.cds.userInfo.certificate_number_12 ||
        this.cds.userInfo.certificate_number_9,
      name: this.cds.userInfo.customer_name.trim(),
      birthday: this.cds.userInfo.birthday,
      sex: this.cds.userInfo.sex,
      mobile: this.cds.userInfo.mobile,
      email: this.cds.userInfo.email,
      email_bk: this.cds.userInfo.email_bk,
      province: this.cds.userInfo.province,
      county: this.cds.userInfo.county,
      address: this.cds.userInfo.addr,
    });
    this.FBirthday = moment(this.cds.userInfo.birthday).format('DD/MM/YYYY');
  }

  onUpdateCDStoControl(val) {
    Object.keys(this.controls).forEach((item) => {
      const mtype = [...this.Field];
      if (this.controls[item]) {
        this.controls[item].setValue(val[item]);
        mtype.includes(item)
          ? this.controls[item].disable()
          : this.controls[item].enable();
      }
      this.controls.certificate_number.setValue(
        val.certificate_number_12 || val.certificate_number_9
      );
      this.controls.certificate_number.markAsUntouched();
      this.FcheckIdError = '';
      this.controls.name.setValue(val.customer_name);
      this.controls.address.setValue(val.addr);
      this.controls.updated.setValue(true);
    });

    this.FBirthday = moment(val.birthday).format('DD/MM/YYYY');
    this.data.xType = 'edit';

    $('#user_birthday').val('');
    if (this.data.xType === 'edit') {
      $('#user_birthday').val(this.FBirthday);
    }
  }

  checkPayment() {
    /** 身分證額外檢核 */
    this.group
      .get('certificate_number')
      .setValidators([
        this.vi.fontLengthNumber,
        Validators.required,
        this.vi.vnCertNumber(
          this.controls.birthday.value,
          this.controls.sex.value
        ),
        this.vi.vn2020ID(this.controls.birthday.value),
      ]);
    this.group.get('certificate_number').updateValueAndValidity();
    /* 開啟日期與性別 身分證檢測 */

    if (this.controls.certificate_number.vaild) {
      this.checkId();
    }
    this.FIdCheck = true;

    if (this.data.xchannel === 'travel') {
      this.group
        .get('birthday')
        .setValidators([
          Validators.required,
          this.vi.birthday18year(
            this.controls.birthday.value || this.cds.userInfo.birthday
          ),
        ]);
      this.group.get('birthday').updateValueAndValidity();
    }

    if (this.data.xchannel === 'travel' && this.data.xType === 'edit') {
      if (moment().diff(this.cds.userInfo.birthday, 'years') >= 18) {
        this.FDateType = true;
      } else {
        this.FDateType = false;
      }
    }

    Object.keys(this.controls).forEach((item) => {
      this.controls[item].markAsDirty();
      this.controls[item].markAsTouched();
    });
    return this.group.valid && this.FDateType ? true : false;
  }

  /** 生日規則
   * 1. 最大歲數不得超過 100歲
   * 2. 最小歲數 日期不得小於當日
   * 3. 日期若沒資料時預設起始歲數30歲
   */
  get mixDate() {
    return moment(new Date()).subtract(100, 'years');
  }

  get maxDate() {
    return new Date();
  }

  get startDate() {
    return this.FBirthday
      ? moment(this.controls.birthday.value).toDate()
      : moment(new Date()).subtract(30, 'years');
  }

  /** 將 input 值 轉換成大寫 */
  changeToUpperCase(event) {
    const input = event.target;
    const start = input.selectionStart;
    const end = input.selectionEnd;
    input.value = input.value.toLocaleUpperCase();
    input.setSelectionRange(start, end);
  }
}
