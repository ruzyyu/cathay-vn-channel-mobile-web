import { IJob } from '../core/interface';

export const jobDetail: IJob[] = [
  {
    id: '1',
    type: 'ReBao',
    title: 'Nhân viên bồi thường',
    area: 'Đồng Nai',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value:
          'Xử lý các hồ sơ bồi thường cho các sản phẩm của kênh Doanh nghiệp (Tài sản - Kỹ thuật - Hàng hải - Trách nhiệm)',
      },
      {
        value:
          'Thực hiện các công việc liên quan đến hồ sơ bồi thường cho các sản phẩm của kênh doanh nghiệp, bao gồm nhưng không giới hạn các công việc:',
        children: [
          'Nhận và xử lý thông báo tổn thất.',
          'Giám định hiện trường tổn thất.',
          'Hướng dẫn khách hàng lập hồ sơ yêu cầu bồi thường.',
          'Trình giải quyết bồi thường.',
          'Quản lý hồ sơ bồi thường.',
        ],
      },
    ],
    content: [
      {
        value:
          'Tốt nghiệp các đại học kỹ thuật hoặc kinh tế. Ưu tiên các chuyên ngành Hàng hải, Giao thông, Cơ khí, Xây dựng, Bảo hiểm.<br />Tiếng Anh tương đương bằng B, kỹ năng đọc viết tốt. Ưu tiên ứng viên biết thêm tiếng Hoa.<br />Có kinh nghiệm trong lĩnh vực giám định và/hoặc bồi thường. Ưu tiên ứng viên có chứng chỉ giám định tổn thất do Bộ TC hoặc các cơ sở đào tạo nước ngoài cấp.<br />Sử dụng thuần thục các công cụ tin học văn phòng (word, excel, powerpoint ...)',
      },
    ],
  },
  {
    id: '2',
    type: 'ReBao',
    title: 'Trưởng phòng Tái bảo',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value:
          'Nghiên cứu và hoạch định kế hoạch hợp đồng tái bảo hiểm, hoàn thành sắp xếp các hợp đồng bảo hiểm, tăng điều kiện hoặc điều chỉnh hợp đồng theo nhu cầu của kinh doanh phù hợp với lợi ích của công ty theo đúng chiến lược phát triển.',
      },
      {
        value:
          'Tiếp đón và thăm hỏi đồng nghiệp tái bảo hiểm trong ngoài nước, duy trì quan hệ khăng khít.',
      },
      {
        value:
          'Lập kế hoạch nhận, nhượng tái bảo hiểm tạm thời và tổng hợp các nghiệp vụ tái bảo hiểm tạm thời, nắm bắt tiến độ của từng trường hợp cá biệt, đảm bảo thu xếp tái bảo hiểm tạm thời trước ngày phát sinh hiệu lực.',
      },
      {
        value:
          'Theo dõi thanh toán tái bảo hiểm và xét duyệt hóa đơn kế toán, đảm bảo các nghiệp vụ tái bảo hiểm phù hợp quy chế của công ty.',
      },
      {
        value:
          'Hoạch định và đôn đốc thực hiện kế hoạch công việc quản lý hằng ngày và kiểm soát chi phí.',
      },
      {
        value:
          'Xét duyệt các tài liệu về dự toán, thanh toán, đảm bảo phù hợp với chế độ kế toán/kiểm toán của công ty.',
      },
      {
        value:
          'Quản lý và đào tạo nhân viên cấp dưới, nâng cao hiệu quả làm việc nhóm và năng lực chuyên môn của từng nhân viên.',
      },
    ],
    content: [
      {
        value:
          'Tốt nghiệp Đại học, có chuyên ngành bảo hiểm hoặc thương mại là lợi thế',
      },
      { value: 'Có 5 năm kinh nghiệm trở lên trong lĩnh vực tái bảo hiểm' },
      { value: 'Tiếng Anh thành thạo 4 kỹ năng' },
    ],
  },
  {
    id: '3',
    type: 'IT',
    title: 'Chuyên viên lập trình ứng dụng',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value:
          'Lập trình, thiết kế, xây dựng, sửa chữa và thử nghiệm hệ thống nghiệp vụ bảo hiểm.',
      },
      {
        value: 'Quản lý,chỉnh sửa website công ty.',
      },
    ],
    content: [
      {
        value:
          'Học vấn: Tốt nghiệp Đại học/Cao Đẳng/bằng cấp tương đương chuyên ngành công nghệ thông tin.',
      },
      {
        value:
          'Kinh nghiệm: Có trên 2 năm kinh nghiệm thực tế trong lĩnh vực lập trình và phát triển phần mềm.',
      },
      {
        value: 'Kiến thức:',
        children: [
          'Có kiến thức tốt về kỹ thuật lập trình ứng dụng web application:  java,JavaScript, AJAX, CSS, jquery.',
          'Am hiểu về thiết kế CSDL:  DB2, viết những câu lệnh SQL.',
        ],
      },
      {
        value: 'Kỹ năng:',
        children: [
          'Có khả năng phân tích yêu cầu user, thiết kế, lập trình và kiểm thử ứng dụng thủ công.',
          'Có khả năng viết tài liệu về phạm vi yêu cầu của user, bản thiết kế ứng dụng.',
          'Có kỹ năng giao tiếp tốt,kỹ năng làm việc đội nhóm, chủ động và có tinh thần trách nhiệm trong công việc.',
        ],
      },
    ],
  },
  {
    id: '4',
    type: 'Indemnify',
    title: 'Chuyên Viên Kinh Doanh (Tiếng Trung)',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value:
          'Thu thập, tổng hợp, quản lý và thống kê phân tích thông tin khách hàng.',
      },
      {
        value: 'Chăm sóc khách hàng, kênh phân phối hiện có',
      },
      {
        value:
          'Thu hút, khai thác và phục vụ khách hàng cá nhân và doanh nghiệp',
      },

      {
        value: 'Thực hiện và hoàn thành theo kế hoạch kinh doanh của công ty.',
      },
    ],
    content: [
      {
        value: 'Tốt nghiệp Đại học',
      },
      {
        value: 'Tiếng Hoa lưu loát.',
      },
      {
        value:
          'Ít nhất 1 năm kinh nghiệm làm việc kinh doanh. Hoặc được đào tạo nếu chưa có kinh nghiệm về bảo hiểm nhưng có tiềm năng phát triển.',
      },
      {
        value: 'Có kỹ năng phối hợp, giao tiếp',
      },
      {
        value:
          'Nắm vững kiến thức chuyên môn và có kỹ năng bán hàng tốt. Có kiến thức tốt về sản phẩm bảo hiểm',
      },
    ],
  },
  {
    id: '5',
    type: 'ReBao',
    title: 'Nhân viên kế hoạch',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value: 'Phân tích các chỉ số kinh doanh',
      },
      {
        value: 'Phân tích các dữ liệu để đề xuất phương án kinh doanh phù hợp',
      },
      {
        value:
          'Phối hợp phân tích số liệu kinh doanh cho các đơn vị trực thuộc khi có yêu cầu',
      },
      {
        value: 'Lập các báo cáo phân tích định kỳ tháng/ quý',
      },
    ],
    content: [
      {
        value:
          'Tốt nghiệp từ bậc Đại học. Ưu tiên các chuyên ngành Tài chính, Bảo hiểm, khối ngành Thống kê Kinh tế...',
      },
      {
        value:
          'Nhạy về số liệu, khả năng sử dụng Microsoft Excel thành thạo, có kĩ năng tổng hợp, phân tích số liệu, chỉ số và thông tin tài chính/kinh doanh;',
      },
      {
        value: 'Nhạy bén, có khả năng lập kế hoạch;',
      },
      {
        value: 'Ưu tiên ứng viên có kinh nghiệm trong ngành bảo hiểm;',
      },
      {
        value: 'Ưu tiên ứng viên thành thạo tiếng Trung.',
      },
    ],
  },
  {
    id: '6',
    type: 'ReBao',
    title: 'Chuyên viên phát triển đối tác',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value:
          'Điều tra nghiên cứu thị trường, lên ý tưởng phát triển mô hình hợp tác và sản phẩm hợp tác phù hợp với thị trường',
      },
      {
        value:
          'Tìm kiếm, thiết lập, duy trì mối quan hệ với các đối tác trong ngành công nghệ (kênh TMĐT, ví điện tử, app ứng dụng CNTT ...)',
      },
      {
        value:
          'Đề xuất hợp tác, đàm phán và triển khai hợp đồng hợp tác với các đối tác',
      },
      {
        value:
          'Theo dõi hiệu quả, báo cáo cập nhật tình hình hợp tác và đề xuất phương án cải thiện chất lượng hợp tác',
      },
      {
        value: 'Công việc khác theo yêu cầu của Trưởng bộ phận.',
      },
    ],
    content: [
      {
        value: 'Tốt nghiệp bậc Đại học trở lên',
      },
      {
        value: 'Có trên 3 năm kinh nghiệm phát triển thị trường',
      },
      {
        value:
          'Khả năng giao tiếp, tìm kiếm đối tác và xây dựng mối quan hệ tốt',
      },
      {
        value: 'Nhạy bén, ham học hỏi, chủ động trong công việc',
      },
      {
        value: 'Ưu tiên ứng viên có kinh nghiệm BD trong lĩnh vực công nghệ',
      },
    ],
  },
  {
    id: '7',
    type: 'IT',
    title: 'Chuyên viên IT',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value:
          'Thực hiện các công việc về quản lý hệ thống (System Admin): hệ thống Windows, Linux và Database',
      },
      {
        value: 'Chi tiết công việc sẽ trao đổi cụ thể qua buổi phỏng vấn.',
      },
    ],
    content: [
      {
        value: 'Học vấn:',
        children: ['Cao đẳng - hoặc  đại học'],
      },
      {
        value: 'Kinh nghiệm:',
        children: [
          'Ưu tiên ứng viên Biết tiếng hoa, Biết Amazon Web',
          'Services, Biết viết Script kinh nghiệm trên 1 năm',
        ],
      },
      {
        value: 'Kỹ năng:<br />Quản lý hệ thống:',
        children: [
          'Nắm rõ 1 trong các hệ điều hành sau: Linux, Windows Server, Database',
          'Biết Database',
          'Biết viết script,',
          'Biết ngôn ngữ Python',
          'Có khả năng giao tiếp, làm việc nhóm, thái độ làm việc tích cực',
        ],
      },
      {
        value: 'Nhạy bén, ham học hỏi, chủ động trong công việc',
      },
      {
        value: 'Ưu tiên ứng viên có kinh nghiệm BD trong lĩnh vực công nghệ',
      },
    ],
  },
  {
    id: '8',
    type: 'ReBao',
    title: 'Chuyên viên pháp chế',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value: 'Giải quyết các vụ việc trong lĩnh vực tố tụng;',
      },
      {
        value: 'Tư vấn, soạn thảo, rà soát, đàm phán các loại Hợp đồng;',
      },
      {
        value:
          'Các công việc liên quan đến Tuân thủ pháp luật và phối hợp giải quyết các chuyên án liên quan đến số hóa hoạt động kinh doanh;',
      },
    ],
    content: [
      {
        value: 'Tốt nghiệp đại học trở lên chuyên ngành Luật hoặc tương đương.',
      },
      {
        value: 'Tiếng Hoa lưu loát (4 kỹ năng nghe, nói, đọc, viết)',
      },
      {
        value:
          'Có kinh nghiệm ít nhất 2-3 năm trong lĩnh vực pháp lý hoặc tài chính.',
      },
      {
        value:
          'Nắm vững và áp dụng được các quy phạm pháp luật về Luật và các văn bản hướng dẫn thi hành...do Nhà nước ban hành.',
      },
      {
        value: 'Nắm vững việc soạn thảo các loại hợp đồng.',
      },
      {
        value: 'Trung thực, nhiệt tình, có trách nhiệm với công việc.',
      },
      {
        value: 'Chịu được áp lực công việc.',
      },
      {
        value: 'Có kỹ năng làm việc chi tiết, đức tính cẩn thận;',
      },
      {
        value:
          'Có khả năng nghiên cứu độc lập cũng như làm việc nhóm dưới áp lực cao và thể hiện được tính chủ động trong công việc;',
      },
    ],
  },
  {
    id: '9',
    type: 'ReBao',
    title: 'Chuyên viên kế toán',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value: 'Tổng hợp số liệu và báo cáo số liệu hàng tháng của công ty.',
      },
      {
        value: 'Báo cáo số liệu cho Cơ quan chủ quản.',
      },
      {
        value: 'Báo cáo và quyết toán Thuế.',
      },
    ],
    content: [
      {
        value: 'Tốt nghiệp đại học chuyên ngành Kế toán - Tài chính.',
      },
      {
        value:
          'Yêu cầu bắt buộc có 3 năm kinh nghiệm làm việc trở lên liên quan đến Kế toán hoặc Kiểm toán trong ngành Bảo Hiểm.',
      },
      {
        value: 'Có chứng chỉ Kế toán trưởng',
      },
      {
        value: 'Ưu tiên biết tiếng Hoa hoặc tiếng Anh',
      },
      {
        value: 'Cẩn thận, có trách nhiệm với công việc.',
      },
      {
        value: 'Sử dụng thành thạo vi tính văn phòng.',
      },
      {
        value: 'Có thể phối hợp tăng ca vào cuối tháng.',
      },
    ],
  },
  {
    id: '10',
    type: 'ReBao',
    title: 'Nhân viên bồi thường -BP.CSKH',
    area: 'TP.HCM',
    email: 'hr@cathay-ins.com.vn',
    explain: [
      {
        value:
          'Thực hiện các công việc liên quan đến hồ sơ bồi thường cho các sản phẩm của kênh doanh nghiệp, bao gồm nhưng không giới hạn các công việc:',
        children: [
          'Nhận và xử lý thông báo tổn thất.',
          'Giám định hiện trường tổn thất.',
          'Hướng dẫn khách hàng lập hồ sơ yêu cầu bồi thường.',
          'Trình giải quyết bồi thường.',
          'Quản lý hồ sơ bồi thường.',
        ],
      },
    ],
    content: [
      {
        value:
          'Tốt nghiệp các đại học kỹ thuật hoặc kinh tế. Ưu tiên các chuyên ngành Hàng hải, Giao thông, Cơ khí, Xây dựng, Bảo hiểm.',
      },
      {
        value:
          'Tiếng Anh tương đương bằng B, kỹ năng đọc viết tốt. Ưu tiên ứng viên biết thêm tiếng Hoa.',
      },
      {
        value:
          'Có kinh nghiệm trong lĩnh vực giám định và/hoặc bồi thường. Ưu tiên ứng viên có chứng chỉ giám định tổn thất do Bộ TC hoặc các cơ sở đào tạo nước ngoài cấp.',
      },
      {
        value:
          'Sử dụng thuần thục các công cụ tin học văn phòng (word, excel, powerpoint ...)',
      },
    ],
  },
];
