import { CommonService } from './../common.service';
import { InsuranceService } from './../api/Insurance/insurance.service';
import { MemberService } from 'src/app/core/services/api/member/member.service';
import { fromEvent, Subject } from 'rxjs';
import { AlertServiceService } from './../alert/alert-service.service';
import { IResponse } from './../../interface/index';
import { OrderService } from './../api/order/order.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import * as shajs from 'js-sha256';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  constructor(
    private router: Router,
    private cds: DatapoolService,
    private apiOrder: OrderService,
    private alert: AlertServiceService,
    private apiInsurance: InsuranceService,
    private apiMember: MemberService
  ) {
    setTimeout(() => {
      this.onInit();
    }, 100);
  }

  FPayResult: boolean;

  payment$: Subject<any>;

  codeMsg = [
    { statusCode: '1', message: 'Payment is Successful' },
    { statusCode: '0', message: 'Payment is Review' },
    { statusCode: '7', message: 'Payment is Review' },
    { statusCode: '-1', message: 'Payment is Failed' },
    { statusCode: '-9', message: 'Payment is Cancel' },
  ];

  /**
   * @description 組成支付網址
   * 測試方法
   * 信用卡卡號:  4111111111111111
   * 密碼: 123
   * 有效年月: 01/2021
   * 姓名: NGUYEN VAN A
   * @param {number} amt 付款金額
   * @param {string} order_id 訂單號碼
   */
  async doPayment(amt: number, order_id: string, paymentType?: string) {
    if (!this.cds.pay_pwd) {
      await this.apiMember.MembergetParam();
    }
    const sha256 = shajs.sha256;
    const securityKey = this.cds.pay_pwd;
    const destinationUrl = this.cds.VNPay_Url;

    const payload = {
      amount: amt,
      currency: 'VND',
      payment_type: paymentType,
      receiver_account: this.cds.pay_account,
      reference_number: order_id,
      transaction_type: 'sale',
      url_return: location.origin + this.router.url,
      website_id: this.cds.websitId,
      signature: undefined,
    };

    const arrPayload = Object.keys(payload).map((key) => payload[key]);
    const plaintext = `${arrPayload.join('|')}${securityKey}`;

    payload.signature = sha256(plaintext).toUpperCase();
    payload.url_return = encodeURIComponent(payload.url_return);

    const mstr = Object.keys(payload)
      .map((key) => `${key}=${payload[key]}`)
      .join('&');

    this.FPayResult = false;

    const win = window.open(
      `${destinationUrl}?${mstr}`,
      'VNPay',
      this.getConfig()
    );

    const startTime = moment();
    const interval = setInterval((count: number = 0) => {
      count++;

      if (win.closed) {
        clearInterval(interval);
        if (!this.FPayResult) {
          this.payment$.error({ status: -9, message: '付款已取消' });
        }
      }

      /** 超過5分鐘、窗體失去焦點 */
      if (win.focus) {
        const endTime = moment();
        const seconds = endTime.diff(startTime, 'seconds');
        console.info(seconds);
        if (seconds >= 300) win.close();
      }
    }, 1000);

    // if (this.payment$) return this.payment$;
    // this.payment$ =
  }

  getPayObserve() {
    this.payment$ = new Subject<any>();
    return this.payment$;
  }

  getConfig(): string {
    const ww = 700;
    const hh = 800;
    // 視窗的垂直位置 600是height
    const mtop = (window.screen.availHeight - 30 - hh) / 2;
    // 視窗的水平位置 450 是 width
    const mleft = (window.screen.availWidth - 10 - ww) / 2;
    return `height=${hh}, width=${ww}, top=${mtop},left=${mleft}`;
  }

  /**
   * @param state this.cds.gBeforPayinfo
   */
  async editOrderState(param, xstate: string = '5', payType: number) {
    const { order_no, aply_no, prod_pol_code, amt } = this.cds.gBeforPayinfo;
    // 付款成功 改狀態

    const payload = {
      member_seq_no: this.cds.userInfo.seq_no,
      order_status: xstate,
      order_no,
      aply_no,
      prod_pol_code,
      pay_data: {
        pay_type: payType + 1, // 1：國際卡 - Thẻ quốc tế，2：記帳卡 - Thẻ nội địa，3：電子錢包 - Ví điệnt
        pay_amt: amt,
        pay_date: moment().format('YYYY-MM-DD HH:mm:ss'),
        pay_status: param.status,
        pay_refno: param.reference_number,
        remark: JSON.stringify(param),
      },
    };
    if (this.cds.gTCalc_to_save) {
      this.cds.gTCalc_to_save.type = 'none';
    }
    const rep = await this.apiOrder.editOrderState(payload);
    return rep.status === 200;
  }

  showResult(xtype) {
    if (xtype === '1') {
      this.alert.open({
        type: 'info',
        title: this.cds.gLang.payment.L0001,
        // content: this.cds.gLang.payment.L0001,
      });
    } else {
      this.payment$.error({
        status: -9,
        message: this.cds.gLang.payment.L0002,
      });
    }
  }

  onInit() {
    if (window.opener) {
      // fromEvent(window, 'beforeunload').subscribe((event: any) => {
      //   delete (event || window.event).returnValue;
      // });

      const win = window.opener as Window;
      win.postMessage(
        {
          type: 'afterPay',
          param: window.location.toString(),
        },
        '*'
      );
      // 解決畫面關閉導致大樹沒長出來
      // 正確解決方法應該是將其移動到ngOninit ...
      // 當前使用位置在constrator
      setTimeout(() => {
        window.close();
      }, 100);
    } else {
      window.addEventListener('message', this.onMessage.bind(this));
    }
  }

  async onMessage(event: any) {
    // 子視窗通知付款完成
    if (event.data.type === 'afterPay') {
      const url = new URL(event.data.param);
      const params: any = {};
      url.searchParams.forEach((value, key) => {
        params[key] = value;
      });
      this.FPayResult = true;
      this.payment$.next(params);
    }
  }

  /** work. */
  async savetxend(payload, updateUser = false) {
    // 註冊會員
    let rep_any: any;
    if (updateUser) {
      rep_any = await this.apiMember.MemberReg(payload.userinfo);
      if (rep_any.status === 200) {
        this.apiMember.reflashUserInfo(payload.userinfo);
      }
    }

    if (!updateUser || (updateUser && rep_any.status === 200)) {
      /** 訂單存檔start */
      updateUser
        ? (payload.saveorder.member_seq_no = payload.userinfo.seq_no)
        : (payload.saveorder.member_seq_no = this.cds.userInfo.seq_no);
      let rep: IResponse = await this.apiOrder.saveOrder(
        payload.saveorder,
        payload.type === 'travel' ? 'TRAVEL' : 'CAR'
      );

      if (rep.status !== 200) {
        this.alert.open({
          type: 'info',
          title: this.cds.gLang.payment.L0003,
          content: rep.error.data,
        });
        return rep;
      }

      const { aply_no, order_no } = rep.data;
      payload.saveorder.aply_no = aply_no;
      payload.saveorder.order_no = order_no;

      /** 核保start */
      if (rep.status === 200) {
        if (payload.type === 'travel') {
          rep = await this.apiInsurance.checkTravelOrderOK(payload.saveorder);
        } else {
          rep = await this.apiInsurance.checkOrderOK(payload.saveorder);
        }

        rep.data = {
          ...rep.data,
          aply_no,
          order_no,
        };
        return rep;
      }
    }
  }

  /** 快速註冊 */
  regeditFlow(payload, id) {
    const req = {
      email: payload.email,
      mobile: payload.mobile,
      notice_action: 'reg',
    };
    this.apiMember.MemberSendOTP(req);

    this.cds.userInfoTemp = payload;
    id.length === 9
      ? (this.cds.userInfoTemp.certificate_number_9 = id)
      : (this.cds.userInfoTemp.certificate_number_12 = id);
  }

  openEmptyWin() {
    return window.open('', 'VNPay', this.getConfig());
  }
}
