import { TestBed } from '@angular/core/testing';

import { DatapoolService } from './datapool.service';

describe('DatapoolService', () => {
  let service: DatapoolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatapoolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
