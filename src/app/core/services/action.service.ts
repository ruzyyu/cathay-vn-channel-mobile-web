import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import IdleJs from 'idle-js';

@Injectable({
  providedIn: 'root'
})
/** 共用操作 */
export class ActionService {
  /** 閒置 */
  accountIdle: any;

  constructor() {
    if (CommonService.isLoginSub$.value) { this.setAccountIdle(); }
  }

  /** 會員登入 */
  async login() {
    return true;
  }

  /** 會員登出 */
  async logout() {
    return true;
  }

  /** 設定閒置時間 */
  setAccountIdle() {
    this.accountIdle = new IdleJs({
      // 設定閒置時間 (單位:毫秒)
      idle: (1 * 60 * 1000) * 9.5,
      // events that will trigger the idle resetter
      events: ['keydown', 'mousedown', 'touchstart'],
      onIdle: () => {
        // callback function to be executed after idle time
      },
      onActive: async () => {
        // callback function to be executed after back form idleness
      },
      onHide: () => {
        // callback function to be executed when window become hidden
      },
      onShow: () => {
        // callback function to be executed when window become visible
      },
      // set it to false if you want to be notified only on the first idleness change
      keepTracking: true,
      // set it to true if you want to start in the idle state
      startAtIdle: false
    });
    this.accountIdle.start();
  }

}
