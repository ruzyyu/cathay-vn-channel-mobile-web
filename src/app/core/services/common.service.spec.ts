import { TestBed } from '@angular/core/testing';

import { CommonService } from './common.service';

describe('CommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommonService = TestBed.get(CommonService);
    expect(service).toBeTruthy();
  });

  it('should return false if response not valid', () => {
    const resp: any = { MWHEADER: null, TRANRS: null };
    // expect(CommonService.isHttpRespSuccess(resp)).toBeFalsy();
    // expect(CommonService.isHttpRespSuccess(resp, true, 'test')).toBeFalsy();
  });
});
