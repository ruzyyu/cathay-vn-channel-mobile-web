// import { TestBed } from '@angular/core/testing';
// import { StorageService } from './storage.service';
// import { StorageKeys } from '../enums/storage-key.enum';


// const value = 'A123456789';


// describe('StorageService', () => {
//   beforeEach(() => TestBed.configureTestingModule({}));

//   it('should be created', () => {
//     const service: StorageService = TestBed.get(StorageService);
//     expect(service).toBeTruthy();
//   });

//   it('should set item to storage', () => {
//     StorageService.setItem(StorageKeys.memberId, value);
//     expect(sessionStorage.getItem(StorageKeys.memberId)).toBe(value);
//   });

//   it('should get item from storage', () => {
//     expect(StorageService.getItem(StorageKeys.memberId)).toBe(value);
//   });

//   it('should remove item from storage', () => {
//     StorageService.removeItem(StorageKeys.memberId);
//     expect(sessionStorage.getItem(StorageKeys.memberId)).toBeNull();
//   });

//   it('should remove item from storage', () => {
//     sessionStorage.setItem(StorageKeys.memberId, value);
//     sessionStorage.setItem(StorageKeys.memberName, value);

//     StorageService.clearAll();

//     expect(sessionStorage.getItem(StorageKeys.memberId)).toBeNull();
//     expect(sessionStorage.getItem(StorageKeys.memberName)).toBeNull();
//   });
// });
