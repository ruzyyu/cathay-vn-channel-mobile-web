import { DatapoolService } from './../datapool/datapool.service';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RouterFilterService implements CanActivate {
  constructor(private router: Router, private cds: DatapoolService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    let isLogin: boolean;

    const user = this.cds.userInfo.seq_no;
    if (!user) {
      isLogin = false;
      // 跳轉到登入
      this.cds.gPre_url = state.url;
      this.router.navigate(['/login']);
    } else {
      isLogin = true;
    }
    return isLogin;
  }
}
