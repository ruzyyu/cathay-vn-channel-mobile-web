import { TestBed } from '@angular/core/testing';

import { RouterFilterService } from './router-filter.service';

describe('RouterFilterService', () => {
  let service: RouterFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RouterFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
