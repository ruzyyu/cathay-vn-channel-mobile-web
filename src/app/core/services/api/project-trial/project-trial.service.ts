import { ApiHelperService } from 'src/app/core/services/utils/api-helper.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProjectTrialService {
  constructor(private api: ApiHelperService) {}

  /** 試算 */
  async trialCalculation(payload) {
    const result = await this.api.post(
      'Insurance/Compulsory/CalucateComp',
      payload
    );

    return result;
  }
}
