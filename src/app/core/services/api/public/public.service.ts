import { IResponse, UserInfo } from './../../../interface/index';
import { StorageKeys } from './../../../enums/storage-key.enum';
import { StorageService } from './../../storage.service';
import { Injectable } from '@angular/core';
import { ApiHelperService } from 'src/app/core/services/utils/api-helper.service';
import { DatapoolService } from 'src/app/core/services/datapool/datapool.service';
import { LocalService } from '../local/local.service';
import { CommonService } from 'src/app/core/services/common.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PublicService {
  constructor(
    private api: ApiHelperService,
    private cds: DatapoolService,
    private localapi: LocalService
  ) {}

  /** nationList  */
  async beforstart() {
    return new Promise(async (resolve, reject) => {
      this.cds.gPubCommon = {};
      resolve(true);
    })
      .then(async (value) => {
        // 多國語
        if (value) {
          // 越文：VN，英文：EN，中文：zh-TW 預設VN
          const lang = StorageService.getItem(StorageKeys.lang) || 'VN';
          StorageService.setItem(StorageKeys.lang, lang || 'VN');

          this.cds.gLangType = 'vi-VN';
          if (lang === 'EN') {
            this.cds.gLangType = 'en-US';
          }
          if (lang === 'zh-TW') {
            this.cds.gLangType = 'zh-TW';
          }

          this.cds.gLang = await this.gGetLang(lang);

          return true;
        }
      })
      .then((value) => {
        if (value) {
          if (!window['env']['apiUrl']) {
            return true;
          }

          if (
            window['env']['apiUrl'].lastIndexOf('/') !==
            window['env']['apiUrl'].length - 1
          ) {
            window['env']['apiUrl'] = `${window['env']['apiUrl']}/`;
          }

          return true;
        }
      })
      .then(async (value) => {
        // 載入登入資訊
        if (value) {
          const memberinfo = StorageService.getItem(StorageKeys.memberinfo);

          if (memberinfo) {
            const memberinfoStr = JSON.parse(memberinfo);
            const decodeData = CommonService.ObjDecode(memberinfoStr);

            this.cds.userInfo = decodeData as UserInfo;
          }
          return true;
        }
      })
      .then(async (value) => {
        // 取得normalToken
        if (value) {
          this.cds.gNormalToken = StorageService.getItem(
            StorageKeys.normarlToken
          );
          this.cds.gMobileToken = StorageService.getItem(
            StorageKeys.mobileToken
          );
          if (!this.cds.gNormalToken) {
            const rep: any = await this.getNormalToken();

            if (rep.status === 200) {
              StorageService.setItem(
                StorageKeys.normarlToken,
                rep.access_token
              );
              this.cds.gNormalToken = rep.access_token;
            }
            return true;
          }
          this.cds.cathaylifebaseurl = atob(
            StorageService.getItem(StorageKeys.cp5)
          );
          this.cds.cathaylifesso = atob(
            StorageService.getItem(StorageKeys.cp6)
          );
        }
      })
      .then(() => {
        // 動態預載入資料
        // this.gAirPortList();

        return true;
      });
  }

  async getNormalToken() {
    const rep: IResponse = await this.api.post('token');
    return rep;
  }

  // loadStyle(styleName: string) {
  //   const head = document.getElementsByTagName('head')[0];

  //   let themeLink = document.getElementById(
  //     `client-${styleName}`
  //   ) as HTMLLinkElement;
  //   if (themeLink) {
  //     themeLink.href = styleName;
  //   } else {
  //     const style = document.createElement('link');
  //     style.id = `client-${styleName}`;
  //     style.rel = 'stylesheet';
  //     style.href = `${styleName}`;
  //     // style.rel = 'preload';
  //     head.appendChild(style);
  //   }
  // }

  async gAirPortList() {
    if (!this.cds.gPubCommon.airportList) {
      const rep: IResponse = await this.api.post(
        'Insurance/Common/SyncData/airportList',
        { mask: false }
      );
      if (rep.status === 200) {
        let countryList = rep.data.reduce((obj, item) => {
          if (!obj[`${item.municipality} ${item.isoCountry}`]) {
            obj[`${item.municipality} ${item.isoCountry}`] = {
              group: `${item.municipality} ${item.isoCountry}`,
              isoCountry: item.isoCountry,
              groupname: `${item.municipality}, ${item.nation_name_eng}` || '',
              data: [],
            };
          }
          obj[`${item.municipality} ${item.isoCountry}`].data.push(item);
          return obj;
        }, {});
        countryList = Object.values(countryList);
        this.cds.gPubCommon.airportList = countryList;
      }
    }
  }

  async gnationList() {
    if (!this.cds.gPubCommon.nationList) {
      const rep = (await this.api.post(
        'Insurance/Common/SyncData/nationList',
        {},
        { mask: true, alert: true }
      )) as IResponse;

      if (rep.status === 200) {
        rep.data.forEach((element) => {
          if (this.cds.gLangType === 'vi-VN') {
            element.nationNameCn = element.nationNameVn;
          }
          if (this.cds.gLangType === 'en-US') {
            element.nationNameCn = element.nationNameEng;
          }
        });

        this.cds.gPubCommon.nationList = [].concat(rep.data);
        return;
      }
    }
  }

  /** 省縣清單 */
  async gCountyList() {
    if (!this.cds.gPubCommon.provinceList) {
      const rep: any = await this.api.post(
        'Insurance/Common/SyncData/countyList',
        {},
        { mask: true, alert: true }
      );

      /**
       * @description 多國語加工
       */
      if (rep.status === 200) {
        const lang = ['vi-VN', 'en-US'];

        rep.data.forEach((element) => {
          if (lang.includes(this.cds.gLangType)) {
            element.provinceZhname = element.provinceVnname;
          }

          element.countys.forEach((item) => {
            if (lang.includes(this.cds.gLangType)) {
              item.countyZhname = item.countyVnname;
            }
          });
        });

        this.cds.gPubCommon.provinceList = [].concat(rep.data);
      }
    }
  }

  /** 摩托車類型清單 */
  async gMotorList() {
    if (this.cds.gPubCommon.motoList) {
      return this.cds.gPubCommon.motoList;
    }
    const rep: IResponse = await this.api.post(
      'Insurance/Common/SyncData/vehicleUsetoScooterList',
      {}
    );

    if (rep.status === 200) {
      rep.data.forEach((element) => {
        if (this.cds.gLangType === 'vi-VN') {
          element.zhName = element.vnName;
        }
        if (this.cds.gLangType === 'en-US') {
          element.zhName = element.enName;
        }
      });

      this.cds.gPubCommon.motoList = rep.data;
    }
  }

  /** 汽車車種 */
  async gCarList() {
    if (this.cds.gPubCommon.carList) {
      return this.cds.gPubCommon.carList;
    }

    const rep: IResponse = await this.api.post(
      'Insurance/Common/SyncData/vechicleKdtoCarList',
      {}
    );

    if (rep.status === 200) {
      rep.data.forEach((element) => {
        if (this.cds.gLangType === 'vi-VN') {
          element.zhName = element.vnName;
        }
        if (this.cds.gLangType === 'en-US') {
          element.zhName = element.enName;
        }
      });

      this.cds.gPubCommon.carList = rep.data;
    }
  }

  /** 汽車類型 */
  async gCarTypeList() {
    if (this.cds.gPubCommon.carUseList) {
      return this.cds.gPubCommon.carUseList;
    }

    const rep: IResponse = await this.api.post(
      'Insurance/Common/SyncData/vehicleUsetoCarList',
      {}
    );

    if (rep.status === 200) {
      rep.data.forEach((element) => {
        if (this.cds.gLangType === 'vi-VN') {
          element.zhName = element.vnName;
        }
        if (this.cds.gLangType === 'en-US') {
          element.zhName = element.enName;
        }
      });

      this.cds.gPubCommon.carUseList = rep.data;
    }
  }

  /** 銀行下拉 */
  async gBankList() {
    if (this.cds.gPubCommon.backList) {
      return this.cds.gPubCommon.backList;
    }
    const rep: IResponse = await this.api.post(
      'Insurance/Common/SyncData/bankList',
      {}
    );
    if (rep.status === 200) {
      this.cds.gPubCommon.backList = rep.data;
    }
  }

  /**
   * @description 多國語言切換
   */
  gGetLang(lang) {
    // 越文：vi-VN，英文：en-US，中文：zh-TW
    if (lang === 'VN') {
      return this.localapi.JSON('assets/json/vn.json');
    }
    if (lang === 'EN') {
      return this.localapi.JSON('assets/json/en.json');
    }
    if (lang === 'zh-TW') {
      return this.localapi.JSON('assets/json/zh-tw.json');
    }
  }

  /** 取得 汽車代號對應的內存 */
  async getvkd_id(vkd: string, ckd: string) {
    if (!this.cds.gPubCommon.carList) {
      await this.gCarList();
    }
    const items = this.cds.gPubCommon.carList;
    return items.findIndex((item) => item.vechicleKdValue === vkd);
  }

  /** 取得 汽車use代號對應的內存 */
  // async getCarUseName(vkd: string, ckd: string) {
  async getckd_id(vkd: string, ckd: string) {
    if (!this.cds.gPubCommon.carUseList) {
      await this.gCarList();
    }
    const items = this.cds.gPubCommon.carUseList;
    // 不要修改，這是搭配下拉選單塞選材這樣寫的...
    return items
      .filter((item) => item.vehcKdValue === vkd)
      .findIndex((item) => item.carPwrKdValue === ckd);
  }

  /** 取得機車清單 */
  async getMotorName(vkd: string, ckd: string) {
    if (!this.cds.gPubCommon.motoList) {
      await this.gMotorList();
    }

    const items = this.cds.gPubCommon.motoList;
    console.log(items);

    // 不要修改，這是搭配下拉選單塞選材這樣寫的...
    return items.findIndex(
      (item) => item.vehcKdValue === vkd && item.carPwrKdValue === ckd
    );
  }
  /** 取得最新消息搜尋資料 */
  async SearchGetList(payload) {
    return await this.api.post('mobileweb/api/search/getlist', payload);
  }

  /** 取得最新消息搜尋資料 */
  async getMarquee() {
    const rep: IResponse = await this.api.get(
      'mobileweb/api/news/getlist?typeid=4'
    );
    if (rep.status === 200) {
      let mStr;
      rep.data.length !== 0 && rep.data[0]
        ? (mStr = {
            title: rep.data[0].title,
            type_id: rep.data[0].type_id,
            id: rep.data[0].id,
          })
        : (mStr = {
            title: '',
            type_id: '',
            id: '',
          });
      return mStr;
    }
    return '';
  }
}
