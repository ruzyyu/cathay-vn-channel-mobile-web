import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StatusService {
  constructor() {}
  certificate_number = undefined;
  verified_success = undefined;

  getDate(certificate_number, status) {
    this.certificate_number = certificate_number;
    this.verified_success = status;
  }
  setDate_certificate_number() {
    return this.certificate_number;
  }
  setDate_verified_success() {
    return this.verified_success;
  }
}
