import { TestBed } from '@angular/core/testing';

import { OnlinecalcService } from './onlinecalc.service';

describe('OnlinecalcService', () => {
  let service: OnlinecalcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OnlinecalcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
