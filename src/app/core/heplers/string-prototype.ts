String.prototype.formatDate = formatDate;

interface String {
  formatDate: typeof formatDate;
}

/** 日期格式轉換 for IOS, IE */
function formatDate() {
  return this?.replace(/-/g, '/');
}
