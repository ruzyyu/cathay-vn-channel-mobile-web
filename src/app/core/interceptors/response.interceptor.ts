import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable()
/** httpClient Response 中繼器 */
export class ResponseInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> | any {
    // 取 ip 失敗不顯示錯誤
    if (req.url.includes(environment.ipAddressUrl)) {
      return next.handle(req);
    }

    // return next.handle(req).pipe(
    //   catchError((error: HttpResponse<any>) => {
    //     switch (error.status) {
    //       case 401:
    //         SweetAlertService.info('未經授權，請重新登入。');
    //         break;
    //       default:
    //         SweetAlertService.info('系統發生錯誤，請洽資訊人員處理。');
    //         break;
    //     }
    //     return throwError(error);
    //   }),
    //   map((res: HttpResponse<any>) => {
    //     if (res?.body?.MWHEADER?.RETURNCODE) {
    //       const baseResp = res.body as BaseRes<any>;
    //       switch (
    //         baseResp.MWHEADER.RETURNCODE
    //         // 集中處理通用錯誤碼 (ex: 登入逾時)
    //       ) {
    //       }
    //     }
    //     return res;
    //   })
    // );
  }
}
