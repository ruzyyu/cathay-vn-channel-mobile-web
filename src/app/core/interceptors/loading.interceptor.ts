import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
/** httpClient loading 中繼器 */
export class LoadingInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('/ncp')) {
      document.body?.classList.add('noClick');

      return next.handle(req).pipe(
        catchError((error: HttpResponse<any>) => {
          document.body?.classList.remove('noClick');
          return throwError(error);
        }),
        tap(res => {
          if (res instanceof HttpResponse) {
            document.body?.classList.remove('noClick');
          }
        }));
    }
    return next.handle(req);
  }
}