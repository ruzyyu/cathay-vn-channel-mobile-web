import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
/** httpClient request 中繼器 */
export class RequestInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!req.url.includes('/assets/i18n')) {
      // tslint:disable-next-line: max-line-length
      const token =
        sessionStorage.getItem('token') ||
        'eyJhbGciOiJIUzM4NCJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJpYXQiOjE1OTk0NTE2NzEsImV4cCI6MTU5OTUzODA3MX0.Umk2ZBWD2Un1n7ylSgK-6QJItxKwrLkWZqiize31Ux7_33GJRB-mIbzEU_Likj8Q';

      const newReq = req.clone({
        url: `${window['env']['apiUrl']}${req.url}`,
        setHeaders: {
          Authorization: `Bearer ${token}`,
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Max-Age': '86400',
          'Cache-Control': 'no-cache',
        },
      });
      return next.handle(newReq);
    }

    return next.handle(req);
  }
}
