import { ValidationErrors } from '@angular/forms';

export const SingleErrorParser = (config: any, name: string, errors: ValidationErrors) => {
  if (!errors) { return; }
  const k = Object.keys(errors)[0];
  return (config[name] && config[name][k]) ? config[name][k](errors[k]) : '';
};