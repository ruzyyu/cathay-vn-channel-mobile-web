import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appCarNoformat]',
})
export class CarNoformatDirective {
  /**
   * 可以是字串或正則
   */
  @Input() xtype: string | RegExp;
  @Input() mtype: string;

  // 限制輸入的正則
  _regexp: RegExp;
  _reg_num = /[^0-9]/g;
  _reg_en = /[^A-Z]/g;

  constructor(private el: ElementRef, private control: NgControl) {}

  @HostListener('change')
  onChange() {
    this.control.control.setValue(this.el.nativeElement.value);
  }

  /**
   * blur & change 需重新給予值，解決最末碼的異常
   * @param $event
   */
  @HostListener('blur', ['$event'])
  onBlur($event) {
    this.control.control.setValue(this.el.nativeElement.value);
  }

  /**
   * 正常輸入
   */
  @HostListener('input', ['$event'])
  onInput(event) {
    this.carInput(event);
  }

  /**
   * @description 根據長度限制輸入
   * @param $event
   */
  @HostListener('keydown', ['$event'])
  onKeydown($event) {
    // if (
    //   ($event.keyCode >= 65 && $event.keyCode <= 90) ||
    //   ($event.keyCode >= 48 && $event.keyCode <= 57)
    // ) {
    //   // 1、2碼僅限英文 ASCII DEC 碼
    //   if (
    //     this.el.nativeElement.value.length <= 1 &&
    //     !($event.keyCode >= 48 && $event.keyCode <= 57)
    //   ) {
    //     $event.preventDefault(*);
    //     return;
    //   }
    //   // 3碼僅限英文
    //   if (
    //     this.el.nativeElement.value.length <= 2 &&
    //     this.el.nativeElement.value.length > 1 &&
    //     !($event.keyCode >= 65 && $event.keyCode <= 90)
    //   ) {
    //     $event.preventDefault();
    //     return;
    //   }
    //   if (
    //     this.el.nativeElement.value.length > 3 &&
    //     !($event.keyCode >= 48 && $event.keyCode <= 57)
    //   ) {
    //     $event.preventDefault();
    //     return;
    //   }
    // }
    // const reg = /^(\d{2}[A-Z]{1}[A-Z0-9]{1}-?\d{1,5})$|^(\d{2}[A-Z]{1})[A-Z0-9]{1}-$|^(\d{2}[A-Z]{1})[A-Z0-9]{1}$|^(\d{2}[A-Z]{1})$|^(\d{1,2})$/;
    // console.log(reg.test(this.el.nativeElement.value));
    // if (!reg.test(this.el.nativeElement.value)) {
    //   $event.preventDefault();
    //   return;
    // }
  }

  @HostListener('keyup', ['$event'])
  onKeyup($event) {
    // this.mtype for 機車電動車
    const reg =
      this.xtype === 'motor'
        ? this.mtype === 'C4'
        ? /^(\d{1,2})$|^(\d{2}[A-Z]{1})$|^(\d{2}[A-Z]{1}Đ)$|^(\d{2}[A-Z]{1}Đ[A-Z0-9]{0,1})$|^(\d{2}[A-Z]{1}Đ[A-Z0-9]{0,1}-)$|^(\d{2}[A-Z]{1}Đ[A-Z0-9]{0,1}-\d{0,5})$|^(\d{2}[A-Z]{1}Đ[A-Z0-9]{0,1}-\d{3}\.\d{0,2})$/
          : /^(\d{2}[A-Z]{1}[A-Z0-9]{0,1}-?\d{1,4})$|^(\d{2}[A-Z]{1}[A-Z0-9]{0,1}-?\d{3}\.\d{2})$|^(\d{2}[A-Z]{1})[A-Z0-9]{0,1}-?$|^(\d{2}[A-Z]{1})$|^(\d{1,2})$/
        : /^(\d{2}[A-Z]{1}[A-Z]{0,1}-?\d{1,4})$|^(\d{2}[A-Z]{1}[A-Z0-9]{0,1}-?\d{3}\.\d{2})$|^(\d{2}[A-Z]{1})[A-Z0-9]{0,1}-?$|^(\d{2}[A-Z]{1})$|^(\d{1,2})$/;

    this.el.nativeElement.value = this.el.nativeElement.value.toUpperCase();
    if (!reg.test(this.el.nativeElement.value)) {
      this.el.nativeElement.value = this.el.nativeElement.value.slice(0, -1);
      return;
    }
  }

  private carInput($event) {
    if (!this.el.nativeElement.value) {
      return;
    }

    if (!this.regtest($event.data)) {
      const mstr = this.el.nativeElement.value;
      const sel = $event.target.selectionStart;
      this.el.nativeElement.value =
        mstr.slice(0, sel - 1) + mstr.slice(sel, mstr.length);
      return;
    }
    if (['insertText', 'deleteContentBackward'].includes($event.inputType)) {
      const mstr = this.fillin(this.el.nativeElement.value, this.xtype);
      this.el.nativeElement.value = mstr;
    }
  }

  private regtest(str) {
    return /^[^.,:;<>\[\]()"!#$%&'*+=?\\^_`{|}~-]+$/.test(str);
  }

  private fillin(str, type) {
    return type === 'motor'
      ?
        this.mtype === 'C4'
        ? str
            .replace(/^(\d{2}[A-Z]{1}Đ{1}[A-Z0-9]{1})-?([0-9]+)\.?(.*)/, '$1-$2$3') // 電動車
            .replace(/^(\S+-)(\d{3})\.?(\d{2})(.*)$/, '$1$2.$3')
            .replace(/^(\S+-)(\d{3})\.(\d?)$/, '$1$2$3')
        : str
            .replace(/^(\d{2}[A-Z0-9Đ]{2,3})-?([0-9]+)\.?(.*)/, '$1-$2$3') // 機車
            .replace(/^(\S+-)(\d{3})\.?(\d{2})(.*)$/, '$1$2.$3')
            .replace(/^(\S+-)(\d{3})\.(\d?)$/, '$1$2$3')
      : str
          .replace(/^(\d{2}[A-Z]{1,2})-?([0-9]+)\.?(.*)/, '$1-$2$3') // 車
          .replace(/^(\S+-)(\d{3})\.?(\d{2})(.*)$/, '$1$2.$3')
          .replace(/^(\S+-)(\d{3})\.(\d?)$/, '$1$2$3');
  }

  // checkNum(input) {
  //   var reg = /^[0-9]+.?[0-9]*$/; //判断字符串是否为数字 ，判断正整数用/^[1-9]+[0-9]*]*$/
  //   if (!reg.test(input)) {
  //     return false;
  //   } else {
  //     return true;
  //   }
  // }

  /**
   * @description 刪除指定字串
   * @param source 來源字串
   * @param location  指定位置
   * @param len  刪除數量
   * @returns
   */
  deleteT(source, location, len) {
    return (
      source.substring(0, location) +
      source.substring(location + len, source.length)
    );
  }
}
