import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CommonService } from '../services/common.service';

@Injectable({
  providedIn: 'root'
})
/** 登入功能 Guard */
export class MemberGuard implements CanActivate {

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (CommonService.isLoginSub$.value) { return true; }

    return false;
  }
}
