// 改變顯示的元素
function changeFilterChildren(type) {
  if (type == "all") {
    $("[data-filter-content]").show();
  } else {
    $("[data-filter-content]").hide();
    $("[data-filter-content='" + type + "']").show();
  }
}

// 設置選擇完後的文字
function setBtnText(dropdownitem) {
  var dropdown = $(dropdownitem).parents(".dropdown");
  var btn = $(dropdown).find(".dropdown-toggle");
  var value = $(dropdownitem).text();

  // 如果有縮寫的話顯示縮寫
  if ($(btn).hasClass("dropdown-abbreviation")) {
    value = $(dropdownitem).find(".abbreviation").text();
  }
  $(dropdown).find(".filterActiveText").text(value);
  $(dropdown).find(".dropdown-input").val(value);
}

// 多選下拉 刪除值
$(".more-data").on("click", ".icons-ic-select-delete", function (e) {
  e.stopPropagation();
  $(this).parent().remove();
});

//多選下拉 設定值
function multiselect(dropdownitem) {
  var dropdown = $(dropdownitem).parents(".dropdown");
  var value = $(dropdownitem).text();
  var html =
    "<span>" + value + '<i class="icons-ic-select-delete i-icon"></i></span>';
  $(dropdown).find(".more-data").append(html);
}

// dropdown filter
function changeFilterDropdownActive() {
  $(".dropdown-menu.filter")
    .find("a")
    .click(function () {
      var type = $(this).data("filter-type");
      var dropdown = $(this).parents(".dropdown");
      $(this).addClass("active").siblings("a").removeClass("active");

      // 判斷是否為多選下拉
      if ($(this).parents(".dropdown").hasClass("form-more-search-input")) {
        multiselect(this);
        return;
      }
      changeFilterChildren(type);
      setBtnText(this);
    });
}

// tab filter
function changeFilterTabActive() {
  $("ul.tab.filter")
    .find("a")
    .click(function () {
      var type = $(this).data("filter-type");
      $(this)
        .parent("li")
        .addClass("active")
        .siblings("li")
        .removeClass("active");
      changeFilterChildren(type);
    });
}

$("input[name='ins-prod-type']").change(function (item) {
  var label = $(this).siblings("label");
  $(".selected-label").empty();

  if (this.checked && this.value == "all") {
    $(".condition-filter > li").show();
  }
  if (this.checked && this.value != "all") {
    currentType = $(this);
    $(".selected-label").append(
      $("<div/>", { class: "label" })
        .append($("<span/>").text(label.text()))
        .append($("<a/>", { role: "close" }).html('<i class="icon-close"></i>'))
    );

    $(".condition-filter > li").hide();
    $(".condition-filter")
      .children("li." + this.value)
      .show();
  }
});

$(document).ready(function () {
  var filterType;
  $("ul.tab.filter")
    .children()
    .each(function (index, value) {
      if ($(this).hasClass("active")) {
        filterType = $(this).children().attr("data-filter-type");
        return;
      }
    });
  if (!filterType) {
    $(".tab.filter")
      .children()
      .each(function (index, value) {
        if ($(this).hasClass("active")) {
          filterType = $(this).attr("data-filter-type");
          return;
        }
      });
  }
  $(".form-more-search-input").on("show.bs.dropdown", function () {
    $(this).find(".dropdown-input").focus();
  });
  changeFilterChildren(filterType);
  changeFilterDropdownActive();
  changeFilterTabActive();
});
