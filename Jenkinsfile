@Library('shared.jenkins.cfh') _
pipeline {
    agent any
    triggers {
        pollSCM('H 12,18 * * *')
    }
    options {
        ansiColor('xterm')
        timeout(time: 30, unit: 'MINUTES')
        disableConcurrentBuilds()
        disableResume()
        timestamps()
    }
    parameters {
        string(name: 'REPO_NAME', defaultValue: 'cathay-vn-channel-mobile-web')
        string(name: 'ECR_REGION', defaultValue: 'ap-southeast-1')
        string(name: 'POD_NAME', defaultValue: 'mobileweb')
        string(name: 'EKS_NAMESPACE', defaultValue: 'vnins-web')
    }
    environment {
        POD_NAME = "${params.POD_NAME}"
        EKS_NAMESPACE = "${params.EKS_NAMESPACE}"
        ECR_NAME = "${env.AWS_ACCOUNT}.dkr.ecr.${params.ECR_REGION}.amazonaws.com"
        REPO = "${env.ECR_NAME}/${params.REPO_NAME}"
        REPO_URL = "https://${env.ECR_NAME}"
        REPO_CRED = "ecr:${params.ECR_REGION}:773d64b8-1c26-4cf0-859b-48cb8b6142a4"
        BUILD_TAG = "${env.BRANCH_NAME}-${env.BUILD_NUMBER}"
    }
    stages {
        stage('Prepare ENV') {
            steps {
                sh 'printenv | sort'
            }
        }
        stage('Build') {
            steps {
                script {
                    cfhBuild.buildAngular()
                }
            }
        }
        stage('Test') {
            steps { 
                script {
                    cfhBuild.testAngular()
                }
            }
        }
        stage('Scan') {
            when {
                branch 'master'
                environment name: 'AWS_ACCOUNT', value: '106366429175'  // only perform scan on UAT-jenkins
            }
            steps {
                withSonarQubeEnv('SonarQube') {
                    sh 'chmod +x sonar.sh; ./sonar.sh'
                }
            }
        }
        stage('Push-develop') {
            when {
                branch 'develop'
                environment name: 'AUTO_REDEPLOY', value: 'YES'
            }
            steps { 
                script {
                    cfhPush()
                }
            }
        }
        stage('Push-master') {
            when {
                branch 'master'
                environment name: 'AWS_ACCOUNT', value: '968206472262'  // master image from PROD environment only
            }
            steps { 
                script {
                    cfhPush()
                }
            }
        }
        stage('Deploy') {
            when {
                branch 'develop'
                environment name: 'AUTO_REDEPLOY', value: 'YES'
            }
            steps { 
                script {
                    cfhDeploy()
                }
            }
        }
    }
    post {
        always {
            script {
                cfhNotify()
            }
        }
        cleanup {
            deleteDir()
        }
    }
}